'use strict';

module.exports = function(grunt) {

  var get_names = function(input, prefix) {
    var res = [];

    for(var i = 0; i < input.length; i++)
      res.push(prefix + /.+\/(.+)/g.exec(input[i])[1]);

    return res;
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json')
  });

  grunt.config.set('copy', {
    //bower
    libs: {
      expand: true,
      flatten: true,
      src: [ grunt.file.readJSON('paths.json').js.debug ], dest: '.tmp/lib/'
    },
    css: {
      expand: true,
      flatten: true,
      src: [ grunt.file.readJSON('paths.json').css.debug ], dest: '.tmp/css/'
    },

    scripts: {
      expand: true,
      flatten: true,
      dest: '.tmp/js',
      src: ['assets/js/*.js']
    },

    view: {
      cwd: 'assets/view/',
      expand: true,
      dest: '.tmp/view/',
      src: ['**']
    },
    pages: {
      cwd: 'assets/',
      expand: true,
      dest: '.tmp/',
      src: ['404.html']
    },

    assets: {
      cwd: 'assets/',
      expand: true,
      dest: '.tmp/',
      src: [
        'locale/**',
        'img/**',
        'fonts/**',
        'data/**'
      ]
    }
  });

  grunt.config.set('concat', {
    directives: {
      options: { separator: '\n\n' },
      src: ['assets/js/directives/*'],
      dest: '.tmp/js/directives.js',
    },

    controllers: {
      options: { separator: '\n\n' },
      src: ['assets/js/controllers/*'],
      dest: '.tmp/js/controllers.js',
    },

    services: {
      options: { separator: '\n\n' },
      src: ['assets/js/services/*'],
      dest: '.tmp/js/services.js',
    },

    styles: {
      src: ['assets/css/*.css'],
      dest: '.tmp/css/styles.css',
    }
  });

  grunt.config.set('htmlbuild', {
    debug_index: {
      src: 'assets/index.html',
      dest: '.tmp/',

      options: {
        beautify: true,
        relative: true,
        prefix: '/',
        scripts: {
          cwd: '.tmp/lib',
          libs: get_names(grunt.file.readJSON('paths.json').js.debug, '.tmp/lib/')
        }
      }
    },
    debug_signin: {
      src: 'assets/sign-in.html',
      dest: '.tmp/',

      options: {
        beautify: true,
        relative: true,
        prefix: '/',
        scripts: {
          cwd: '.tmp/lib',
          libs: get_names(grunt.file.readJSON('paths.json').js.debug, '.tmp/lib/')
        }
      }
    }
  });

  grunt.config.set('watch', {
    scripts: {
      files: ['assets/js/*.js'],
      tasks: ['copy:scripts']
    },
    styles: {
      files: ['assets/css/**'],
      tasks: ['concat:styles']
    },

    index: {
      files: ['assets/index.html'],
      tasks: ['htmlbuild:debug_index']
    },
    signin: {
      files: ['assets/sign-in.html'],
      tasks: ['htmlbuild:debug_signin']
    },
    view: {
      files: ['assets/view/**'],
      tasks: ['copy:view']
    },
    pages: {
      files: ['assets/404.html'],
      tasks: ['copy:pages']
    },

    assets: {
      files: [
        'assets/locale/**',
        'assets/img/**',
        'assets/data/**',
        'assets/fonts/**'
      ],
      tasks: ['copy:assets']
    },

    controllers: {
      files: ['assets/js/controllers/**'],
      tasks: ['concat:controllers']
    },
    services: {
      files: ['assets/js/services/**'],
      tasks: ['concat:services']
    },
    directives: {
      files: ['assets/js/directives/**'],
      tasks: ['concat:directives']
    },

    bower: {
      files: ['bower_components/**'],
      tasks: ['copy:libs', 'copy:css']
    }
  });

  grunt.registerTask('http', function() {
    setTimeout(function() {
      var cp = require('child_process'),
          task = cp.spawn(process.execPath, [ 'app.js' ], {
            cwd: process.cwd(),
            env: process.env
          });

      console.log('\nMatriculant: starting...');

      task.stdout.pipe(process.stdout);
      task.stderr.pipe(process.stderr);
      task.on('exit', function() {
        process.exit();
      });
    }, 500);
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-html-build');
  grunt.loadNpmTasks('grunt-express-server');

  grunt.registerTask('default', ['debug']);
  grunt.registerTask('debug', [
    'copy',
    'concat',
    'htmlbuild',
    'http',
    'watch'
  ]);
};
