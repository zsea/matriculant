'use strict';

delete process.env.http_proxy;
delete process.env.HTTP_PROXY;

var EDBO = require('./modules/edbo.js');

EDBO.then(function(session) {
  session
    .Guides
    //.SubjectsGet()
    .BenefitsGet()
    .then(function(res) {
      return res.dBenefits; 
    })
    .map(function(res) {
      return {
        id: res.Id_Benefit,
        'Name': res.BenefitName,
        'Description': res.BenefitsGroupsName
      }
    })
    .then(function(data) {;
      console.log(JSON.stringify(data, '', 2));
    });
});

// PersonEnteranceTypesGet
/*
{
  "dPersonEnteranceTypes": [
    {
      "Id_PersonEnteranceTypes": 1,
      "PersonEnteranceTypeName": "За даними сертифікату",
      "DateLastChange": "2011-06-08T16:12:45.000Z"
    },
    {
      "Id_PersonEnteranceTypes": 2,
      "PersonEnteranceTypeName": "За результатами іспитів",
      "DateLastChange": "2011-06-08T16:12:45.000Z"
    },
    {
      "Id_PersonEnteranceTypes": 3,
      "PersonEnteranceTypeName": "За даними сертифікатів та результатами іспитів",
      "DateLastChange": "2011-06-08T16:12:45.000Z"
    }
  ]
}
*/


// PersonRequestExaminationCausesGet
/*
{
  "dRequestExaminationCauses": [
    {
      "Id_PersonRequestExaminationCause": 1,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "іноземець за контрактом",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 2,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "громадяни, що звільнились із ЗСУ в цьому році",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 3,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "військовосл., що служать за контрактом (заочна ф.н.)",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 4,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "особи, що мають захворювання із переліку захворювань із Наказу 124/95 МОН/МОЗ",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 5,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "діти-інваліди (до 18 років), інваліди І-ІІ групи (від 18 років)",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 6,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "отримав ПЗСО в 2007 і раніше",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 7,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "третій предмет - творчий конкурс",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 8,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "перший предмет для не атестованих з української мови",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 9,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "Громадяни України, які в рік вступу здобули повну загальну середню освіту за кордоном",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 100,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "Вступ не по ЗНО",
      "PersonRequestExaminationCauseDescription": "Для тих кваліфікаційних рівнів вступ на які не передбачає вступо по сертифікатам ЗНО"
    },
    {
      "Id_PersonRequestExaminationCause": 101,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "Вступ з частковим набором предметів ЗНО",
      "PersonRequestExaminationCauseDescription": "Предмети котрих не має в сертифікаті, доздаються єкзаменами"
    },
    {
      "Id_PersonRequestExaminationCause": 105,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "Зараховані за співбесідою.",
      "PersonRequestExaminationCauseDescription": {}
    },
    {
      "Id_PersonRequestExaminationCause": 106,
      "Id_Language": 1,
      "PersonRequestExaminationCauseName": "Учасник міжнародних олімпіад.",
      "PersonRequestExaminationCauseDescription": {}
    }
  ]
}
*/

// PersonEducationFormsGet
/*
{
  "dPersonEducationFormsGet": [
    {
      "Id_PersonEducationForm": 1,
      "Id_PersonEducationFormName": 1,
      "PersonEducationFormName": "Денна",
      "Id_Language": 1
    },
    {
      "Id_PersonEducationForm": 2,
      "Id_PersonEducationFormName": 2,
      "PersonEducationFormName": "Заочна",
      "Id_Language": 1
    },
    {
      "Id_PersonEducationForm": 3,
      "Id_PersonEducationFormName": 3,
      "PersonEducationFormName": "Екстернат",
      "Id_Language": 1
    },
    {
      "Id_PersonEducationForm": 4,
      "Id_PersonEducationFormName": 4,
      "PersonEducationFormName": "Вечірня",
      "Id_Language": 1
    },
    {
      "Id_PersonEducationForm": 5,
      "Id_PersonEducationFormName": 5,
      "PersonEducationFormName": "Дистанційна",
      "Id_Language": 1
    },
    {
      "Id_PersonEducationForm": 6,
      "Id_PersonEducationFormName": 6,
      "PersonEducationFormName": "Інтернатура",
      "Id_Language": 1
    }
  ]
}
*/

// PersonRequestStatusTypesGet
/*
{
  "dPersonRequestStatusTypes": [
    {
      "Id_PersonRequestStatusType": 1,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "created",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "Нова заява",
      "PersonRequestStatusTypeDescription": "заяву (електронну або паперову) прийнято до розгляду у ВНЗ, заведено особову справу вступника з персональним номером та розпочато процес щодо прийняття рішення про допуск вступника до участі у конкурсному відборі"
    },
    {
      "Id_PersonRequestStatusType": 2,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "denied",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "Відмова",
      "PersonRequestStatusTypeDescription": "власника зареєстрованої заяви не допущено до участі у конкурсному відборі на підставі рішення приймальної комісії. У разі присвоєння заяві цього статусу ВНЗ зазначає причину відмови"
    },
    {
      "Id_PersonRequestStatusType": 3,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "canceled",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "Скасовано",
      "PersonRequestStatusTypeDescription": "подана заява вважається такою, що не подавалась, а факт подачі – анулюється в ЄДЕБО, якщо:– електронну заяву скасовано вступником в особистому електронному кабінеті до моменту встановлення заяві статусу «Зареєстровано у вищому навчальному закладі» або «Потребує уточнення вступником»;– заяву анульовано ВНЗ за рішенням приймальної комісії (до моменту встановлення статусу «Рекомендовано до зарахування») за умови виявлення ВНЗ технічної помилки, зробленої під час внесення даних до ЄДЕБО, з обов’язковим зазначенням причини анулювання"
    },
    {
      "Id_PersonRequestStatusType": 4,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "confirmed",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "Допущено",
      "PersonRequestStatusTypeDescription": "– електронну заяву скасовано вступником в особистому електронному кабінеті до моменту встановлення заяві статусу «Зареєстровано у вищому навчальному закладі» або «Потребує уточнення вступником»;"
    },
    {
      "Id_PersonRequestStatusType": 5,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "committed",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "Рекомендовано",
      "PersonRequestStatusTypeDescription": "– заяву анульовано ВНЗ за рішенням приймальної комісії (до моменту встановлення статусу «Рекомендовано до зарахування») за умови виявлення ВНЗ технічної помилки, зробленої під час внесення даних до ЄДЕБО, з обов’язковим зазначенням причини анулювання"
    },
    {
      "Id_PersonRequestStatusType": 6,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "rejected",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "Відхилено",
      "PersonRequestStatusTypeDescription": "вступник втратив право бути зарахованим на навчання до обраного ВНЗ у зв’язку з невиконанням вимог  Умов прийому або їх порушенням, зарахуванням на навчання до іншого навчального закладу тощо. При встановленні заяві такого статусу ВНЗ обов’язково зазначає причину виключення"
    },
    {
      "Id_PersonRequestStatusType": 7,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "submitted",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "До наказу",
      "PersonRequestStatusTypeDescription": "наказом про зарахування на навчання вступника зараховано до ВНЗ"
    },
    {
      "Id_PersonRequestStatusType": 8,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "sitereg",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "Заява надійшла з сайту",
      "PersonRequestStatusTypeDescription": "підтвердження факту подання електронної заяви до обраного вступником ВНЗ. Подана електронна заява одразу відображається в ЄДЕБО на сторінці ВНЗ"
    },
    {
      "Id_PersonRequestStatusType": 9,
      "DateLastChange": "2011-05-17T16:25:47.000Z",
      "PersonRequestStatusCode": "delayed",
      "Id_Language": 0,
      "PersonRequestStatusTypeName": "Затримано",
      "PersonRequestStatusTypeDescription": "електронну заяву прийнято до розгляду у ВНЗ, але дані стосовно вступника потребують уточнення. Після присвоєння електронній заяві цього статусу ВНЗ зобов`язаний невідкладно надіслати вступнику повідомлення з переліком даних, які потребують уточнення, та в який спосіб їх необхідно подати"
    }
  ]
}
*/

// RequestEnteranceCodesGet
/*
{
  "dRequestEnteranceCodes": [
    {
      "id_RequestEnteranceCodes": 1,
      "RequestEnteranceCodes": "101",
      "RequestEnteranceCodesName": "Особи, які вступили на основі базової загальної середньої освіти."
    },
    {
      "id_RequestEnteranceCodes": 2,
      "RequestEnteranceCodes": "102",
      "RequestEnteranceCodesName": "Особи, які вступили на основі повної загальної середньої освіти."
    },
    {
      "id_RequestEnteranceCodes": 3,
      "RequestEnteranceCodes": "103",
      "RequestEnteranceCodesName": "Особи, які вступили на основі здобутого освітньо-кваліфікаційного рівня кваліфікованого робітника відповідної спеціальності."
    },
    {
      "id_RequestEnteranceCodes": 4,
      "RequestEnteranceCodes": "104",
      "RequestEnteranceCodesName": "Особи, які вступили на основі здобутого освітньо-кваліфікаційного рівня молодшого спеціаліста відповідної спеціальності."
    },
    {
      "id_RequestEnteranceCodes": 5,
      "RequestEnteranceCodes": "105",
      "RequestEnteranceCodesName": "Інваліди І та II груп та діти-інваліди віком до 18 років, яким не протипоказане навчання за обраним напрямом (спеціальністю)."
    },
    {
      "id_RequestEnteranceCodes": 6,
      "RequestEnteranceCodes": "106",
      "RequestEnteranceCodesName": "Діти-сироти та діти, позбавлені батьківського піклування, а також особи з їх числа, віком до 23 років."
    },
    {
      "id_RequestEnteranceCodes": 7,
      "RequestEnteranceCodes": "107",
      "RequestEnteranceCodesName": "Особи, яким відповідно до Закону України \"Про статус і соціальний захист громадян, які постраждали внаслідок Чорнобильської катастрофи\" надане право пільгового зарахування."
    },
    {
      "id_RequestEnteranceCodes": 8,
      "RequestEnteranceCodes": "108",
      "RequestEnteranceCodesName": "Особи, які зараховані на умовах цільового прийому згідно з постановою Кабінету Міністрів України від 29.06.1999 р. № 1159 \"Про підготовку фахівців для роботи в сільській місцевості\"."
    },
    {
      "id_RequestEnteranceCodes": 9,
      "RequestEnteranceCodes": "109",
      "RequestEnteranceCodesName": "Учасники міжнародних олімпіад."
    },
    {
      "id_RequestEnteranceCodes": 10,
      "RequestEnteranceCodes": "110",
      "RequestEnteranceCodesName": "Призери IV етапу Всеукраїнських учнівських олімпіад з базових дисциплін."
    },
    {
      "id_RequestEnteranceCodes": 11,
      "RequestEnteranceCodes": "111",
      "RequestEnteranceCodesName": "Призери III етапу Всеукраїнських конкурсів-захистів науково-дослідницьких робіт учнів-членів Малої академії наук України."
    },
    {
      "id_RequestEnteranceCodes": 12,
      "RequestEnteranceCodes": "112",
      "RequestEnteranceCodesName": "Особи з числа професійно орієнтованої молоді, які в рік вступу закінчили підготовчі курси (відділення, факультети довузівської підготовки) вищого навчального закладу, для вступу на основі повної загальної середньої освіти до цього вищого навчального закладу для навчання на природничо-математичних та інженерно-технічних напрямах підготовки (спеціальностях)."
    },
    {
      "id_RequestEnteranceCodes": 13,
      "RequestEnteranceCodes": "113",
      "RequestEnteranceCodesName": "Іноземці, зараховані за квотою МОНмолодьспорт."
    },
    {
      "id_RequestEnteranceCodes": 14,
      "RequestEnteranceCodes": "114",
      "RequestEnteranceCodesName": "Іноземні громадяни."
    },
    {
      "id_RequestEnteranceCodes": 15,
      "RequestEnteranceCodes": "115",
      "RequestEnteranceCodesName": "Особи, яким Законом України \"Про статус ветеранів війни, гарантії їх соціального захисту\" надано право на вступ до вищого навчального закладу поза конкурсом."
    },
    {
      "id_RequestEnteranceCodes": 16,
      "RequestEnteranceCodes": "116",
      "RequestEnteranceCodesName": "Особи, яким Законом України \"Про підвищення престижності шахтарської праці\" надано право на вступ до вищого навчального закладу поза конкурсом."
    },
    {
      "id_RequestEnteranceCodes": 17,
      "RequestEnteranceCodes": "117",
      "RequestEnteranceCodesName": "Діти військовослужбовців Збройних сил України, інших військових формувань, працівників правоохоронних органів, які загинули під час виконання службових обов'язків."
    },
    {
      "id_RequestEnteranceCodes": 18,
      "RequestEnteranceCodes": "118",
      "RequestEnteranceCodesName": "Особи з числа членів сімей шахтарів та гірничорятувальників, що загинули внаслідок аварії на орендному підприємстві \"Шахта імені О.Ф. Засядька\"."
    },
    {
      "id_RequestEnteranceCodes": 19,
      "RequestEnteranceCodes": "119",
      "RequestEnteranceCodesName": "Особи, яким відповідно до Закону України «Про основи соціальної захищеності інвалідів в Україні» надане право першочергового зарахування."
    },
    {
      "id_RequestEnteranceCodes": 20,
      "RequestEnteranceCodes": "120",
      "RequestEnteranceCodesName": "Особи, яким відповідно до Указу Президента України від 21.02.2002 № 157 «Про додаткові заходи щодо посилення турботи про захисників Вітчизни, їх правового і соціального захисту, поліпшення військово-патріотичного виховання молоді» надане право першочергового зарахування."
    },
    {
      "id_RequestEnteranceCodes": 21,
      "RequestEnteranceCodes": "121",
      "RequestEnteranceCodesName": "Особи, яким відповідно до Закону України «Про охорону дитинства» надане право першочергового зарахування."
    },
    {
      "id_RequestEnteranceCodes": 22,
      "RequestEnteranceCodes": "122",
      "RequestEnteranceCodesName": "Особи, яким відповідно до Указу Президента України від 12.09.2007 № 849 «Про рішення Ради національної безпеки і оборони України від 4 вересня 2007 року «Про основні напрями фінансового забезпечення заходів щодо підвищення життєвого рівня населення у 2008 році» надане право першочергового зарахування."
    },
    {
      "id_RequestEnteranceCodes": 23,
      "RequestEnteranceCodes": "123",
      "RequestEnteranceCodesName": "Особи, яким відповідно до Закону України «Про соціальний і правовий захист військовослужбовців та членів їх сімей» надане право першочергового зарахування при вступі до вищих військових навчальних закладів та військових навчальних підрозділів вищих навчальних закладів."
    },
    {
      "id_RequestEnteranceCodes": 24,
      "RequestEnteranceCodes": "124",
      "RequestEnteranceCodesName": "Випускники середньої загальноосвітньої школи, які нагороджені золотою (срібною) медаллю."
    },
    {
      "id_RequestEnteranceCodes": 25,
      "RequestEnteranceCodes": "125",
      "RequestEnteranceCodesName": "Випускники основної школи, які мають свідоцтво про базову загальну середню освіту з відзнакою."
    },
    {
      "id_RequestEnteranceCodes": 26,
      "RequestEnteranceCodes": "126",
      "RequestEnteranceCodesName": "Особи з обмеженими фізичними можливостями (інваліди І та ІІ груп, діти-інваліди), які мали право брати участь у конкурсі за результатами вступних екзаменів з конкурсних предметів у вищому навчальному закладі або за результатами зовнішнього незалежного оцінювання за їх вибором."
    },
    {
      "id_RequestEnteranceCodes": 27,
      "RequestEnteranceCodes": "127",
      "RequestEnteranceCodesName": "Військовослужбовці Збройних Сил України, інших утворених відповідно до законів України військових формувань, а також правоохоронних органів спеціального призначення та Державної спеціальної служби транспорту, які проходять військову строкову службу."
    },
    {
      "id_RequestEnteranceCodes": 28,
      "RequestEnteranceCodes": "128",
      "RequestEnteranceCodesName": "Громадяни України, звільнені зі строкової військової служби в рік вступу до вищого навчального закладу."
    },
    {
      "id_RequestEnteranceCodes": 29,
      "RequestEnteranceCodes": "129",
      "RequestEnteranceCodesName": "Військовослужбовці рядового, сержантського та старшинського складу, які проходять військову службу за контрактом."
    },
    {
      "id_RequestEnteranceCodes": 30,
      "RequestEnteranceCodes": "130",
      "RequestEnteranceCodesName": "Особи, які проживають на території населеного пункту, якому в установленому порядку надано статус гірського."
    },
    {
      "id_RequestEnteranceCodes": 31,
      "RequestEnteranceCodes": "131",
      "RequestEnteranceCodesName": "Особи, які мають захворювання, вказані в переліку захворювань, що можуть бути перешкодою для проходження громадянами зовнішнього незалежного оцінювання, встановленому Міністерством освіти і науки України та Міністерством охорони здоров’я України, для яких Український центр оцінювання якості освіти не може забезпечити проходження незалежного зовнішнього оцінювання навчальних досягнень."
    },
    {
      "id_RequestEnteranceCodes": 32,
      "RequestEnteranceCodes": "132",
      "RequestEnteranceCodesName": "Особи, які здобули повну загальну середню освіту у 2007 році і раніше."
    },
    {
      "id_RequestEnteranceCodes": 33,
      "RequestEnteranceCodes": "133",
      "RequestEnteranceCodesName": "Громадяни України, які в рік вступу здобули повну загальну середню освіту за кордоном."
    },
    {
      "id_RequestEnteranceCodes": 34,
      "RequestEnteranceCodes": "134",
      "RequestEnteranceCodesName": "Особи, які не атестовані з української мови та літератури."
    },
    {
      "id_RequestEnteranceCodes": 35,
      "RequestEnteranceCodes": "135",
      "RequestEnteranceCodesName": "Особи, які подавали сертифікат Українського центра оцінювання якості освіти, кількість балів якого з непрофільних загальноосвітніх предметів (предмету), визначених правилами прийому до вищого навчального закладу, була нижче 124 балів за умови, якщо кількість балів з профільних загальноосвітніх предметів (предмету), визначених правилами прийому до вищого навчального закладу, становить не нижче 170 балів з кожного предмету."
    },
    {
      "id_RequestEnteranceCodes": 36,
      "RequestEnteranceCodes": "136",
      "RequestEnteranceCodesName": "Квота МОНмолодьспорт відповідно до Указу Президента України від 25 березня 1994 року № 112/94 «Про заходи щодо розвитку економічного співробітництва областей України з суміжними прикордонними областями Російської Федерації»"
    },
    {
      "id_RequestEnteranceCodes": 37,
      "RequestEnteranceCodes": "137",
      "RequestEnteranceCodesName": "Квота відповідно до наказу Міністерства освіти і науки від 04.07.2006 року № 504 «Про організацію прийому на навчання осіб з обмеженими можливостями (вадами слуху)» (Українське товариство глухих)"
    },
    {
      "id_RequestEnteranceCodes": 38,
      "RequestEnteranceCodes": "138",
      "RequestEnteranceCodesName": "Зараховані за співбесідою."
    },
    {
      "id_RequestEnteranceCodes": 39,
      "RequestEnteranceCodes": "139",
      "RequestEnteranceCodesName": "Діти, чиї батьки загинули або стали інвалідами на вугледобувних підприємствах, при вступі на навчання за гірничими спеціальностями відповідно до Указу Президента України від 19.05.99 № 524 «Про державну допомогу дітям, які вчаться за гірничими спеціальностями і чиї батьки загинули або стали інвалідами на вугледобувних підприємствах»."
    },
    {
      "id_RequestEnteranceCodes": 40,
      "RequestEnteranceCodes": "140",
      "RequestEnteranceCodesName": "Діти, батьки яких, працівники органів внутрішніх справ, померли чи стали інвалідами І групи при виконанні службових обов'язків (у тому числі інвалідність яких пов'язана з участю в ліквідації наслідків аварії на ЧАЕС), яким надане право поза конкурсного вступу відповідно до Указу Президента України від 17 березня 1998 року № 197/98 \"Про деякі заходи щодо державної підтримки сімей з неповнолітніми дітьми працівників органів внутрішніх справ, які загинули під час виконання службових обов'язків\"."
    },
    {
      "id_RequestEnteranceCodes": 41,
      "RequestEnteranceCodes": "141",
      "RequestEnteranceCodesName": "Діти працівників органів внутрішніх справ, які загинули, померли чи стали інвалідами за обставин, не пов'язаних з виконанням службових обов'язків."
    },
    {
      "id_RequestEnteranceCodes": 42,
      "RequestEnteranceCodes": "142",
      "RequestEnteranceCodesName": "Діти працівників органів внутрішніх справ."
    },
    {
      "id_RequestEnteranceCodes": 43,
      "RequestEnteranceCodes": "143",
      "RequestEnteranceCodesName": "Особи із числа випускників училищ професійної підготовки системи МВС, які закінчили навчання з відзнакою."
    },
    {
      "id_RequestEnteranceCodes": 44,
      "RequestEnteranceCodes": "144",
      "RequestEnteranceCodesName": "Особи рядового і молодшого начальницького складу органів внутрішніх справ."
    },
    {
      "id_RequestEnteranceCodes": 45,
      "RequestEnteranceCodes": "149",
      "RequestEnteranceCodesName": "Інше."
    },
    {
      "id_RequestEnteranceCodes": 46,
      "RequestEnteranceCodes": "145",
      "RequestEnteranceCodesName": "Діти військовослужбовців або громадян, звільнених з військової служби за віком, за станом здоров`я чи у зв`язку із скороченням штатів або проведенням організаційних закодів, які мають вислугу в календарному обчисленні 20 років і більше."
    },
    {
      "id_RequestEnteranceCodes": 47,
      "RequestEnteranceCodes": "146",
      "RequestEnteranceCodesName": "Діти військовослужбовців, які стали інвалідами внаслідок захворювання, пов`язаного з проходженням військової служби."
    },
    {
      "id_RequestEnteranceCodes": 48,
      "RequestEnteranceCodes": "147",
      "RequestEnteranceCodesName": "Громадянин України, який відслужив строкову військову службу або службу за контрактом і має позитивну характеристику від командування військової частини."
    },
    {
      "id_RequestEnteranceCodes": 49,
      "RequestEnteranceCodes": "148",
      "RequestEnteranceCodesName": "Особи, яким відповідно до правил прийому до вищого навчального закладу надане право першочергового зарахування"
    }
  ]
}
*/

// QualificationsGet
/*
{
  "Id_Qualification": 1,
  "QualificationName": "Бакалавр",
  "QualificationDateLastChange": "2012-02-26T19:35:15.677Z"
},
{
  "Id_Qualification": 13,
  "QualificationName": "Бакалавр (з нормативним терміном навчання, на 2 курс)",
  "QualificationDateLastChange": "2011-06-11T10:31:13.000Z"
},
{
  "Id_Qualification": 14,
  "QualificationName": "Бакалавр (з нормативним терміном навчання, на 3 курс)",
  "QualificationDateLastChange": "2011-06-11T10:31:13.000Z"
},
{
  "Id_Qualification": 3,
  "QualificationName": "Спеціаліст",
  "QualificationDateLastChange": "2012-02-26T19:35:15.677Z"
},
{
  "Id_Qualification": 2,
  "QualificationName": "Магістр",
  "QualificationDateLastChange": "2012-02-26T19:35:15.677Z"
},
*/
