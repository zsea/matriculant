var Connection = require('./modules/connection.js'),
    db         = require('./modules/db.js')(Connection),
    Promise    = require('bluebird');

new db.Request()
  .query(function(params) {
    return params.where({
      Speciality: process.argv[3] || 1,
      Stream: process.argv[2] || 1,
      TeachingForm: 1
    }).orderBy('id');
  })
  .fetch()
  .then(function(data) {
    var number = 1;
    console.log(data.length);

    var result = data.map(function(item) {
      return item.save({ Number: number++ }, { path: true });
    });

    return Promise.all(result);
  })
  .then(function() {
    process.exit(1);
  });
