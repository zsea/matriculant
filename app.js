'use strict';
var express = require('express'),
    Connection = require('./modules/connection.js'),
    Promise = require('bluebird');

Promise.longStackTraces();

delete process.env.http_proxy;
delete process.env.HTTP_PROXY;

var httpPort = require('./package.json').httpPort;

var app = express()
  .use(require('body-parser').json({ limit: '20mb' }))
  .use(function(req, res, next) {
    if(/^\/(request|person|edbo|generate|data)\/?/.test(req.url)) {
      req.url = '/';
    }

    next();
  })
  .use(function(req, res, next) {
    if(/^\/api\//.test(req.url)) {
      res.header('Content-Type', 'charset=utf-8');
    }
    res.header('Access-Control-Allow-Origin', '*');

    next();
  })
  .use(express.static(__dirname + '/.tmp'))
  .get('/', function(req, res) {
    res.sendfile(__dirname + '/.tmp/index.html');
  })
  .get('/sign-in', function(req, res) {
    res.sendfile(__dirname + '/.tmp/sign-in.html');
  })
  .get('/rates/4', function(req, res) {
    res.sendfile(__dirname + '/assets/rates4.html');
  })
  .get('/rates/2', function(req, res) {
    res.sendfile(__dirname + '/assets/rates2.html');
  })
  .get('/rates/1', function(req, res) {
    res.sendfile(__dirname + '/assets/rates1.html');
  })
  .get('/report', function(req, res) {
    res.sendfile(__dirname + '/assets/report.html');
  });

require('./modules/koatuu-schema.js')(Connection);
require('./modules/person-schema.js')(Connection);

app.set('db', require('./modules/db.js')(Connection));

require('./modules/api/person.js')(app);
require('./modules/api/geodata.js')(app);
require('./modules/api/dictionary.js')(app);
require('./modules/api/edbo.js')(app);
require('./modules/api/request.js')(app);
require('./modules/api/data.js')(app);
require('./modules/api/info.js')(app);
require('./modules/api/report.js')(app);
require('./modules/api/authorization.js')(app);
console.log('Matriculant: server is running now!');
console.info('http://localhost:' + httpPort);

require('./modules/edbo.js')
  .then(function(session) {
    console.log('Matriculant: EDBO is ready!');
    app.set('edbo', session);
  });

app.listen(httpPort);
