'use strict';
var findAllPersons    = require('./person/find-all-persons'),
    savePersonHandler = require('./person/save-person-handler'),
    quickFind         = require('./person/quick-find'),
    findExactPerson   = require('./person/find-exact-person'),
    bulkAddPerson     = require('./person/bulk-add-person'),
    diplomFiller      = require('./person/diplom-filler'),
    addCertMark       = require('./person/add-certificate-mark'),
    setEduMark        = require('./person/set-edu-mark'),
    delCertMark       = require('./person/del-cert-mark');

module.exports = function(app) {
  app
    .post('/api/person/quick-find', quickFind.bind(null, app))
    .post('/api/person', savePersonHandler.bind(null, app))

    .get('/api/person', findAllPersons.bind(null, app))
    .get('/api/person/:id', findExactPerson.bind(null, app))

    .post('/api/person/diplom', diplomFiller.bind(null, app))
    .post('/api/person/bulk',   bulkAddPerson.bind(null, app))

    .put('/api/person/add-certificate-mark', addCertMark.bind(null, app))
    .put('/api/person/:id/set-edu-mark', setEduMark.bind(null, app))
    .delete('/api/person/certificate/:cert/mark/:mark', delCertMark.bind(null, app));
};
