'use strict';
var personCheck  = require('./edbo/person-check-handler'),
    personAdd    = require('./edbo/person-add'),
    requestCheck = require('./edbo/request-check-handler'),
    requestAdd   = require('./edbo/request-add'),
    addEduDocument = require('./edbo/add-edu-document'),
    requestLoad    = require('./edbo/request-load'),
    addCertificate = require('./edbo/add-certificate'),
    addExam        = require('./edbo/add-exam'),
    editSubStatus  = require('./edbo/edit-sub-status'),
    editOriginal   = require('./edbo/edit-original'),
    eduDocumentChangeMark = require('./edbo/edu-document-change-mark');

module.exports = function(app) {
  app
    .get('/api/edbo/person', function(req, res) {
      new (app.get('db').Person)()
        .query(function(arg) {
          arg
            .where({ Synchronized: false })

            /*.whereRaw([
              'EXISTS(',
              'SELECT * FROM "Request" r',
              '  WHERE r."Person" = "Person".id',
              '  AND r."Stream" = 4',
              ')'
            ].join(' '))*/

            /*.whereRaw([
              'NOT EXISTS(',
              'SELECT * FROM "EduDocument" ed',
              '  WHERE ed."Person" = "Person".id',
              '  AND "Series" IS NULL',
              ')'
            ].join(' '))*/

            .orderBy('id')
            .limit(100);
        })
        .fetch()
        .then(function(data) {
          res.json(data);
        })
        .catch(function(err) {
          if(err) {
            console.log(err.stack || err);
          }

          res.status(500);
          res.end();
        });
    })

    .get('/api/edbo/request', function(req, res) {
      var stream = req.param('Stream'),
          form   = req.param('TeachingForm'),
          speciality = req.param('Speciality');

      new (app.get('db').Request)()
        .query(function(arg) {
          var params = { Synchronized: false };

          if(stream) {
            params.Stream = stream;
          }
          if(form) {
            params.TeachingForm = form;
          }
          if(speciality) {
            params.Speciality = speciality;
          }

          arg.where(params)
            .andWhere('Stream', '<>', 3)
            .orderBy('id')
            .limit(100);
        })
        .fetch()
        .then(function(data) {
          if(!data.length) {
            throw undefined;
          }

          return data.load([ 'Person', 'SubRequests' ]);
        })
        .then(function(data) {
          res.json(data);
        })
        .catch(function(err) {
          if(err) {
            console.log(err.stack || err);
            return res.json(500, err.stack || err);
          }

          res.json([]);
        });
    })

    .delete('/api/edbo/person/:id', function(req, res) {
      return new(app.get('db').PersonModel)({ id: req.param('id') })
        .fetch()
        .then(function(data) {
          if(!data) {
            throw undefined;
          }

          return data.save({ Synchronized: true }, { patch: true });
        })
        .then(function() {
          res.json({ success: true });
        })
        .catch(function(err) {
          if(err) {
            console.log(err.stack || err);
            return res.json(500, err.stack || err);
          }

          res.json({ success: false });
        });
    })
    .delete('/api/edbo/request/:id', function(req, res) {
      return new(app.get('db').RequestModel)({ id: req.param('id') })
        .fetch()
        .then(function(data) {
          if(!data) {
            throw undefined;
          }

          return data.save({ Synchronized: true }, { patch: true });
        })
        .then(function() {
          res.json({ success: true });
        })
        .catch(function(err) {
          if(err) {
            console.log(err.stack || err);
            return res.json(500, err.stack || err);
          }

          res.json({ success: false });
        });
    })

    .put ('/api/edbo/person/:id/edu-document', addEduDocument.bind(null, app))
    .put ('/api/edbo/person/:id/certificate',  addCertificate.bind(null, app))
    .put ('/api/edbo/person/:id/edu-document-mark',
          eduDocumentChangeMark.bind(null, app))

    .put ('/api/edbo/request/:id/sub-status', editSubStatus.bind(null, app))
    .put ('/api/edbo/request/:id/original', editOriginal.bind(null, app))
    .post('/api/edbo/request/:id/exam', addExam.bind(null, app))
    .get ('/api/edbo/request/load', requestLoad.bind(null, app))

    .post('/api/edbo/person/:id',  personAdd.bind(null, app))
    .post('/api/edbo/request/:id', requestAdd.bind(null, app))
    .get ('/api/edbo/person/:id',  personCheck.bind(null, app))
    .get ('/api/edbo/request/:id', requestCheck.bind(null, app));
};
