'use strict';
var Promise = require('bluebird');

module.exports = function(app) {
  app.get('/api/report/:speciality/:teachingForm/:passed', function(req, res) {
    var knex         = app.get('db')._connection.knex,
        speciality   = req.param('speciality'),
        teachingForm = +req.param('teachingForm'),
        passed       = +req.param('passed');

    if(speciality == 0) {
      speciality = undefined;
    }

    if(!teachingForm) {
      teachingForm = 1;
    }

    var counts = {
      PaperCount:     getPaperCount(knex('Request'), speciality, teachingForm, passed),
      EZCount:        getEZCount(knex('Request'), speciality, teachingForm, passed),
      Benefit1Count:  getBenefitCount(knex('Request'), speciality, teachingForm, passed, 1),
      Benefit1Groups: getBenefitGroups(knex, speciality, teachingForm, passed, 1),
      Benefit2Count:  getBenefitCount(knex('Request'), speciality, teachingForm, passed, 2),
      Benefit2Groups: getBenefitGroups(knex, speciality, teachingForm, passed, 2),
      ZNOSubjects:    getZNOSubjects(knex, speciality, teachingForm, passed),
      OriginalsCount: getOriginalsCount(knex('Request'), speciality, teachingForm, passed),
      MaleCount:      getMaleCount(knex('Request'), speciality, teachingForm, passed),
      HostelCount:    getHostelCount(knex('Request'), speciality, teachingForm, passed),
      CoursesCount:   getCoursesCount(knex('Request'), speciality, teachingForm, passed)
    };

    if(passed === 1) {
      counts.Status7Count   = getStatus7Count(knex('Request'), speciality, teachingForm);
      counts.BudgetCapacity = getBudgetCapacityCount(knex('SpecialityCapacity'), speciality, teachingForm);
    }

    Promise.props(counts)
    .then(function(result) {
      result.TotalCount = result.PaperCount + result.EZCount;
      res.json(result);
    });
  });
};

function getPaperCount(knex, speciality, teachingForm, passed) {
  var selector = { Stream: 1, TeachingForm: teachingForm, EZ: false };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .count('id')
    .where(selector)
    .then(function(result) {
      return +result[0].count;
    });
}

function getEZCount(knex, speciality, teachingForm, passed) {
  var selector = { Stream: 1, TeachingForm: teachingForm, EZ: true };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .count('id')
    .where(selector)
    .then(function(result) {
      return +result[0].count;
    });
}

function getBenefitCount(knex, speciality, teachingForm, passed, type) {
  var selector = { Stream: 1, TeachingForm: teachingForm, 'BenefitType.Strength': type };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .select('Request.id')
    .join('Benefit', 'Request.Person', '=', 'Benefit.Person')
    .join('BenefitType', 'Benefit.Type', '=', 'BenefitType.id')
    .where(selector)
    .groupBy('Request.id')
    .then(function(result) {
      return result.length;
    });
}

function getBenefitGroups(knex, speciality, teachingForm, passed, type) {
  var selector = { Stream: 1, TeachingForm: teachingForm, 'BenefitType.Strength': type };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex('Request')
    .select(knex.raw('"BenefitType"."Name" as "Name", COUNT("Request".id) as "Count"'))
    .join('Benefit', 'Request.Person', '=', 'Benefit.Person')
    .join('BenefitType', 'Benefit.Type', '=', 'BenefitType.id')
    .where(selector)
    .groupBy('BenefitType.id', 'BenefitType.Name')
    .then(function(result) {
      result.forEach(function(row) {
        row.Count = +row.Count;
      });

      return result;
    });
}

function getZNOSubjects(knex, speciality, teachingForm, passed) {
  var selector = { Stream: 1, TeachingForm: teachingForm };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex('Request')
    .select(knex.raw('"CertificateSubject"."Name" as "Name", COUNT("Request".id) as "Count"'))
    .join('CertificateMark', function() {
      this.on('Request.CertificateMark1', '=', 'CertificateMark.id')
          .orOn('Request.CertificateMark2', '=', 'CertificateMark.id')
          .orOn('Request.CertificateMark3', '=', 'CertificateMark.id');
    })
    .join('CertificateSubject', 'CertificateMark.Subject', '=', 'CertificateSubject.id')
    .where(selector)
    .groupBy('CertificateSubject.id', 'CertificateSubject.Name')
    .then(function(result) {
      result.forEach(function(row) {
        row.Count = +row.Count;
      });

      return result;
    });
}

function getOriginalsCount(knex, speciality, teachingForm, passed) {
  var selector = { Stream: 1, TeachingForm: teachingForm, Original: true };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .count('id')
    .where(selector)
    .then(function(result) {
      return +result[0].count;
    });
}

function getMaleCount(knex, speciality, teachingForm, passed) {
  var selector = { Stream: 1, TeachingForm: teachingForm, 'Person.Sex': 1 };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .count('Request.id')
    .join('Person', 'Person.id', '=', 'Request.Person')
    .where(selector)
    .then(function(result) {
      return +result[0].count;
    });
}

function getHostelCount(knex, speciality, teachingForm, passed) {
  var selector = { Stream: 1, TeachingForm: teachingForm, 'Person.NeedHostel': true };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .count('Request.id')
    .join('Person', 'Person.id', '=', 'Request.Person')
    .where(selector)
    .then(function(result) {
      return +result[0].count;
    });
}

function getCoursesCount(knex, speciality, teachingForm, passed) {
  var selector = { Stream: 1, TeachingForm: teachingForm, Courses: true };
  if(passed === 1) {
    selector.Status = 7;
  }
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .count('id')
    .where(selector)
    .then(function(result) {
      return +result[0].count;
    });
}

function getStatus7Count(knex, speciality, teachingForm, passed) {
  var selector = { Stream: 1, TeachingForm: teachingForm, Status: 7 };
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .count('id')
    .where(selector)
    .then(function(result) {
      return +result[0].count;
    });
}

function getBudgetCapacityCount(knex, speciality, teachingForm, passed) {
  var selector = { TeachingForm: teachingForm, SubRequestType: 0 };
  if(speciality) {
    selector.Speciality = speciality;
  }

  return knex
    .select('BudgetCapacity', 'Speciality')
    .where(selector)
    .then(function(result) {
      if(result.length > 1) {
        var sum = 0;
        result.forEach(function(e) {
          if(e.Speciality < 19)
            sum += e.BudgetCapacity;
        });
        return sum;
      }
      if(result.length > 0) {
        return +result[0].BudgetCapacity;
      }
      return 0;
    });
}
