'use strict';
var async = require('async');

module.exports = function(app) {
  app
    .get('/api/data/examination.json', function(req, res) {
      new (app.get('db').Request)()
        .query(function(arg) {
          arg
            .where('ExaminationCause', 'IS NOT', 'NULL')
            .andWhere('ExaminationCause', '<>', '0');
        })
        .fetch()
        .then(function(data) {
          return data.load([ 'Person', 'Exams', 'Exams.Subject', 'Speciality' ]);
        })
        .then(function(models) {
          models = models.toJSON();

          models = models.map(function(request) {
            request.Exams.forEach(function(exam) {
              exam.Subject = exam.Subject.Name;
            });

            return {
              Request:    request.id,
              Stream:     request.Stream,
              Speciality: request.Speciality.Name,
              Exams:      request.Exams,
              Form:       request.TeachingForm,

              Person: {
                id:         request.Person.id,
                Surname:    request.Person.Surname,
                Name:       request.Person.Name,
                Patronymic: request.Person.Patronymic
              }
            };
          });

          res.json(models);
        })
        .catch(function(err) {
          console.error(err.stack);
          res.json(500, err.stack);
        });
    })

    .post('/api/data/examination.json', function(req, res) {
      var exams = req.body;

      async.eachSeries(exams, function(item, next) {
        if(!item.value) {
          return next();
        }

        var request;

        new (app.get('db').ExaminationResultModel)({ id: item.exam })
          .fetch()
          .then(function(model) {
            if(!model) {
              console.error('Не знайдено екзамена #' + item.exam );
              throw undefined;
            }

            request = model.toJSON().Request;
            if(model.toJSON().Mark !== item.value) {
              return model.save({ Mark: item.value });
            }

            throw undefined;
          })
          .then(function() {
            return new (app.get('db').RequestModel)({ id: request }).fetch();
          })
          .then(function(model) {
            model.save({ Synchronized: false });
            next();
          })
          .catch(function(err) {
            if(err) {
              return next(err.stack || err);
            }

            next();
          });
      },
      function(err) {
        if(err) {
          console.log(err);

          res.status(500);
          return res.end();
        }

        res.json({ success: true });
      });
    });
};
