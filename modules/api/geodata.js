'use strict';
var Promise = require('bluebird');

module.exports = function(app) {

  function getLevel1Items(res) {
    app.get('db').Geodata.GeodataLevel1()
      .query({
        orderBy: 'level1'
      })
      .fetch()
      .then(function(data) {
        res.json(data);
      });
  }

  function getLevel2Items(level1, res) {
    app.get('db').Geodata.GeodataLevel2()
      .query({
        where: { level1: level1 },
        orderBy: ['level1', 'level2']
      })
      .fetch()
      .then(function(data) {
        res.json(data);
      });
  }

  function getLevel3Items(level1, level2, res) {
    app.get('db').Geodata.GeodataLevel3()
      .query({
        where: { level1: level1, level2: level2 },
        orderBy: ['level1', 'level2', 'level3']
      })
      .fetch()
      .then(function(data) {
        res.json(data);
      });
  }

  function getLevel4Items(level1, level2, level3, res) {
    app.get('db').Geodata.GeodataLevel4()
      .query({
        where: { level1: level1, level2: level2, level3: level3 },
        orderBy: ['level1', 'level2', 'level3', 'level4']
      })
      .fetch()
      .then(function(data) {
        res.json(data);
      });
  }

  app.get('/api/geodata', function(req, res) {
    var params = {
      level1: req.param('level1'),
      level2: req.param('level2'),
      level3: req.param('level3')
    };

    if(!params.level1 && !params.level2 && !params.level3) {
      return getLevel1Items(res);
    }

    if(!params.level2 && !params.level3) {
      return getLevel2Items(params.level1, res);
    }

    if(!params.level3) {
      return getLevel3Items(params.level1, params.level2, res);
    }

    getLevel4Items(params.level1, params.level2, params.level3, res);
  });

  app.get('/api/geodata/convert', function(req, res) {
    var koatuu = req.param('koatuu');
    if(!/^\d{10}$/.test(koatuu)) {
      return res.json('');
    }

    var levels = [
      parseInt(koatuu.substr(0, 2)),
      parseInt(koatuu.substr(2, 3)),
      parseInt(koatuu.substr(5, 3)),
      parseInt(koatuu.substr(8, 2))
    ];

    if(!levels[1] && !levels[2] && !levels[3]) {
      new (app.get('db').Geodata.GeodataLevel1)()
        .query()
        .where('GeodataLevel1.level1', '=', levels[0])
        .select([
          'GeodataLevel1.name as level1Name'
        ])
        .then(function(data) {
          if(data.length === 0) {
            res.end('невизначено');
            return;
          }

          res.end(data[0].level1Name);
        });

      return;
    }

    if(!levels[2] && !levels[3]) {
      new (app.get('db').Geodata.GeodataLevel2)()
        .query()
        .join('GeodataLevel1', 'GeodataLevel2.level1', '=', 'GeodataLevel1.level1')
        .where('GeodataLevel2.level1',    '=', levels[0])
        .andWhere('GeodataLevel2.level2', '=', levels[1])
        .select([
          'GeodataLevel1.name as level1Name',
          'GeodataLevel2.name as level2Name'
        ])
        .then(function(data) {
          if(data.length === 0) {
            res.end('невизначено');
            return;
          }

          res.end(data[0].level1Name + ', ' + data[0].level2Name);
        });

      return;
    }

    if(!levels[3]) {
      new (app.get('db').Geodata.GeodataLevel3)()
        .query()
        .join('GeodataLevel1', 'GeodataLevel3.level1', '=', 'GeodataLevel1.level1')
        .join('GeodataLevel2', function() {
          this.on('GeodataLevel3.level1', '=', 'GeodataLevel2.level1');
          this.on('GeodataLevel3.level2', '=', 'GeodataLevel2.level2');
        })
        .where('GeodataLevel3.level1',    '=', levels[0])
        .andWhere('GeodataLevel3.level2', '=', levels[1])
        .andWhere('GeodataLevel3.level3', '=', levels[2])
        .select([
          'GeodataLevel1.name as level1Name',
          'GeodataLevel2.name as level2Name',
          'GeodataLevel3.name as level3Name'
        ])
        .then(function(data) {
          if(data.length === 0) {
            res.end('невизначено');
            return;
          }

          res.end(data[0].level1Name + ', ' + data[0].level2Name + ', ' + data[0].level3Name);
        });

      return;
    }

    new (app.get('db').Geodata.GeodataLevel4)()
      .query()
      .join('GeodataLevel1', 'GeodataLevel4.level1', '=', 'GeodataLevel1.level1')
      .join('GeodataLevel2', function() {
        this.on('GeodataLevel4.level1', '=', 'GeodataLevel2.level1');
        this.on('GeodataLevel4.level2', '=', 'GeodataLevel2.level2');
      })
      .join('GeodataLevel3', function() {
        this.on('GeodataLevel4.level1', '=', 'GeodataLevel3.level1');
        this.on('GeodataLevel4.level2', '=', 'GeodataLevel3.level2');
        this.on('GeodataLevel4.level3', '=', 'GeodataLevel3.level3');
      })
      .where('GeodataLevel4.level1',    '=', levels[0])
      .andWhere('GeodataLevel4.level2', '=', levels[1])
      .andWhere('GeodataLevel4.level3', '=', levels[2])
      .andWhere('GeodataLevel4.level4', '=', levels[3])
      .select([
        'GeodataLevel1.name as level1Name',
        'GeodataLevel2.name as level2Name',
        'GeodataLevel3.name as level3Name',
        'GeodataLevel4.name as level4Name',
      ])
      .then(function(data) {
        if(data.length === 0) {
          res.end('невизначено');
          return;
        }

        res.end(data[0].level1Name + ', ' + data[0].level2Name + ', ' + data[0].level3Name + ', ' + data[0].level4Name);
      });
  });

  app.post('/api/geodata/search', function(req, res) {
    var query = req.body.query,
        level2 = new (app.get('db').Geodata.GeodataLevel2)(),
        level3 = new (app.get('db').Geodata.GeodataLevel3)(),
        level4 = new (app.get('db').Geodata.GeodataLevel4)();

    if(!query || query.length < 4) {
      return res.json([]);
    }

    query = query.toLocaleUpperCase();

    var level2Promise =
      level2
        .query()
        .join('GeodataLevel1', 'GeodataLevel2.level1', '=', 'GeodataLevel1.level1')
        .orderBy('GeodataLevel2.level1')
        .orderBy('GeodataLevel2.level2')
        .where('GeodataLevel2.name', 'like', '%' + query + '%')
        .select([
          'GeodataLevel1.name as level1Name',
          'GeodataLevel2.name as level2Name',
          'GeodataLevel2.level1',
          'GeodataLevel2.level2'
        ]);

    var level3Promise =
      level3
        .query()
        .join('GeodataLevel1', 'GeodataLevel3.level1', '=', 'GeodataLevel1.level1')
        .join('GeodataLevel2', function() {
          this.on('GeodataLevel3.level1', '=', 'GeodataLevel2.level1');
          this.on('GeodataLevel3.level2', '=', 'GeodataLevel2.level2');
        })
        .orderBy('GeodataLevel3.level1')
        .orderBy('GeodataLevel3.level2')
        .orderBy('GeodataLevel3.level3')
        .where('GeodataLevel3.name', 'like', '%' + query + '%')
        .select([
          'GeodataLevel1.name as level1Name',
          'GeodataLevel2.name as level2Name',
          'GeodataLevel3.name as level3Name',
          'GeodataLevel3.level1',
          'GeodataLevel3.level2',
          'GeodataLevel3.level3'
        ]);

      var level4Promise =
      level4
        .query()
        .join('GeodataLevel1', 'GeodataLevel4.level1', '=', 'GeodataLevel1.level1')
        .join('GeodataLevel2', function() {
          this.on('GeodataLevel4.level1', '=', 'GeodataLevel2.level1');
          this.on('GeodataLevel4.level2', '=', 'GeodataLevel2.level2');
        })
        .join('GeodataLevel3', function() {
          this.on('GeodataLevel4.level1', '=', 'GeodataLevel3.level1');
          this.on('GeodataLevel4.level2', '=', 'GeodataLevel3.level2');
          this.on('GeodataLevel4.level3', '=', 'GeodataLevel3.level3');
        })
        .orderBy('GeodataLevel4.level1')
        .orderBy('GeodataLevel4.level2')
        .orderBy('GeodataLevel4.level3')
        .orderBy('GeodataLevel4.level4')
        .where('GeodataLevel4.name', 'like', '%' + query + '%')
        .select([
          'GeodataLevel1.name as level1Name',
          'GeodataLevel2.name as level2Name',
          'GeodataLevel3.name as level3Name',
          'GeodataLevel4.name as level4Name',
          'GeodataLevel4.level1',
          'GeodataLevel4.level2',
          'GeodataLevel4.level3',
          'GeodataLevel4.level4'
        ]);

    Promise
      .all([ level2Promise, level3Promise, level4Promise ])
      .then(function(data) {
        res.json(data[0].concat(data[1]).concat(data[2]));
      });
  });

};
