'use strict';
var Promise = require('bluebird');

module.exports = function saveBenefits(app, person, benefits) {
  benefits = benefits.map(function(benefit) {
    return {
      Person: person,
      Type:   benefit
    };
  });

  return Promise.all(app.get('db').Benefit.forge(benefits).invoke('save'));
};
