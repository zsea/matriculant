'use strict';
var savePerson = require('./save-person');

module.exports = function savePersonHandler(app, req, res) {
  savePerson(app, req.body)
    .then(function(id) {
      res.json({ success: true, id: id });
    })
    .catch(function(err) {
      console.error(err.stack);
      res.json({ success: false }, 500);
    });
};
