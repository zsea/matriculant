'use strict';

module.exports = function setEduMark(app, req, res) {
  var id          = req.param('id'),
      eduDocument = req.body.EduDocument,
      mark        = req.body.Mark;

  new (app.get('db').EduDocumentModel)({ id: eduDocument })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.save({ Mark: mark }, { patch: true });
    })
    .then(function() {
      return new (app.get('db').PersonModel)({ id: id }).fetch();
    })
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.save({ Synchronized: false }, { patch: true });
    })
    /*.then(function(model) {
      return model.load([
        'Person',
        'Speciality'
      ]);
    })*/
    .then(function(result) {
      /*result = result.toJSON();
      res.json(result);*/
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err);
      }

      res.json(404, {});
    });
};
