'use strict';

module.exports = function findAllPersons(app, req, res) {
  var filter = req.param('filter') || '',
      offset = req.param('offset') || 0,
      limit  = req.param('limit')  || 30;

  // shouldn't cache
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.header('Pragma', 'no-cache');
  res.header('Expires', 0);

  var parse = function(str) {
    str = +str;
    if(str != str) {
      return -1;
    }

    return str;
  };

  var chunks = filter.split(' ');

  var cyrillicToLatin = function(val) {
    var latinKeyboard    = "qwertyuiop[]asdfghjkl;'zxcvbnm,.",
        cyrillicKeyboard = "йцукенгшщзхїфівапролджєячсмитьбю",
        ret = "",
        flag;
    val = val.toLowerCase();
    for(var i in val) {
      flag = false;
      for(var j in latinKeyboard)
        if(val[i] == latinKeyboard[j] || val[i] == cyrillicKeyboard[j]) {
          ret += cyrillicKeyboard[j];
          flag = true;
        }
      if(!flag)
        return val;
    }
    return ret;
  };

  var query = app.get('db')._connection.knex('Person')
    .join('Document',    'Person.id', '=', 'Document.Person')
    .join('EduDocument', 'Person.id', '=', 'EduDocument.Person')

    .where  ('Person.id', '=', parse(filter))
    .orWhere('Person.Surname', 'ilike', '%' + cyrillicToLatin(chunks[0]) + '%');

  if(chunks[1]) {
    query = query.andWhere('Person.Name', 'ilike', '%' + cyrillicToLatin(chunks[1]) + '%');
  }
  if(chunks[2]) {
    query = query.andWhere('Person.Patronymic', 'ilike', '%' + cyrillicToLatin(chunks[2]) + '%');
  }

  query
    .orWhere('Document.Number',    'ilike', filter + '%')
    .orWhere('EduDocument.Number', 'ilike', filter + '%')

    .distinct('Person.id')
    .limit(+limit)
    .offset(+offset)
    .orderBy('Person.id', 'DESC')

    .then(function(data) {
      return data.map(function(elem) {
        return elem.id;
      });
    })
    .then(function(ids) {
      if(ids.length === 0) {
        return [];
      }

      var selector = function(params) {
        params.whereIn('id', ids).orderBy('id', 'DESC');
      };
      return new (app.get('db').Person)().query(selector).fetch();
    })

    // final
    .then(function(persons) {
      if(!persons.toJSON) {
        throw undefined;
      }

      return persons.load(['Requests', 'Requests.Speciality', 'EduDocuments', 'Benefits']);
    })
    .then(function(persons) {
      var pers = persons.toJSON();
      pers.forEach(function(e) {
        e.BenefitsCount = e.Benefits.filter(function(i) {
          return i.Type != 39;
        }).length;
      });
      return res.json(pers);
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack);
        res.json(500, []);
      }
      else {
        res.json([]);
      }
    });
};
