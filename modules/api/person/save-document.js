'use strict';

module.exports = function saveDocuments(app, person, document) {
  return app.get('db').DocumentModel.forge({
    Person:  person,
    Type:    document.Type,
    Series:  document.id.Series,
    Number:  document.id.Number,
    Date:    document.Date,
    GivenBy: document.GivenBy
  })
  .save();
};
