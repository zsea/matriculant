'use strict';
var Promise = require('bluebird');

module.exports = function diplomFiller(app, req, res) {
  new (app.get('db').Person)()
  .query(function(arg) {
    arg.where({
      Surname:    req.body.Surname,
      Name:       req.body.Name,
      Patronymic: req.body.Patronymic
    });
  })
  .fetch()
  .then(function(model) {
    model = model.toJSON();
    if(model.length !== 1) {
      res.json({ success: false, found: false });
      throw undefined;
    }

    return new (app.get('db').EduDocumentModel)({
      Person: model[0].id,
      Type:   11,
      Series: null,
      Number: null
    }).fetch();
  })
  .then(function(document) {
    if(!document) {
      res.json({ success: true, exist: true });
      throw undefined;
    }

    var data = {
      Mark:   req.body.Mark,
      Series: req.body.Series,
      Number: req.body.Number,
      Date:   new Date(new Date().getFullYear(), 7 - 1, 1) // month - 1
    };

    return document.save(data, { path: true });
  })
  .then(function() {
    res.json({ success: true });
  })
  .catch(function(err) {
    if(err) {
      console.error(err.stack);
    }
  });
};
