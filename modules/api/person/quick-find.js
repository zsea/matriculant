'use strict';
var edboFindPerson = require('../edbo/person-find');

module.exports = function edboQuickFind(app, req, res) {
  if(!app.get('edbo')) {
    res.json(500, { found: false });

    console.error('EDBO connection is not established!');
    return;
  }

  var findOpts = {
    FIOMask: req.body.Surname + ' ' + req.body.Name + ' ' + req.body.Patronymic,
    //DocumentSeries: req.body.Document.id.Series,
    DocumentNumber: req.body.Document.id.Number,
    DocumentSeries: '', // bug?
    Ids_DocumentTypes: '3', // only passport
    Hundred: 1,
    PersonCodeU: '',
    Filters: ''
  };
  app.get('edbo')
    .Person
    .PersonsFind2(findOpts)
    .then(function(person) {
      if(!person.dPersonsFind2 || person.dPersonsFind2.length === 0) {
        return { found: false };
      }

      return edboFindPerson(app, person.dPersonsFind2[0].PersonCodeU);
    })
    .then(function(result) {
      if(result.Document) {
        result.Document.id = {
          Series: result.Document.Series,
          Number: result.Document.Number
        };
      }
      if(result.EduDocuments) {
        result.EduDocuments.forEach(function(document) {
          document.id = {
            Series: document.Series,
            Number: document.Number
          };
        });
      }

      res.json(result);
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack);
      }

      res.json(500, { found: false });
    });
};
