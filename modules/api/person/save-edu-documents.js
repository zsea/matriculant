'use strict';
var Promise = require('bluebird');

module.exports = function saveEduDocuments(app, person, documents) {
  documents = documents.map(function(document) {
    return app.get('db').EduDocumentModel.forge({
      Person:     person,
      Type:       document.Type,
      Series:     document.id.Series,
      Number:     document.id.Number,
      Date:       document.Date,
      GivenBy:    document.GivenBy,
      Mark:       document.Mark.toString().replace(',', '.'),
      MarkType:   document.MarkType,
      Award:      document.Award
    })
    .save();
  });

  return Promise.all(documents);
};
