'use strict';

module.exports = function delCertMark(app, req, res) {
  var cert = req.param('cert'),
      mark = req.param('mark');

  new (app.get('db').CertificateMarkModel)({
    id: mark,
    Certificate: cert
  })
  .fetch()
  .then(function(model) {
    if(!model) {
      throw undefined;
    }

    return model.destroy();
  })
  .then(function() {
    res.json({ success: true });
  })
  .catch(function() {
    res.status(404);
  });
};
