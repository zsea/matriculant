'use strict';

module.exports = function setOriginal(app, req, res) {
  var certificate = req.param('certificate'),
      mark        = req.body;

  new(app.get('db').CertificateModel)({ id: certificate })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw new Error('Неможливо знайти сертифікат id:' + certificate);
      }

      var person = model.toJSON().Person;
      return new(app.get('db').PersonModel)({ id: person }).fetch();
    })
    .then(function(person) {
      return person.save({ Synchronized: false });
    })
    .then(function() {
      return app.get('db')
        .CertificateMarkModel
        .forge({
          Certificate:  certificate,
          Subject:      mark.Subject,
          Mark:         mark.Mark.toString().replace(',', '.')
        })
        .save();
    })
    .then(function(mark) {
      res.json({ success: true, Mark: mark });
    })
    .catch(function(err) {
      res.json(500, err.stack || err);
    });
};
