'use strict';
var Promise = require('bluebird');

module.exports = function saveCertificates(app, person, certificates) {
  certificates = certificates.map(function(certificate) {
    return app.get('db').CertificateModel.forge({
      Person:     person,
      Year:       certificate.Year,
      Number:     certificate.Number,
      Pin:        certificate.Pin
    })
    .save()

    // save marks
    .then(function(model) {
      var marks = certificate.marks.map(function(mark) {
        return app.get('db').CertificateMarkModel.forge({
          Certificate:  model.id,
          Subject:      mark.Subject,
          Mark:         mark.Mark.toString().replace(',', '.')
        })
        .save();
      });

      return Promise.all(marks);
    });
  });

  return Promise.all(certificates);
};
