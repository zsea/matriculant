'use strict';
var saveCertificates = require('./save-certificates'),
    saveEduDocuments = require('./save-edu-documents'),
    saveBenefits     = require('./save-benefits'),
    saveDocument     = require('./save-document');

module.exports = function savePerson(app, person) {
  var PersonId;

  return app.get('db').PersonModel.forge({
      Name:         person.Name,
      Surname:      person.Surname,
      Patronymic:   person.Patronymic,
      BirthDate:    person.BirthDate,
      BirthPlace:   person.BirthPlace,
      LivingPlace:  person.LivingPlace,
      StreetType:   person.StreetType,
      LivingStreet: person.LivingStreet,
      PhoneNumber:  person.PhoneNumber,
      PostCode:     person.PostCode,
      IPN:          person.IPN,
      Sex:          person.Sex,
      IsForeigner:  person.IsForeigner,
      NeedHostel:   person.NeedHostel
    })
    .save()
    .then(function(model) {
      PersonId = model.id;
    })

    .then(function() {
      return saveCertificates(app, PersonId, person.Certificates);
    })
    .then(function() {
      return saveEduDocuments(app, PersonId, person.EduDocuments);
    })
    .then(function() {
      return saveBenefits(app, PersonId, person.Benefits);
    })
    .then(function() {
      return saveDocument(app, PersonId, person.Document);
    })
    .then(function() {
      return PersonId;
    })

    .catch(function(err) {
      var instance = new (app.get('db').PersonModel)({ id: PersonId });
      instance.destroy();

      throw err;
    });
};
