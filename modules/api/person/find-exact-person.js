'use strict';

module.exports = function findAllPersons(app, req, res) {
  var id = req.param('id');

  new (app.get('db').PersonModel)({ id: id })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw 'end';
      }

      return model.load([
        'Document', 'EduDocuments', 'Benefits',
        'Certificates', 'Certificates.marks', 'Requests', 'Requests.Speciality'
      ]);
    })
    .then(function(result) {
      result = result.toJSON();

      // adopt to frontend
      result.Document.id_ = result.Document.id;
      result.Document.id = {
        Series: result.Document.Series,
        Number: result.Document.Number
      };
      result.EduDocuments.forEach(function(document) {
        document.id_ = document.id;
        document.id = {
          Series: document.Series,
          Number: document.Number
        };
      });
      result.Benefits = result.Benefits.map(function(benefit) {
        return benefit.Type;
      });

      res.json(result);
    })
    .catch(function() {
      res.json({}, 404);
    });
};
