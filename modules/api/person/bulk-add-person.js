'use strict';
var Promise        = require('bluebird'),
    edboFindPerson = require('../edbo/person-find'),
    savePerson     = require('./save-person'),
    saveRequest    = require('../request/save-request');

module.exports = function edboQuickFind(app, req, res) {
  if(!app.get('edbo')) {
    res.json(500, { found: false });

    console.error('EDBO connection is not established!');
    return;
  }

  new (app.get('db').PersonModel)({
    Surname:    req.body.Surname,
    Name:       req.body.Name,
    Patronymic: req.body.Patronymic
  })
  .fetch()
  .then(function(model) {
    if(model) {
      res.json({ success: true, exists: true });
      throw undefined;
    }

    var findOpts = {
      FIOMask: req.body.Surname + ' ' + req.body.Name + ' ' + req.body.Patronymic,
      DocumentNumber: '',
      DocumentSeries: '',
      Ids_DocumentTypes: '',
      Hundred: 1,
      PersonCodeU: '',
      Filters: ''
    };

    return app.get('edbo').Person.PersonsFind2(findOpts);
  })
  .then(function(persons) {
    if(!persons.dPersonsFind2) {
      res.json({ found: false });
      throw undefined;
    }

    persons = persons.dPersonsFind2.map(function(person) {
      return app.get('edbo')
        .Person
        .PersonEducationsGet({
          PersonCodeU: person.PersonCodeU,
          Id_PersonEducation: 0,
          Id_PersonEducationType: 3,
          Filters: ''
        });
    });

    return Promise.all(persons);
  })
  .then(function(educations) {
    educations = educations
      .map(function(education) {
        return education.dPersonEducations;
      })
      .filter(function(education) {
        return education !== undefined;
      });

    if(educations.length === 0) {
      res.json({ found: false });
      throw undefined;
    }

    var persons = educations
      .map(function(item) {
        var filtered = item.filter(function(e) {
          return 'Запорізька державна інженерна академія' === e.UniversityFullName;
        });

        if(filtered.length) {
          item = filtered[0];
          var speciality = new (app.get('db').SpecialityModel)({
            Code: item.SpecClasifierCode
          })
          .fetch()
          .then(function(model) {
            return new (app.get('db').Speciality)()
              .query(function(query) {
                query.where({ Parent: model.id });
              })
              .fetch()
              .then(function(specialities) {
                specialities = specialities.toJSON();

                /*if(specialities.length > 1) {
                  specialities = [];
                }*/

                return specialities;
              });
          });

          return Promise.all([
            item.Id_PersonEducationForm,
            speciality,
            edboFindPerson(app, item.PersonCodeU)
          ]);
        }
      })
      .filter(function(item) {
        return item !== undefined;
      });

    return Promise.all(persons);
  })
  .then(function(persons) {
    if(persons.length === 0) {
      res.json({ found: false });
      throw undefined;
    }

    persons = persons.map(function(person) {
      var teachgingForm = person[0],
          specialities  = person[1];

      specialities = specialities.map(function(item) {
        return item.id;
      });

      person = person[2];
      person.Document.id = {
        Series: person.Document.Series,
        Number: person.Document.Number
      };
      person.EduDocuments.forEach(function(doc) {
        doc.id = {
          Series: doc.Series,
          Number: doc.Number
        };
      });

      return savePerson(app, person)
        .then(function(id) {
          var dipl = person.EduDocuments.filter(function(e) {
            return e.Type == 11;
          });
          if(dipl.length) {
            return app.get('db').
              _connection.knex('EduDocument')
              .where({
                Person: id,
                Type:   11
              })
              .then(function(data) {
                return data[0];
              });
          }
          else {
            return app.get('db').EduDocumentModel.forge({
              Person:     id,
              Type:       11,
              Series:     null,
              Number:     null,
              Date:       null,
              GivenBy:    'Запорізька державна інженерна академія',
              Mark:       null,
              MarkType:   5,
              Award:      0
            })
            .save();
          }
        })
        .then(function(model) {
          //model = model.toJSON();
          return {
            EduDocument:  model.id,
            Person:       model.Person,
            Specialities: specialities,
            TeachingForm: teachgingForm
          };
        });
    });

    return Promise.all(persons);
  })
  .then(function(result) {
    result = result.map(function(item) {
      var requests = item.Specialities.map(function(spec) {
        return saveRequest(app, {
          Person: item.Person,
          Type: 2,
          TeachingForm: item.TeachingForm,
          Stream: 4,

          EduDocument: item.EduDocument,
          Original: true,

          Courses: false,
          CourseMark: null,

          Exams: [
            {
              Primary: true,
              Foreign: false
            },
            {
              Primary: false,
              Foreign: true
            }
          ],
          ExaminationCause: 100,
          Speciality: spec
        });
      });

      return Promise.all(requests);
    });

    return Promise.all(result);
  })
  .then(function() {
    res.json({ success: true });
  })
  .catch(function(err) {
    if(err) {
      console.error(err.stack);
    }
  });
};
