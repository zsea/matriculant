'use strict';
var saveRequest = require('./save-request');

module.exports = function saveRequestHandler(app, req, res) {
  saveRequest(app, req.body)
    .then(function(result) {
      res.json({ success: true, id: result.id, Number: result.Number });
    })
    .catch(function(err) {
      console.error(err.stack);
      res.json({ success: false }, 500);
    });
};
