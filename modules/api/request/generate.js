'use strict';
var city    = require('../../geodata/city'),
    Promise = require('bluebird');

module.exports = function findAllRequests(app, req, res) {
  var teachingForm = req.param('TeachingForm'),
      stream       = req.param('Stream'),
      speciality   = req.param('Speciality');

  // shouldn't cache
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.header('Pragma', 'no-cache');
  res.header('Expires', 0);


  app.get('db')._connection.knex('Request')
    .where(function() {
      this.whereRaw('TRUE');

      if(teachingForm) { this.andWhere('TeachingForm', '=', teachingForm); }
      if(speciality)   { this.andWhere('Speciality',   '=', speciality); }
      if(stream)       { this.andWhere('Stream',       '=', stream); }

      //this.andWhere('Original', '=', stream); 
    })

    .distinct('Request.id')
    .orderBy('Request.id', 'DESC')
    .select('Request.id')

    .then(function(data) {
      return data.map(function(elem) {
        return elem.id;
      });
    })
    .then(function(ids) {
      if(ids.length === 0) {
        throw undefined;
      }

      var selector = function(params) {
        params.whereIn('id', ids).orderBy('id', 'DESC');
      };
      return new (app.get('db').Request)().query(selector).fetch();
    })

    // final
    .then(function(requests) {
      return requests.load([
        'Speciality',
        'Speciality.Faculty',
        'Person',
        'Person.Document',
        'Person.Benefits',
        'Person.Benefits.Type',
        'EduDocument',
        'Exams',
        'SubRequests',
        'CertificateMark1',
        'CertificateMark2',
        'CertificateMark3'
      ]);
    })
    .then(function(requests) {
      requests = requests.toJSON();

      var cities = requests.map(function(req) {
        return city(app, req.Person.LivingPlace);
      });

      return  Promise.all(cities).then(function(result) {
        result.forEach(function(city, i) {
          requests[i].Person.LivingPlace = city;
        });
      })
      .then(function() {
        return requests;
      });
    })
    .then(function(requests) {
      var crossSpecialities = requests.map(function(req) {
        return findCrossSpecialities(app, req, req.Person);
      });

      return Promise.all(crossSpecialities).then(function(result) {
        result.forEach(function(spec, i) {
          requests[i].CrossSpecialities = spec;
        });
      })
      .then(function() {
        return requests;
      });
    })
    .then(function(requests) {
      res.json(requests);
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack);
        res.json(500, []);
      }
      else {
        res.json([]);
      }
    });
};

function findCrossSpecialities(app, request, person) {
  var selector = function(params) {
    params
      .where('Person', '=', person.id)
      .andWhere('id', '<>', request.id);
  };

  return new (app.get('db').Request)()
    .query(selector)
    .fetch()
    .then(function(models) {
      if(models.length === 0) {
        throw undefined;
      }

      return models.load('Speciality');
    })
    .then(function(models) {
      return models.toJSON().map(function(req) {
        return req.Speciality.Hash;
      });
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err);
      }

      return [];
    })
}
