'use strict';

module.exports = function setOriginal(app, req, res) {
  var id    = req.param('id'),
      value = req.body.value;

  new (app.get('db').RequestModel)({ id: id })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.save({ Status: value, Synchronized: false }, { patch: true });
    })
    .then(function(model) {
      return model.load([
        'Person',
        'Exams',
        'Speciality'
      ]);
    })
    .then(function(result) {
      result = result.toJSON();
      res.json(result);
    })
    .catch(function() {
      res.json(404, {});
    });
};
