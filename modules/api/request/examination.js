'use strict';

module.exports = function findAllRequests(app, req, res) {
  var teachingForm = req.param('TeachingForm'),
      stream       = req.param('Stream'),
      speciality   = req.param('Speciality');

  // shouldn't cache
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.header('Pragma', 'no-cache');
  res.header('Expires', 0);


  app.get('db')._connection.knex('Request')
    .join('ExaminationResult', 'ExaminationResult.Request', '=', 'Request.id')
    .groupBy('Request.id', 'Request.Stream', 'Request.Speciality', 'Request.TeachingForm')
    .where   ('Request.Stream',       '=', stream)
    .andWhere('Request.Speciality',   '=', speciality)
    .andWhere('Request.TeachingForm', '=', teachingForm)
    .havingRaw('COUNT("ExaminationResult"."id") > 0')
    .distinct('Request.id')

    .then(function(data) {
      return data.map(function(elem) {
        return elem.id;
      });
    })
    .then(function(ids) {
      if(ids.length === 0) {
        throw undefined;
      }

      var selector = function(params) {
        params.whereIn('id', ids).orderBy('id');
      };
      return new (app.get('db').Request)().query(selector).fetch();
    })

    // final
    .then(function(requests) {
      return requests.load([
        'Exams',
        'Person',
        'Speciality',
        'Speciality.Faculty'
      ]);
    })
    .then(function(requests) {
      return res.json(requests.toJSON());
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack);
        res.json(500, []);
      }
      else {
        res.json([]);
      }
    });
};
