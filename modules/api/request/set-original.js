'use strict';

module.exports = function setStatus(app, req, res) {
  var id    = req.param('id'),
      value = req.body.value;

  new (app.get('db').RequestModel)({ id: id })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.save({ Original: value, Synchronized: false }, { patch: true });
    })
    .then(function(model) {
      return model.load([
        'Person',
        'Exams',
        'Speciality'
      ]);
    })
    .then(function(result) {
      result = result.toJSON();
      res.json(result);
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err);
      }

      res.json(404, {});
    });
};
