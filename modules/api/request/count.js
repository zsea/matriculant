'use strict';

module.exports = function count(app, req, res) {
  var teachingForm = req.param('TeachingForm'),
      stream       = req.param('Stream'),
      speciality   = req.param('Speciality'),
      courses      = req.param('Courses'),
      originals    = req.param('Originals');

  // shouldn't cache
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.header('Pragma', 'no-cache');
  res.header('Expires', 0);

  app.get('db')._connection.knex('Request')
    .where(function() {
      this.whereRaw('TRUE');

      if(teachingForm) { this.andWhere('TeachingForm', '=', teachingForm); }
      if(speciality)   { this.andWhere('Speciality',   '=', speciality); }
      if(stream)       { this.andWhere('Stream',       '=', stream); }
      if(courses)      { this.andWhere('Courses',      '=', true); }
      if(originals)    { this.andWhere('Original',     '=', true); }
    })

    .count('id')

    .then(function(data) {
      return res.json({ count: data[0].count });
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack);
        res.json(500, []);
      }
      else {
        res.json([]);
      }
    });
};
