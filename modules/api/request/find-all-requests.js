'use strict';

module.exports = function findAllRequests(app, req, res) {
  var offset = req.param('offset') || 0,
      limit  = req.param('limit')  || 30,
      teachingForm = req.param('TeachingForm'),
      stream       = req.param('Stream'),
      speciality   = req.param('Speciality'),
      courses      = req.param('Courses'),
      originals    = req.param('Originals');

  // shouldn't cache
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.header('Pragma', 'no-cache');
  res.header('Expires', 0);

  app.get('db')._connection.knex('Request')
    .where(function() {
      this.whereRaw('TRUE');

      if(teachingForm) { this.andWhere('TeachingForm', '=', teachingForm); }
      if(speciality)   { this.andWhere('Speciality',   '=', speciality); }
      if(stream)       { this.andWhere('Stream',       '=', stream); }
      if(courses)      { this.andWhere('Courses',      '=', true); }
      if(originals)    { this.andWhere('Original',     '=', true); }
    })


    .limit(+limit)
    .offset(+offset)

    .orderBy('Request.id', 'DESC')
    .distinct('Request.id')

    .then(function(data) {
      return data.map(function(elem) {
        return elem.id;
      });
    })
    .then(function(ids) {
      if(ids.length === 0) {
        throw undefined;
      }

      var selector = function(params) {
        params.whereIn('id', ids);

        if(originals) {
          params
            .orderBy('Stream', 'ASC')
            .orderBy('Person', 'DESC');
        }

        if(courses) {
          params.orderBy('Person', 'DESC');
        }

        params.orderBy('id', 'DESC').orderBy('Number', 'DESC');
      };
      return new (app.get('db').Request)().query(selector).fetch();
    })

    // final
    .then(function(requests) {
      return requests.load([ 'Speciality', 'Person', 'Person.Benefits' ]);
    })
    .then(function(requests) {
      var req = requests.toJSON();
      req.forEach(function(e) {
        e.BenefitsCount = e.Person.Benefits.filter(function(i) {
          return i.Type != 39;
        }).length;
      });
      return res.json(req);
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack);
        res.json(500, []);
      }
      else {
        res.json([]);
      }
    });
};
