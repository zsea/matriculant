'use strict';
var Promise = require('bluebird');

module.exports = function saveRequest(app, request) {
  var RequestId, RequestNumber, promise;

  promise = app.get('db')._connection.knex
    .raw('select CalculateRequestNumber(?, ?, ?) AS Number', [
      request.Speciality,
      request.Stream,
      request.TeachingForm
    ]);

  return promise.then(function(result) {
    if(request.Stream == 4) {
      request.Status = null;
    }
    else {
      request.Status = 4;
    }

    if(request.ExaminationCause === 0) {
      request.ExaminationCause = null;
    }

    return app.get('db').RequestModel.forge({
      Number: result.rows[0].number,
      Person: request.Person,
      Type:   request.Type,
      EZ:     !!request.EZ,

      Speciality:   request.Speciality,
      TeachingForm: request.TeachingForm,
      Stream:       request.Stream,
      Priority:     request.Priority, 

      ExaminationCause: request.ExaminationCause,
      CertificateMark1: request.CertificateMark1,
      CertificateMark2: request.CertificateMark2,
      CertificateMark3: request.CertificateMark3,

      EduDocument: request.EduDocument,
      Original:    request.Original,

      Budget: true,
      Status: request.Status,

      Courses:      request.Courses,
      CoursesMark:  request.CoursesMark,
      Synchronized: false
    })
    .save();
  })
  .then(function(model) {
    RequestId = model.id;
    RequestNumber = model.toJSON().Number;
  })

  .then(function() {
    return saveExams(app, RequestId, request.Exams);
  })
  .then(function() {
    if(request.Stream != 4) {
      return;
    }

    var subrequests = [];
    if(request.MagisterDegree) {
      subrequests.push({
        Parent: RequestId,
        Status: 4,
        Type: 2
      });
    }
    if(request.SpecialistDegree) {
      subrequests.push({
        Parent: RequestId,
        Status: 4,
        Type: 1
      });
    }

    app.get('db').SubRequest.forge(subrequests).invokeThen('save');
  })
  .then(function() {
    return {
      id:     RequestId,
      Number: RequestNumber
    };
  })

  .catch(function(err) {
    if(RequestId) {
      var instance = new (app.get('db').RequestModel)({ id: RequestId });
      instance.destroy();
    }

    throw err;
  });
};

function saveExams(app, request, exams) {
  if(exams && exams.length > 0) {
    exams.forEach(function(exam) {
      exam.Request = request;
    });

    var collection = app.get('db').ExaminationResult.forge(exams);
    return Promise.all(collection.invoke('save'));
  }
}
