'use strict';

module.exports = function setSetOriginal(app, req, res) {
  var id    = req.param('id'),
      value = req.body.value;

  new (app.get('db').SubRequestModel)({ id: id })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.save({ Status: value });
    })
    .then(function(model) {
      return new(app.get('db').RequestModel)({ id: model.toJSON().Parent })
    })
    .then(function(model) {
      return model.save({ Synchronized: false });
    })
    .then(function(model) {
      return model.load([
        'Person',
        'Exams',
        'Speciality'
      ]);
    })
    .then(function(result) {
      result = result.toJSON();
      res.json(result);
    })
    .catch(function(err) {
      if(err) {
        console.log(err.stack || err);
      }

      res.json(404, {});
    });
};
