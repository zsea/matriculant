'use strict';
var Promise = require('bluebird');

module.exports = function findExactRequest(app, req, res) {
  var id = req.param('id');

  new (app.get('db').RequestModel)({ id: id })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.load([
        'Person', 'Person.Benefits',
        'Exams',
        'Speciality',
        'SubRequests',
        'EduDocument'
      ]);
    })
    .then(function(result) {
      result = result.toJSON();

      return Promise.props({
        request: result,
        cert1: findCertificateMark(app, result.CertificateMark1),
        cert2: findCertificateMark(app, result.CertificateMark2),
        cert3: findCertificateMark(app, result.CertificateMark3),
      });
    })
    .then(function(result) {
      result.request.CertificateMark1 = result.cert1;
      result.request.CertificateMark2 = result.cert2;
      result.request.CertificateMark3 = result.cert3;
      result.request.BenefitsCount = result.request.Person.Benefits.filter(function(i) {
        return i.Type != 39;
      }).length;
      res.json(result.request);
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err);
      }

      res.json(404, {});
    });
};

function findCertificateMark(app, id) {
  if(!id) {
    return undefined;
  }

  return new(app.get('db').CertificateMarkModel)({ id: id })
    .fetch()
    .then(function(model) {
      return model.toJSON();
    });
}
