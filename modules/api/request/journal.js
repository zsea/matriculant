'use strict';
var city    = require('../../geodata/city'),
    Promise = require('bluebird');

var GROUP_SIZE = 7;

module.exports = function journal(app, req, res) {
  var teachingForm = req.param('TeachingForm'),
      stream       = req.param('Stream'),
      speciality   = req.param('Speciality'),
      date         = req.param('Date'),
      currentOffset;

  // shouldn't cache
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.header('Pragma', 'no-cache');
  res.header('Expires', 0);

  findOffset(app, stream, speciality, teachingForm, date)
    .then(function(offset) {
      currentOffset = offset;

      var selector = function(params) {
        params
          .where   ('TeachingForm', '=', teachingForm)
          .andWhere('Speciality',   '=', speciality)
          .andWhere('Stream',       '=', stream)
          .orderBy('id')
          .offset(offset);
      };
      return new (app.get('db').Request)().query(selector).fetch();
    })
    .then(function(requests) {
      return requests.load([
        'Speciality',
        'Speciality.Faculty',
        'Person',
        'Person.Document',
        'EduDocument',
        'CertificateMark1',
        'CertificateMark1.Certificate',
        'CertificateMark2',
        'CertificateMark2.Certificate',
        'CertificateMark3',
        'CertificateMark3.Certificate'
      ]);
    })
    .then(function(requests) {
      requests = requests.toJSON();

      requests.forEach(function(item, idx) {
        item.id = currentOffset + idx + 1;
      });

      var cities = requests.map(function(req) {
        return city(app, req.Person.LivingPlace);
      });

      return  Promise.all(cities).then(function(result) {
        result.forEach(function(city, i) {
          requests[i].Person.LivingPlace = city;
        });
      })
      .then(function() {
        return requests;
      });
    })
    .then(function(requests) {
      return groupItems(requests);
    })
    .then(function(requests) {
      res.json(requests);
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack);
        res.json(500, []);
      }
      else {
        res.json([]);
      }
    });
};

function findFirstEntry(app, stream, speciality, form, date) {
  return app.get('db')._connection.knex('Request')
    .select('id')
    .where   ('TeachingForm', '=',  form)
    .andWhere('Speciality',   '=',  speciality)
    .andWhere('Stream',       '=',  stream)
    .andWhere('Date',         '>=', date)
    .orderBy('id')
    .limit(1)
    .then(function(item) {
      if(item.length === 0) {
        throw undefined;
      }

      return item[0].id;
    });
}

function findOffset(app, stream, speciality, form, date) {
  return findFirstEntry(app, stream, speciality, form, date)
    .then(function(id) {
      return app.get('db')._connection.knex('Request')
        .count('*')
        .where   ('TeachingForm', '=', form)
        .andWhere('Speciality',   '=', speciality)
        .andWhere('Stream',       '=', stream)
        .andWhere('id',           '<', id)
        .then(function(offset) {
          offset = offset[0].count;
          offset = Math.floor(offset / GROUP_SIZE) * GROUP_SIZE;

          return offset;
        });
    });
}

function groupItems(requests) {
  var count  = Math.ceil(requests.length / GROUP_SIZE),
      groups = new Array(count);

  for(var i = 0; i < count; ++i) {
    var offset = i * GROUP_SIZE;
    groups[i] = requests.slice(offset, offset + GROUP_SIZE);
  }

  return groups;
}
