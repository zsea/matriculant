'use strict';

module.exports = function setExamMark(app, req, res) {
  var id   = req.param('id'),
      exam = req.body.Exam,
      mark = req.body.Mark;

  new (app.get('db').ExaminationResultModel)({ id: exam })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.save({ Mark: mark }, { patch: true });
    })
    .then(function() {
      return new (app.get('db').RequestModel)({ id: id }).fetch();
    })
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.save({ Synchronized: false }, { patch: true });
    })
    .then(function(model) {
      return model.load([
        'Person',
        'Speciality'
      ]);
    })
    .then(function(result) {
      result = result.toJSON();
      res.json(result);
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err);
      }

      res.json(404, {});
    });
};
