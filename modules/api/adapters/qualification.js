'use strict';

module.exports = function(Request, SubRequest) {
  if(Request.Stream === 4 && SubRequest.Type === 1) {
    return 3;
  }

  if(Request.Stream === 4 && SubRequest.Type === 2) {
    return 2;
  }

  if(Request.Stream === 2 && SubRequest.Type === 3) { // 2 grade
    return 13;
  }

  if(Request.Stream === 2 && SubRequest.Type === 4) { // 3 grade
    return 14;
  }

  if(Request.Stream === 1) {
    return 1;
  }
}
