'use strict';

module.exports = function(qualification) {
  if(qualification == 3 || qualification == 2) {
    return 4;
  }

  if(qualification == 13 || qualification == 14) {
    return 2;
  }

  if(qualification == 1) {
    return 1;
  }
};
