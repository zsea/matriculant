'use strict';

module.exports = function(app) {
  app
    .put('/api/authorization', function(req, res) {
      new(app.get('db').UserModel)({
        Login: req.body.login,
        Hash:  req.body.hash
      })
      .fetch()
      .then(function(model) {
        if(!model) {
          throw undefined;
        }

        res.json(model.toJSON());
      })
      .catch(function(err) {
        if(err) {
          console.error(err.stack || err);
        }

        res.status(403);
        res.end();
      });
    })

    .post('/api/authorization/:id', function(req, res) {
      var id    = req.param('id'),
          hash  = req.body.hash;
      new (app.get('db').UserModel)({ id: id })
      .fetch()
      .then(function(model) {
        if(!model) {
          throw undefined;
        }

        return model.save({ Hash: hash }, { path: true });
      })
      .then(function() {
        res.json({ success: true });
      })
      .catch(function() {
        res.json(404, {});
      });
    })

    .post('/api/authorization', function(req, res) {
      new (app.get('db').UserModel)(req.body)
      .save()
      .then(function(result) {
        res.json(result.toJSON());
      })
      .catch(function() {
        res.json(500, {});
      });
    })

    .get('/api/authorization', function(req, res) {
      new(app.get('db').User)()
      .fetch()
      .then(function(models) {
        var users = models.toJSON();
        users.forEach(function(user) {
          delete user.Hash;
        });
        res.json(users);
      });
    });
};
