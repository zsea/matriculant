'use strict';
var async = require('async');

module.exports = function(app) {
  app
  .get('/api/info/speciality', function(req, res) {
    new(app.get('db').SpecialityModel)({ id: req.param('Speciality') })
      .fetch()
      .then(function(model) {
        return model.load([ 'Capacity' ]);
      })
      .then(function(model) {
        model = model.toJSON();
        var Capacity = model.Capacity;

        model.Capacity = { };
        model.Capacity.specialist = Capacity.filter(function(info) {
          return info.TeachingForm   === +req.param('TeachingForm')
              && info.SubRequestType === 1;
        });
        model.Capacity.magister = Capacity.filter(function(info) {
          return info.TeachingForm   === +req.param('TeachingForm')
              && info.SubRequestType === 2;
        });
        /*model.Capacity.course2 = Capacity.filter(function(info) {
          return info.TeachingForm   === +req.param('TeachingForm')
              && info.SubRequestType === 3;
        });
        model.Capacity.course3 = Capacity.filter(function(info) {
          return info.TeachingForm   === +req.param('TeachingForm')
              && info.SubRequestType === 4;
        });*/
        model.Capacity.general = Capacity.filter(function(info) {
          return info.TeachingForm   === +req.param('TeachingForm')
              // && info.SubRequestType === 0;
              && !info.SubRequestType;
        });

        model.Capacity.specialist = model.Capacity.specialist[0] || { };
        model.Capacity.magister   = model.Capacity.magister[0]   || { };
        model.Capacity.general    = model.Capacity.general[0]    || { };

        model.Capacity.specialist = model.Capacity.specialist.BudgetCapacity || 0;
        model.Capacity.magister   = model.Capacity.magister.BudgetCapacity   || 0;
        model.Capacity.general    = model.Capacity.general.BudgetCapacity    || 0;

        return model;
      })
      .then(function(speciality) {
        res.json(speciality);
      })
      .catch(function(err) {
        if(err) {
          console.log(err.stack || err);
        }

        res.json(500, err.stack || err);
      });
  });
};
