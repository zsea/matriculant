'use strict';

module.exports = function(app) {
  var DB = app.get('db');

  app
    .get('/api/dictionary/document-type', function(req, res) {
      var collection = new DB.DocumentType();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/edu-document-type', function(req, res) {
      var collection = new DB.EduDocumentType();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/edu-document-mark-type', function(req, res) {
      var collection = new DB.EduDocumentMarkType();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/sex', function(req, res) {
      var collection = new DB.Sex();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/certificate-subject', function(req, res) {
      var collection = new DB.CertificateSubject();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/street-type', function(req, res) {
      var collection = new DB.StreetType();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/benefit-type', function(req, res) {
      var collection = new DB.BenefitType();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/stream', function(req, res) {
      var collection = new DB.Stream();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/teaching-form', function(req, res) {
      var collection = new DB.TeachingForm();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/priority', function(req, res) {
      var collection = new DB.Priority();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/examination-cause', function(req, res) {
      var collection = new DB.ExaminationCause();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/request-type', function(req, res) {
      var collection = new DB.RequestType();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/request-status', function(req, res) {
      var collection = new DB.RequestStatus();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/speciality', function(req, res) {
      var collection = new DB.Speciality();

      collection.fetch()
        .then(function(data) {
          return data.load('Faculty');
        })
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/speciality-subject', function(req, res) {
      var collection = new DB.SpecialitySubject();

      collection.fetch()
        .then(function(data) {
          return data.load('Subject');
        })
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/edu-document-award', function(req, res) {
      var collection = new DB.EduDocumentAward();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    })

    .get('/api/dictionary/subrequest-type', function(req, res) {
      var collection = new DB.SubRequestType();

      collection.fetch()
        .then(function(data) {
          res.json(data);
        });
    });
};
