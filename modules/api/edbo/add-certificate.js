'use strict';
var edboMatch  = require('./person-match'),
    formatDate = require('./format-date'),
    Promise    = require('bluebird');

module.exports = function addCertificate(app, req, res) {
  var value     = req.body,
      person    = req.param('id'),
      direction = req.param('direction');

  var promise;

  if(direction === 'to_edbo') {
    promise = addCertificateToEdbo(app, person, value);
  }
  else
  if(direction === 'to_matriculant') {
    promise = addCertificateToMatriculant(app, person, value);
  }

  promise
    .then(function() {
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err[0]);
      }

      res.status(500);
      res.end();
    });
};

function addCertificateToEdbo(app, person, value) {
  return new(app.get('db').PersonModel)({ id: person })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.load([ 'Document', 'EduDocuments', 'Certificates' ]);
    })
    .then(function(model) {
      return edboMatch(app, model.toJSON());
    })
    .then(function(PersonCodeU) {
      return app.get('edbo').Person.PersonsGet2({
        UIds: "'" + PersonCodeU + "'"
      });
    })
    .then(function(persons) {
      return app.get('edbo')
        .Person
        .PersonDocumentsZnoAdd({
          Id_Person:       persons.dPersonsGet2[0].Id_Person,
          DocumentNumbers: value.Number,
          DocumentDateGet: '01.01.' + value.Year + ' 00:00:00',
          ZNOPin:          +value.Pin
        });
    })
    .then(function(document) {

    });
}

function addCertificateToMatriculant(app, person, value) {
  return app.get('db')
    .CertificateModel
    .forge({
      Person: person,
      Number: value.Number,
      Year:   value.Year,
      Pin:    value.Pin
    })
    .save()
    .then(function(res) {
      var queue = [];
      for(var i in value.marks) {
        queue.push(
          app.get('db')
          .CertificateMarkModel
          .forge({
            Certificate: res.attributes.id,
            Subject: value.marks[i].Subject,
            Mark:    value.marks[i].Mark.toString().replace(',', '.')
          })
          .save()
        );
      }

      return Promise.all(queue);
    });
}
