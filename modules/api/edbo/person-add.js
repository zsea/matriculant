'use strict';
var formatDate = require('./format-date');

module.exports = function addPerson(app, req, res) {
  var person;

  return new (app.get('db').PersonModel)({ id: req.param('id') })
    .fetch()
    .then(function(model) {
      return model.load([
        'Document',
        'EduDocuments',
        'Certificates',
        'Certificates.marks'
      ]);
    })
    .then(function(model) {
      person = model.toJSON();
      if(person.IsForeigner) {
        console.error('FOREIGNERS ARE NOT AVAILABLE YET!');
        throw undefined;
      }

      var Street = '', House = '', Flat = '';

      var streetChunks = person.LivingStreet.split(',');
      if(streetChunks[1]) {
        Flat = streetChunks[1].trim() || '';
        Flat = Flat.replace(/\D/g, '');
      }

      if(/\d+/g.test(streetChunks[0])) {
        streetChunks[0] = streetChunks[0].trim();
        streetChunks = streetChunks[0].split(' ');

        Street = streetChunks.slice(0, streetChunks.length - 1).join(' ');
        House = streetChunks[streetChunks.length - 1];
        House = House.replace(/\D/g, '');
      }
      else {
        Street = person.LivingStreet;
        House  = Flat;
        Flat   = '';
      }

      person.EduDocuments.sort(function(lhs, rhs) {
        return lhs.Type - rhs.Type;
      });

      return app.get('edbo')
        .Person
        .PersonEntrantAdd2({
          AllowProcessedPersonalData: 1,

          Birthday:   formatDate(person.BirthDate),
          Birthplace: '', // person.BirthPlace,
          KOATUUCode: person.LivingPlace,

          Id_PersonSex: person.Sex,
          FirstName:    person.Name,
          MiddleName:   person.Patronymic,
          LastName:     person.Surname,
          FirstNameEn:  '',
          MiddleNameEn: '',
          LastNameEn:   '',

          Id_StreetType: person.StreetType,
          Adress:        Street,
          HomeNumber:    House,
          Apartment:     Flat,
          Housing:       '',
          PostIndex:     trim(person.PostCode),

          LanguagesAreStudied: '',

          EntrantDocumentSeries:  person.EduDocuments[0].Series,
          EntrantDocumentNumber:  person.EduDocuments[0].Number,
          EntrantDocumentDate:    formatDate(person.EduDocuments[0].Date),
          EntrantDocumentValue:   person.EduDocuments[0].Mark + '',
          EntrantDocumentIssued:  person.EduDocuments[0].GivenBy,
          Id_EntrantDocumnetType: person.EduDocuments[0].Type,
          Id_EntrantDocumentsAwardType:  person.EduDocuments[0].Award,

          EntrantDocumnetUniversityKode: '',
          Kode_School: '',

          IsNotCheckAttestat:       0, // sometimes: 1
          IsForeinghEntrantDocumet: 0,

          PasportSeries: trim(person.Document.Series),
          PasportNumber: trim(person.Document.Number),
          PasportIssued: trim(person.Document.GivenBy),
          PasportDate:   formatDate(person.Document.Date),
          Id_PersonDocumnetType_Pasport: person.Document.Type,

          Phone:  '',
          Mobile: person.PhoneNumber,
          Email:  '',
          Skype:  '',
          ICQ:    '',

          Father: '',
          Mother: '',
          FatherPhones: '',
          MotherPhones: '',

          Resident:   person.IsForeigner == false ? 1 : 0,

          Id_Country:        804, // ugly hardcode
          Id_ForeignCountry: -2
        });
    })
    .then(function() {
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err);
      }

      res.json(500, err.stack || err);
    });
};

function trim(value) {
  if(!value) {
    return '';
  }

  return value;
}
