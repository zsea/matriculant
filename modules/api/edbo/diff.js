'use strict';

function diff(ignore, obj, refObj) {
  var checked = [],
      result  = [],
      details;

  if(!(obj instanceof Object) || !(refObj instanceof Object)) {
    if(obj != refObj) {
      result.push({
        type:    'edit',
        key:     key,
        ref:     refObj,
        current: obj
      });
    }

    return result;
  }

  function recursiveMapper(key, item) {
    if(item.key) {
      item.key = key + '.' + item.key;
    }
    else {
      item.key = key;
    }
  }

  for(var key in obj) {
    if(!obj.hasOwnProperty(key) || ignore.indexOf(key) !== -1) {
      continue;
    }

    checked.push(key);

    if(obj[key] !== null &&
       obj[key] !== undefined &&
       refObj[key] === undefined)
    {
      result.push({
        type: 'add',
        key:  key
      });

      continue;
    }

    if(obj[key] && obj[key].getFullYear) {
      var res = obj[key].getFullYear() !== refObj[key].getFullYear();
          res = res || obj[key].getDate() !== refObj[key].getDate();
          res = res || obj[key].getMonth() !== refObj[key].getMonth();

      if(res) {
        result.push({
          type:    'edit',
          key:     key,
          ref:     refObj[key],
          current: obj[key]
        });
      }
    }
    else if(obj[key] && obj[key].splice) { // is array
      details = arrayDiff(ignore, obj[key], refObj[key]);
      details.forEach(recursiveMapper.bind(null, key));

      result = result.concat(details);
    }
    else if(typeof obj[key] == 'object') {
      details = diff(ignore, obj[key], refObj[key]);
      details.forEach(recursiveMapper.bind(null, key));

      result = result.concat(details);
    }
    else if(/^\d{1,3}(\.\d+)?$/.test(obj[key].toString())) {
      var n1 = Math.round(100 * parseFloat(obj[key]) + 0.05) / 100,
          n2 = Math.round(100 * parseFloat(refObj[key]) + 0.05) / 100;

      if(n1 != n2) {
        result.push({
          type:    'edit',
          key:     key,
          ref:     refObj[key],
          current: obj[key]
        });
      }
    }
    else if(obj[key] != refObj[key] || obj[key] != refObj[key]) {
      result.push({
        type:    'edit',
        key:     key,
        ref:     refObj[key],
        current: obj[key]
      });
    }
  }

  for(var refKey in refObj) {
    if(!refObj.hasOwnProperty(refKey) || ignore.indexOf(refKey) !== -1) {
      continue;
    }

    if(checked.indexOf(refKey) === -1) {
      result.push({
        type: 'remove',
        key:  refKey
      });
    }
  }

  return result;
};

function arrayDiff(ignore, array, referense) {
  var referenseVisited = new Array(referense.length),
      arrayVisited     = new Array(array.length);

  for(var i = 0; i < array.length; ++i) {
    for(var j = 0; j < referense.length; ++j) {
      if(referenseVisited[j]) {
        continue;
      }

      if(diff(ignore, array[i], referense[j]).length === 0) {
        arrayVisited[i]     = true;
        referenseVisited[j] = true;
        break;
      }
    }
  }

  var result = [];
  referense.forEach(function(item, idx) {
    if(referenseVisited[idx]) {
      return;
    }

    result.push({
      type:    'remove',
      current: item
    });
  });
  array.forEach(function(item, idx) {
    if(arrayVisited[idx]) {
      return;
    }

    result.push({
      type:    'add',
      current: item
    });
  });


  return result;
};

module.exports = {
  arrayDiff: arrayDiff,
  diff:      diff
};
