'use strict';
var requestMatch = require('./request-match'),
    formatDate   = require('./format-date'),
    Seasons      = require('./cache/seasons'),
    Promise      = require('bluebird'),
    UniversityKode = require('./cache/university-kode');

module.exports = function editOriginal(app, req, res) {
  var value     = req.body,
      request   = req.param('id'),
      Id_PersonRequest;

  Promise.resolve()
    .then(function() {
      return Promise.all([ Seasons(app) ]);
    })
    .then(function() {
      return new(app.get('db').RequestModel)({ id: request }).fetch();
    })
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.load([
        'Person',
        'Person.Document',
        'Person.EduDocuments',
        'Person.Certificates',
        'Person.Certificates.marks',
        'SubRequests',
        'Speciality'
      ]);
    })
    .then(function(model) {
      var Request = model.toJSON();

      if(Request.Stream === 2) {
        Request.SubRequests = [
          { id: 3, Type: 3, Status: Request.Status },
          { id: 4, Type: 4, Status: Request.Status }
        ];
      }

      return requestMatch(app, Request);
    })
    .then(function(result) {
      if(req.param('subrequest')) {
        result = result.filter(function(item) {
          return item.SubRequest == req.param('subrequest');
        })[0];

        if(!result) {
          throw new Error('Невірна підзаява!');
        }
      }

      return result.Id_PersonRequest;
    })
    .then(function(Id_PersonRequest) {
      return app.get('edbo')
        .Person
        .PersonRequestOriginalDocumentChange({
          Id_PersonRequest: Id_PersonRequest,
          OriginalDocumentsAdd: value.Original ? 1 : 0
        });
    })
    .then(function() {
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.log(err.stack || err);
        res.json(500, err.stack || err);
      }
    });
};
