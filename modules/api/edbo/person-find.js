'use strict';
var Promise        = require('bluebird'),
    UniversityKode = require('./cache/university-kode');

function trim(value) {
  if(typeof value === 'object') {
    return '';
  }

  return value;
}

module.exports = function(app, PersonCodeU) {
  var result = {
    Document:     { },
    EduDocuments: [],
    Certificates: [],
    Benefits:     []
  };
  var IdPerson;

  return Promise.resolve()
    .then(function() {
      return UniversityKode(app);
    })
    // step 1
    .then(function() {
      var opts = {
        FIOMask: '*',
        DocumentSeries: '',
        DocumentNumber: '',
        Ids_DocumentTypes: '',
        Hundred: 1,
        PersonCodeU: PersonCodeU,
        Filters: ''
      };

      return app.get('edbo').Person.PersonsFind2(opts);
    })
    .then(function(person) {
      person = person.dPersonsFind2[0];

      IdPerson = person.Id_Person;
      personParser.call(result, person);
    })

    // step 2
    .then(function() {
      var opts = {
        PersonCodeU:           PersonCodeU,
        Id_PersonDocumentType: 0, // all docs
        Id_PersonDocument:     0,
        IsEntrantDocument:     0, // zno, passport, etc...
        UniversityKode:        UniversityKode()
      };

      return app.get('edbo').Person.PersonDocumentsGet(opts);
    })
    .then(documentsParser.bind(result, app))

    // step 3
    .then(function() {
      var opts = {
        PersonCodeU:           PersonCodeU,
        Id_PersonDocumentType: 0, // all docs
        Id_PersonDocument:     0,
        IsEntrantDocument:     -1, // attestat, diplom, etc...
        UniversityKode:        UniversityKode()
      };

      return app.get('edbo').Person.PersonDocumentsGet(opts);
    })
    .then(eduDocumentsParser.bind(result))

    // step 4
    .then(function() {
      var opts = { Id_Person: IdPerson };
      return app.get('edbo').Person.PersonBenefitsGet2(opts);
    })
    .then(function(benefits) {
      if(benefits.dPersonBenefits2) {
        result.Benefits = benefits.dPersonBenefits2.map(function(item) {
          return item.Id_Benefit;
        });
      }
    })

    // step 5
    .then(function() {
      return app.get('edbo').Person.PersonContactsGet({
        PersonCodeU:      PersonCodeU,
        Id_PersonContact: 0
      });
    })
    .then(function(contacts) {
      if(contacts.dPersonContacts) {
        contacts.dPersonContacts.forEach(function(addr) {
          if([1, 2].indexOf(addr.Id_PersonContactType) !== -1) {
            result.PhoneNumber = addr.Value;
          }
        });
      }
    })

    // step 6
    .then(function() {
      return app.get('edbo').Person.PersonAddressesGet2({
        PersonCodeU:      PersonCodeU,
        Id_PersonAddress: 0
      });
    })
    .then(function(addresses) {
      if(addresses.dPersonAddresses2) {
        addresses.dPersonAddresses2.forEach(function(addr) {
          result.BirthPlace   = trim(addr.KOATUUCode);
          result.LivingPlace  = trim(addr.KOATUUCode);
          result.NeedHostel   = addr.KOATUUCode !== '2310100000';
          result.StreetType   = addr.Id_StreetType;
          result.LivingStreet = trim(addr.Adress) + ' ' + trim(addr.HomeNumber);
          result.PostCode     = trim(addr.PostIndex);
        });
      }
    })

    // final
    .then(function() {
      return result;
    });
};

function personParser(person) {
  this.Surname    = person.LastName;
  this.Name       = person.FirstName;
  this.Patronymic = person.MiddleName;
  this.BirthDate  = person.Birthday;
  this.Sex        = person.Id_PersonSex;

  this.IsForeigner = person.Resident == 0;
  this.BirthPlace  = person.Birthplace;
}

function requestCertificate(app, self, document) {
  var idx = self.Certificates.push({
    Year:   document.DocumentDateGet.getFullYear(),
    Number: document.DocumentNumbers,
    Pin:    document.ZNOPin,

    marks: []
  });

  // acquire marks
  var promise = app.get('edbo')
    .Person
    .PersonDocumentsSubjectsGet({
      Id_PersonDocument:     document.Id_PersonDocument,
      Id_Person:             0,
      Id_PersonDocumentType: 0
    })
    .then(function(marks) {
      marks = marks.dPersonDocumentsSubjects.map(function(mark) {
        return {
          Subject: mark.IdZnoSubject,
          Mark:    mark.PersonDocumentSubjectValue
        };
      });

      self.Certificates[idx - 1].marks = marks;
    });

    return promise;
}

function documentsParser(app, documents) {
  var queue = [],
      self = this;

  if(!documents.dPersonDocuments) {
    return;
  }

  var documents = documents.dPersonDocuments.sort(function(lhs, rhs) {
    return lhs.Id_PersonDocumentType - rhs.Id_PersonDocumentType;
  });

  documents.forEach(function(document) {
    if(document.Id_PersonDocumentType == 4) { // is ZNO
      queue.push(requestCertificate(app, self, document));
      return;
    }

    if([1, 3, 17].indexOf(document.Id_PersonDocumentType) !== -1) {
      self.Document.Series  = document.DocumentSeries;
      self.Document.Number  = document.DocumentNumbers;
      self.Document.Date    = document.DocumentDateGet;
      self.Document.Type    = document.Id_PersonDocumentType;
      self.Document.GivenBy = trim(document.DocumentIssued);
    }
  });


  return Promise.all(queue);
}

function eduDocumentsParser(documents) {
  var self = this;

  if(!documents.dPersonDocuments) {
    return;
  }

  documents.dPersonDocuments.forEach(function(document) {
    if(document.Cancellad) {
      return;
    }

    if(document.Id_PersonDocumentType == 14) {
      self.EduDocuments.push({
        Type:     document.Id_PersonDocumentType,
        Series:   document.DocumentSeries,
        Number:   document.DocumentNumbers,
        Date:     document.DocumentDateGet,
        GivenBy:  trim(document.DocumentIssued),
        Mark:     +document.AtestatValue,
        MarkType: 5,
        Award:    document.Id_PersonDocumentsAwardType
      });
    }

    if(/(диплом)|(атестат)/i.test(document.PersonDocumentTypeName)) {
      var markType = 5;
      if(/атестат/i.test(document.PersonDocumentTypeName)) {
        markType = 12;
      }

      self.EduDocuments.push({
        Type:     document.Id_PersonDocumentType,
        Series:   document.DocumentSeries,
        Number:   document.DocumentNumbers,
        Date:     document.DocumentDateGet,
        GivenBy:  trim(document.DocumentIssued),
        Mark:     +document.AtestatValue,
        MarkType: markType,
        Award:    document.Id_PersonDocumentsAwardType
      });
    }
  });
}
