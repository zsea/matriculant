'use strict';

module.exports = { };

module.exports.findByDocument = function findByDocument(app, document) {
  var opts = getDocumentOpts(document, 1);
  return findHandler(app, opts);
};
module.exports.findByName = function findByName(app, surname, name, patronymic) {
  var opts = getNameOpts(surname, name, patronymic, 1);
  return findHandler(app, opts);
};

function findHandler(app, opts) {
  return app.get('edbo')
    .Person
    .PersonsFind2(opts)
    .then(function(found) {
      if(!found.dPersonsFind2 || found.dPersonsFind2.length === 0) {
        return [];
      }

      return found.dPersonsFind2.map(function(person) {
        return person.PersonCodeU;
      });
    });
}

function getDocumentOpts(document, hundred) {
  return {
    FIOMask: '*',
    DocumentSeries: document.Series,
    DocumentNumber: document.Number,
    Ids_DocumentTypes: '',
    Hundred: hundred,
    PersonCodeU: '',
    Filters: ''
  };
}
function getNameOpts(surname, name, patronymic, hundred) {
  return {
    FIOMask: surname + ' ' + name + ' ' + patronymic,
    DocumentSeries: '',
    DocumentNumber: '',
    Ids_DocumentTypes: '',
    Hundred: hundred,
    PersonCodeU: '',
    Filters: ''
  };
}
