'use strict';

var Promise     = require('bluebird'),
    formatDate  = require('./format-date'),
    personMatch = require('./person-match'),
    Seasons     = require('./cache/seasons'),
    requestFind = require('./request-find'),
    qualificationAdapter = require('../adapters/qualification.js');

module.exports = function(app, Request) {
  var Seasons, PersonCodeU;

  return personMatch(app, Request.Person)
    .then(function(code) {
      PersonCodeU = code;

      if(Request.SubRequests.length > 0) {
        var promises = Request.SubRequests.map(function(item) {
          return subRequestMatch(app, PersonCodeU, Request, item);
        });

        return Promise.all(promises);
      }

      return requestMatch(app, PersonCodeU, Request);
    });
};

function requestMatch(app, PersonCodeU, Request) {
  var qualification = qualificationAdapter(Request);

  return Promise.resolve()
    .then(function() {
      return Seasons(app);
    })
    .then(function() {
      return app.get('edbo')
        .Person
        .PersonRequestsGet({
          PersonCodeU:             PersonCodeU,
          Id_PersonRequestSeasons: Seasons(),
          UniversityFacultetKode:  '',
          Id_PersonEducationForm:  Request.TeachingForm,

          Id_Qualification: qualification,
          Id_PersonRequest: 0,

          Filters: ''
        })
        .then(function(result) {
          if(!result.dPersonRequests) {
            return undefined;
          }

          var needle = Request.Speciality.Code;
          result = result.dPersonRequests.filter(function(item) {
            if(item.SpecClasifierCode !== needle) {
              return false;
            }

            if(item.UniversityFullName !== 'Запорізька державна інженерна академія') {
              return false;
            }

            return true;
          });

          if(result.length === 0) {
            return undefined;
          }

          result.sort(function(lhs, rhs) {
            return rhs.Id_PersonRequestStatus - lhs.Id_PersonRequestStatus;
          });
          var Id_PersonRequest = result[0].Id_PersonRequest;


          return requestFind(app, Request.Person, Id_PersonRequest)
            .then(function(result) {
              result.Id_PersonRequest = Id_PersonRequest;
              return result;
            });
        });
  });
}

function subRequestMatch(app, PersonCodeU, Request, SubRequest) {
  var qualification = qualificationAdapter(Request, SubRequest),
      Id_PersonRequest;

  return Promise.resolve()
    .then(function() {
      return Seasons(app);
    })
    .then(function() {
      return app.get('edbo')
        .Person
        .PersonRequestsGet({
          PersonCodeU:             PersonCodeU,
          Id_PersonRequestSeasons: Seasons(),
          UniversityFacultetKode:  '',
          Id_PersonEducationForm:  Request.TeachingForm,

          Id_Qualification: qualification,
          Id_PersonRequest: 0,

          Filters: ''
        })
        .then(function(result) {
          if(!result.dPersonRequests) {
            return {
              SubRequest:       SubRequest.id,
              Value:            undefined
            };
          }

          result = result.dPersonRequests;

          var tail = Request.Speciality.Code.split('.')[1];
          result = result.filter(function(item) {
            if(item.UniversityFullName !== 'Запорізька державна інженерна академія') {
              return false;
            }

            var chunk = (item.SpecSpecialityClasifierCode + '').split('.')[1];
            if(tail === chunk) {
              return true;
            }

            chunk = (item.SpecClasifierCode + '').split('.')[1];
            if(tail === chunk) {
              return true;
            }
          });

          if(result.length === 0) {
            return {
              SubRequest:       SubRequest.id,
              Value:            undefined
            };
          }

          result.sort(function(lhs, rhs) {
            return rhs.Id_PersonRequest - lhs.Id_PersonRequest;
          });

          return requestFind(app, Request.Person, result[0].Id_PersonRequest)
            .then(function(request) {
              return {
                SubRequest:       SubRequest.id,
                Id_PersonRequest: result[0].Id_PersonRequest,
                Value:            request
              };
            });
        });
  });
}
