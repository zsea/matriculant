'use strict';
var personMatch = require('./person-match'),
    eduDocumentFind = require('./edu-document-find'),
    UniversityKode  = require('./cache/university-kode'),
    Promise         = require('bluebird');

module.exports = function addEduDocument(app, req, res) {
  var value     = req.body,
      person    = req.param('id');

  return Promise.resolve()
    .then(function() {
      return UniversityKode(app);
    })
    .then(function() {
      return new(app.get('db').PersonModel)({ id: person }).fetch();
    })
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.load([ 'Document', 'EduDocuments', 'Certificates' ]);
    })
    .then(function(model) {
      return personMatch(app, model.toJSON());
    })
    .then(function(PersonCodeU) {
      return eduDocumentFind(app, PersonCodeU, value);
    })
    .then(function(Id_PersonDocument) {
      return app.get('edbo')
        .Person
        .EntrantDocumentValueChange({
          AttestatValue: value.Mark,
          IsCheckForPaperCopy: 0,
          UniversityKode: UniversityKode(),
          Id_PersonDocument: Id_PersonDocument
        })
    })
    .then(function() {
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err[0]);
      }

      res.status(500);
      res.end();
    });
};
