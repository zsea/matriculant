'use strict';
var Promise = require('bluebird'),
    genDiff = require('./diff');

var ignorableKeys = [
  'id',
  'Person',
  'EduDocuments',
  'SubRequests',
  'Synchronized',
  'Date',
  'Speciality',
  'CertificateMark1',
  'CertificateMark2',
  'CertificateMark3',
  'Id_PersonRequest',
  'Request',
  'ExaminationCause',
  'Number'
];

module.exports = function requestDiff(ourRequest, edboRequest) {
  ourRequest.EZ = !!ourRequest.EZ;
  ourRequest.Courses = !!ourRequest.Courses;
  if(!ourRequest.CoursesMark) {
    ourRequest.CoursesMark = 0;
  }

  return Promise.resolve()
    .then(function() {
      return genDiff.diff(ignorableKeys, ourRequest, edboRequest);
    })
    .then(function(diff) {
      if(!diff) {
        return diff;
      }
      
      return diff.filter(function(item) { // ignore empty exams
        if(item.key !== 'Exams') {
          return true;
        }

        if(item.current.Mark) {
          return true;
        }
      });
    })
};
