'use strict';
var match      = require('./person-match'),
    personDiff = require('./person-diff'),
    personFind = require('./person-find');

module.exports = function checkPersonHandler(app, person) {
  var currentPerson;

  var result = new (app.get('db').PersonModel)({ id: person })
    .fetch()
    .then(function(model) {
      return model.load([
        'Document',
        'EduDocuments',
        'Certificates', 'Certificates.marks',
        'Benefits'
      ]);
    })
    .then(function(model) {
      person = model.toJSON();
      currentPerson = person;
      currentPerson.Benefits = currentPerson.Benefits.map(function(item) {
        return item.Type;
      });

      return match(app, person);
    })
    .then(function(PersonCodeU) {
      if(!PersonCodeU) {
        return;
      }

      return personFind(app, PersonCodeU);
    })
    .then(function(person) {
      if(!person) {
        return;
      }

      return personDiff(currentPerson, person);
    })
    .then(function(diff) {
      if(diff === undefined) {
        currentPerson.isDifferent = false;
        currentPerson.isMissing   = true;
      }
      else {
        currentPerson.isDifferent = diff.length > 0;
        currentPerson.isMissing   = false;
        currentPerson.diff        = diff;
      }

      return currentPerson;
    });

  return result;
};
