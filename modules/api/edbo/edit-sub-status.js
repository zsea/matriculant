'use strict';
var requestMatch = require('./request-match'),
    formatDate   = require('./format-date'),
    Seasons      = require('./cache/seasons'),
    Promise      = require('bluebird'),
    UniversityKode = require('./cache/university-kode');

module.exports = function addExamResult(app, req, res) {
  var value     = req.body,
      request   = req.param('id'),
      Id_PersonRequest;

  Promise.resolve()
    .then(function() {
      return Promise.all([ Seasons(app) ]);
    })
    .then(function() {
      return new(app.get('db').RequestModel)({ id: request }).fetch();
    })
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.load([
        'Person',
        'Person.Document',
        'Person.EduDocuments',
        'Person.Certificates',
        'SubRequests',
        'Speciality'
      ]);
    })
    .then(function(model) {
      return requestMatch(app, model.toJSON());
    })
    .then(function(result) {
      result = result.filter(function(item) {
        return item.SubRequest == req.param('subrequest');
      })[0];

      if(!result) {
        throw new Error('Невірна підзаява!');
      }

      Id_PersonRequest = result.Id_PersonRequest;

      return app.get('edbo')
        .Person
        .PersonRequestsStatusChange({
          Id_PersonRequest:           Id_PersonRequest,
          Id_PersonRequestStatusType: value.Status,
          Id_UniversityEntrantWave:   value.Wave,
          IsBudejt:                  -1,
          IsContract:                -1,
          Descryption: ''
        });
    })
    .then(function() {
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.log(err.stack || err);
        res.json(500, err.stack || err);
      }
    });
};
