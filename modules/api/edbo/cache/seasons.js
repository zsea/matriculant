'use strict';

var formatDate = require('../format-date');
var cachedSeasons;

module.exports = function(app) {
  if(cachedSeasons) {
    return cachedSeasons;
  }

  return app.get('edbo')
    .Person
    .PersonRequestSeasonsGet({
      Id_PersonRequestSeasons: 0,
      Id_PersonEducationForm:  1,
      ActualData: '',
      OnlyActive: 1
    })
    .then(function(seasons) {
      if(seasons.dPersonRequestSeasons) {
        seasons.dPersonRequestSeasons.sort(function(lhs, rhs) {
          return rhs.Id_PersonRequestSeasons - lhs.Id_PersonRequestSeasons;
        });
        
        cachedSeasons = seasons.dPersonRequestSeasons[0].Id_PersonRequestSeasons;
        return cachedSeasons;
      }
    });
}
