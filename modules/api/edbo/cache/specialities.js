'use strict';

var formatDate     = require('../format-date'),
    UniversityKode = require('./university-kode'),
    Seasons        = require('./seasons'),
    Promise        = require('bluebird');

var cachedSpecialities;

module.exports = function(app) {
  if(cachedSpecialities) {
    return cachedSpecialities;
  }

  return Promise.resolve()
  .then(function() {
    return Promise.all([ Seasons(app), UniversityKode(app) ]);
  })
  .then(function() {
    return app.get('edbo')
      .Guides
      .UniversityFacultetSpecialitiesGet({
        UniversityKode: UniversityKode(),

        UniversityFacultetKode:     '',
        UniversitySpecialitiesKode: '',
        SpecCode: '',
        Filters:  '',

        Id_PersonRequestSeasons: Seasons(),
        Id_PersonEducationForm:  0,

        SpecDirectionsCode: '',
        SpecSpecialityCode: ''
      });
  })
  .then(function(result) {
    cachedSpecialities = result.dUniversityFacultetSpecialities;
    return cachedSpecialities;
  });
};
