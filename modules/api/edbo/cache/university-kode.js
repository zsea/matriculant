'use strict';

var formatDate = require('../format-date');
var cachedUniversityKode;

module.exports = function(app) {
  if(cachedUniversityKode) {
    return cachedUniversityKode;
  }

  return app.get('edbo')
    .Guides
    .UniversitiesGet({
      UniversityKode: '',
      UniversityName: 'Запорізька державна інженерна академія'
    })
    .then(function(result) {
      cachedUniversityKode = result.dUniversities[0].UniversityKode;

      return cachedUniversityKode;
    });
};
