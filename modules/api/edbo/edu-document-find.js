module.exports = function(app, PersonCodeU, document) {
  return app.get('edbo')
    .Person
    .PersonDocumentsGet({
      PersonCodeU:           PersonCodeU,
      Id_PersonDocumentType: document.Type,
      Id_PersonDocument:     0,
      IsEntrantDocument:     1,
      UniversityKode:        ''
    })
    .then(function(result) {
      result = result.dPersonDocuments;
      
      result = result.filter(function(item) {
        return item.DocumentNumbers == document.Number;
      });

      if(result.length !== 1) {
        throw new Error('Неможливо знайти документ про освіту!');
      }

      return result[0].Id_PersonDocument;
    })
}
