module.exports = function formatDate(date) {
  function padding(n) {
    return n < 10 ? '0' + n : n;
  }

  if(!date) {
    return '';
  }

  return padding(date.getDate()) + '.' +
         padding(date.getMonth() + 1) + '.' +
         date.getFullYear() + ' ' +

         padding(date.getHours()) + ':' +
         padding(date.getMinutes()) + ':' +
         padding(date.getSeconds());
}
