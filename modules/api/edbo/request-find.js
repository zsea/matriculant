'use strict';
var Promise = require('bluebird'),
    Seasons = require('./cache/seasons.js'),
    streamAdapter = require('../adapters/stream.js');

module.exports = function(app, Person, Id_PersonRequest) {
  var edboRequest, znoSubjects;

  return Promise.resolve()
    .then(function() {
      return Seasons();
    })
    .then(function() {
      return app.get('edbo')
        .Person
        .PersonRequestsGet2({
          PersonCodeU: '',
          Id_PersonRequestSeasons: Seasons(),
          Id_PersonRequest: Id_PersonRequest,

          UniversityFacultetKode: '',
          Id_PersonEducationForm: 0,
          Id_Qualification: 0,

          Filters: ''
        });
    })
    .then(function(result) {
      edboRequest = result.dPersonRequests2[0];

      return app.get('edbo')
        .Person
        .PersonRequestDocumentSubjectsGet({
          Id_PersonRequest: Id_PersonRequest
        });
    })
    .then(function(result) {
      if(result.dPersonRequestDocumentSubjects) {
        znoSubjects = result.dPersonRequestDocumentSubjects.map(function(item) {
          return {
            Number:  item.DocumentNumbers,
            Subject: item.IdZnoSubject
          };
        });
      }
      else {
        znoSubjects = [];
      }

      return findSpeciality(app, edboRequest);
    })
    .then(function(Speciality) {
      return Promise.props({
        Person: Person.id,
        Type:   edboRequest.Id_PersonEnteranceTypes,
        EZ:     !!edboRequest.IsEz,

        EduDocument:  findEduDocument(Person, edboRequest),
        Original:     !!edboRequest.OriginalDocumentsAdd,
        Status:       edboRequest.Id_PersonRequestStatusType,

        Number:       edboRequest.CodeOfBusiness,
        Speciality:   Speciality,
        Stream:       streamAdapter(edboRequest.Id_Qualification),
        TeachingForm: edboRequest.Id_PersonEducationForm,

        ExaminationCause: edboRequest.Id_PersonRequestExaminationCause,
        CertificateMark1: findCertificateMark(Person, znoSubjects[0]),
        CertificateMark2: findCertificateMark(Person, znoSubjects[1]),
        CertificateMark3: findCertificateMark(Person, znoSubjects[2]),

        Exams:    findExams(app, edboRequest),
        Budget:   edboRequest.IsBudget,
        Priority: edboRequest.RequestPriority,

        Courses: false,
        CoursesMark: 0
      });
    })
    .then(function(request) {
      return findCourses(app, Id_PersonRequest, request);
    });
};

function findEduDocument(Person, Request) {
  var id;
  Person.EduDocuments.some(function(document) {
    if(document.Number == Request.EntrantDocumentNumbers) {
      id = document.id;
      return true;
    }
  });

  if(!id) {
    throw new Error('Неможливо знайти документ про освіту!');
  }

  return id;
}

function findSpeciality(app, edboRequest) {
  var code = edboRequest.SpecSpecialityClasifierCode;
  if(JSON.stringify(code) === '{}') {
    code = edboRequest.SpecClasifierCode;
  }
  else {
    code = '.' + code.split('.')[1];
  }

  return new(app.get('db').SpecialityModel)()
  .query(function(params) {
    params.where('Code', 'ilike', '%' + code)
  })
  .fetch()
  .then(function(model) {
    if(!model) {
      throw new Error('Неможливо знайти спеціальність "' + code + '"');
    }

    return model.toJSON().id;
  });
}

function findCertificateMark(Person, Subject) {
  var Certificate, id;
  if(Subject === undefined) {
    return;
  }

  Person.Certificates.some(function(certificate) {
    if(certificate.Number == Subject.Number) {
      Certificate = certificate;
      return true;
    }
  });

  if(!Certificate) {
    throw new Error([
      'Неможливо знайти сертифікат ЗНО!',
      '#' + Subject.Number
    ].join(' '));
  }

  Certificate.marks.some(function(mark) {
    if(mark.Subject == Subject.Subject) {
      id = mark.id;
      return true;
    }
  });

  if(!id) {
    throw new Error([
      'Неможливо знайти оцінку ЗНО!',
      'subject:' + Subject.Subject,
      'сертифікат #' + Subject.Number
    ].join(' '));
  }

  return id;
}

function findExams(app, edboRequest) {
  return app.get('edbo')
    .Person
    .PersonRequestExaminationsGet({
      Id_PersonRequest: edboRequest.Id_PersonRequest
    })
    .then(function(result) {
      result = result.dPersonRequestExaminations;
      if(!result) {
        return [];
      }

      return result.map(function(item) {
        return {
          Primary: item.SubjectName == 'Фаховий іспит',
          Foreign: item.SubjectName == 'Іноземна мова',
          Subject: item.IdZnoSubject ? item.IdZnoSubject : null,
          Mark:    item.PersonRequestExaminationValue
        };
      });
    });
}

function findCourses(app, Id_PersonRequest, request) {
  return app.get('edbo')
    .Person
    .PersonRequestCoursesGet({
      Id_PersonRequest: Id_PersonRequest
    })
    .then(function(result) {
      result = result.dPersonRequestCourses;

      if(!result) {
        return request;
      }

      request.Courses     = true;
      request.CoursesMark = +result[0].PersonRequestCourseBonus;
      return request;
    });
}
