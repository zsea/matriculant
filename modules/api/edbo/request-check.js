'use strict';
var match   = require('./request-match'),
    diff    = require('./request-diff'),
    subDiff = require('./request-sub-diff');

module.exports = function checkPersonHandler(app, request) {
  var Request;
  return new(app.get('db').RequestModel)({ id: request })
    .fetch()
    .then(function(model) {
      return model.load([
        'Person',
        'Person.Document',
        'Person.EduDocuments',
        'Person.Certificates',
        'Person.Certificates.marks',
        'Speciality',
        'SubRequests',
        'Exams'
      ]);
    })
    .then(function(model) {
      Request = model.toJSON();

      if(Request.Stream === 2) {
        Request.SubRequests = [
          { id: 3, Type: 3, Status: Request.Status },
          { id: 4, Type: 4, Status: Request.Status }
        ];
      }

      return match(app, Request);
    })
    .then(function(result) {
      if(!result) {
        Request.isMissing = true;
        return Request;
      }

      if(!result.length) {
        return diff(Request, result)
          .then(function mapDiff(diff) {
            Request.isDifferent = diff.length > 0;
            Request.diff        = diff;
            return Request;
          });
      }

      return subDiff(result, Request);
    });
};
