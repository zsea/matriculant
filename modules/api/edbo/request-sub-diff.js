'use strict';
var Promise = require('bluebird'),
    genDiff = require('./diff');

module.exports = function requestSubDiff(edboRequests, ourRequest) {
  edboRequests.forEach(function(item) {
    if(!item || item.Value === undefined) {
      return makeMissing(item, ourRequest);
    }

    return diff(item, ourRequest);
  });

  return ourRequest;
};

function findSubRequest(SubRequests, id) {
  return SubRequests.filter(function(item) {
    return item.id == id;
  })[0];
}

function makeMissing(edboSubRequest, ourRequest) {
  var req = findSubRequest(ourRequest.SubRequests, edboSubRequest.SubRequest);
  req.isMissing = true;
}

var ignorableKeys = [
  'id',
  'Person',
  'EduDocuments',
  'SubRequests',
  'Synchronized',
  'Date',
  'Speciality',
  'CertificateMark1',
  'CertificateMark2',
  'CertificateMark3',
  'Id_PersonRequest',
  'Request',
  'Number'
];

function diff(edboSubRequest, ourRequest) {
  var req = findSubRequest(ourRequest.SubRequests, edboSubRequest.SubRequest);
  ourRequest.Status = req.Status;
  if(!ourRequest.CoursesMark) {
    ourRequest.CoursesMark = 0;
  }
  
  ourRequest.Priority = -1;

  req.diff = genDiff.diff(ignorableKeys, ourRequest, edboSubRequest.Value);
  req.isDifferent = req.diff.length > 0;

  if(req.Type == 1) { // ignore specialist foreign exam
    req.diff = req.diff.filter(function(item) {
      if(item.key !== 'Exams') {
        return true;
      }

      if(!item.current.Foreign) {
        return true;
      }
    });
  }


  req.diff = req.diff.filter(function(item) { // ignore empty foreign exam
    if(item.key !== 'Exams') {
      return true;
    }

    if(item.current.Mark) {
      return true;
    }
  });
}
