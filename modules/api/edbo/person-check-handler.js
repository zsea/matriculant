'use strict';
var handler = require('./person-check');

module.exports = function checkPerson(app, req, res) {
  if(!req.param('id')) {
    res.status(500);
    res.end();
    return;
  }
  if(!app.get('edbo')) {
    res.status(500);
    res.end('EDBO connection is not established!');
    return;
  }

  handler(app, req.param('id'))
    .then(function(person) {
      res.json(person);

      if(person.diff && person.diff.length === 0) {
        return new (app.get('db').PersonModel)({ id: req.param('id') })
          .fetch()
          .then(function(data) {
            return data.save({ Synchronized: true }, { path: true });
          });
      }
    })
    .catch(function(err) {
      console.error(err.stack || err);

      res.status(500);
      res.end();
    });
};
