'use strict';
var diff = require('./diff').diff;

var ignorableKeys = [
  'id',
  'NeedHostel',
  'Synchronized',
  'Person',
  'Series', // ugly and dangerous,
            // but edbo translates cyrillic 'АР' into latin 'AP'
  'Certificate',
  'MarkType',

  'LivingStreet', 'GivenBy' // temporary fix
];

module.exports = function personDiff(person, refPerson) {
  return diff(ignorableKeys, person, refPerson);
};
