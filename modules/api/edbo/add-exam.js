'use strict';
var requestMatch = require('./request-match'),
    formatDate   = require('./format-date'),
    Seasons      = require('./cache/seasons'),
    Promise      = require('bluebird'),
    UniversityKode = require('./cache/university-kode');

module.exports = function addExamResult(app, req, res) {
  var value     = req.body,
      request   = req.param('id'),
      Id_PersonRequest;

  Promise.resolve()
    .then(function() {
      return Promise.all([ Seasons(app), UniversityKode(app) ]);
    })
    .then(function() {
      if(value.Subject !== 0) {
        return new(app.get('db').CertificateSubjectModel)({ id: value.Subject })
          .fetch()
          .then(function(model) {
            if(model) {
              value.Subject = model.toJSON().Name;
            }
          });
      }
    })
    .then(function() {
      return new(app.get('db').RequestModel)({ id: request }).fetch();
    })
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.load([
        'Person',
        'Person.Document',
        'Person.EduDocuments',
        'Person.Certificates',
        'Person.Certificates.marks',
        'SubRequests',
        'Speciality'
      ]);
    })
    .then(function(model) {
      var Request = model.toJSON();

      if(Request.Stream === 2) {
        Request.SubRequests = [
          { id: 3, Type: 3, Status: Request.Status },
          { id: 4, Type: 4, Status: Request.Status }
        ];
      }

      return requestMatch(app, Request);
    })
    .then(function(result) {
      if(result.length) {
        result = result.filter(function(item) {
          return item.SubRequest == req.param('subrequest');
        })[0];
      }

      if(!result) {
        throw new Error('Невірна заява або підзаява!');
      }

      Id_PersonRequest = result.Id_PersonRequest;

      return app.get('edbo').Person.PersonRequestsGet2({
        PersonCodeU: '',
        Id_PersonRequestSeasons: Seasons(),
        Id_PersonRequest: result.Id_PersonRequest,
        UniversityFacultetKode: '',
        Id_PersonEducationForm: 0,
        Id_Qualification: 0,
        Filters: ''
      });
    })
    .then(function(request) {
      var kode = request.dPersonRequests2[0].UniversitySpecialitiesKode;

      return app.get('edbo')
        .Guides
        .UniversityFacultetSpecialitiesSubjectsGet({
          UniversitySpecialitiesKode: kode
        });
    })
    .then(function(result) {
      result = result.dUniversityFacultetSpecialitiesSubjects;
      var needle;
      if(value.Foreign) {
        needle = 'Іноземна мова';
      }
      else
      if(value.Primary) {
        needle = 'Фаховий іспит';
      }
      else {
        needle = value.Subject;
      }

      result = result.filter(function(subject) {
        return subject.SubjectName == needle;
      })[0];

      if(!result) {
        throw new Error('Невірно вказано предмет!');
      }

      var subject = result.Id_UniversitySpecialitiesSubject;

      return app.get('edbo')
        .Person
        .PersonRequestExaminationsAdd({
          Id_PersonRequest:                 Id_PersonRequest,
          Id_UniversitySpecialitiesSubject: subject,
          PersonRequestExaminationValue:    value.Mark.toFixed(2)
        });
    })
    .then(function() {
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.log(err.stack || err);
        res.json(500, err.stack || err);
      }
    });
};
