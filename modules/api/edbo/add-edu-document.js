'use strict';
var edboMatch  = require('./person-match'),
    formatDate = require('./format-date');

module.exports = function addEduDocument(app, req, res) {
  var value     = req.body,
      person    = req.param('id'),
      direction = req.param('direction');

  var promise;

  if(direction === 'to_edbo') {
    promise = addEduDocumentToEdbo(app, person, value);
  }
  else
  if(direction === 'to_matriculant') {
    promise = addEduDocumentToMatriculant(app, person, value);
  }

  promise
    .then(function() {
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err[0]);
      }

      res.status(500);
      res.end();
    });
};

function addEduDocumentToEdbo(app, person, value) {
  return new(app.get('db').PersonModel)({ id: person })
    .fetch()
    .then(function(model) {
      if(!model) {
        throw undefined;
      }

      return model.load([ 'Document', 'EduDocuments', 'Certificates' ]);
    })
    .then(function(model) {
      return edboMatch(app, model.toJSON());
    })
    .then(function(PersonCodeU) {
      return app.get('edbo').Person.PersonsGet2({
        UIds: "'" + PersonCodeU + "'"
      });
    })
    .then(function(persons) {
      return app.get('edbo').Person.PersonDocumentsAdd({
        Id_Person: persons.dPersonsGet2[0].Id_Person,
        Id_PersonDocumentType: value.Type,
        Id_PersonEducation:    0, //TODO

        DocumentSeries:  value.Series,
        DocumentNumbers: value.Number,
        DocumentDateGet: formatDate(new Date(value.Date)),
        DocumentIssued:  value.GivenBy,
        AttestatValue:   value.Mark + '',
        Id_PersonDocumentsAwardType: value.Award,

        Description: '',
        ZNOPin:      0,
        IsCheckForPaperCopy: 1 // TODO,
      });
    });
}

function addEduDocumentToMatriculant(app, person, value) {
  return app.get('db')
    .EduDocumentModel
    .forge({
      Person:     person,
      Type:       value.Type,
      Series:     value.Series,
      Number:     value.Number,
      Date:       value.Date,
      GivenBy:    value.GivenBy,
      Mark:       value.Mark.toString().replace(',', '.'),
      MarkType:   value.MarkType,
      Award:      value.Award
    })
    .save();
}
