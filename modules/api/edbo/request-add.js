'use strict';
var formatDate      = require('./format-date'),
    personMatch     = require('./person-match'),
    eduDocumentFind = require('./edu-document-find'),
    Promise         = require('bluebird'),
    Seasons         = require('./cache/seasons'),
    UniversityKode  = require('./cache/university-kode'),
    Specialities    = require('./cache/specialities');

module.exports = function addRequest(app, req, res) {
  var Request, PersonCodeU;

  return Promise.resolve()
    .then(function() {
      return Promise.all([ Seasons(app), UniversityKode(app), Specialities(app) ]);
    })
    .then(function() {
      return new (app.get('db').RequestModel)({
        id: req.param('id')
      }).fetch();
    })
    .then(function(model) {
      return model.load([
        'Person',
        'Person.Document',
        'Person.EduDocuments',
        'Person.Certificates',
        'Person.Certificates.marks',
        'Speciality',
        'SubRequests',
        'EduDocument',
        'CertificateMark1',
        'CertificateMark1.Certificate',
        'CertificateMark2',
        'CertificateMark2.Certificate',
        'CertificateMark3',
        'CertificateMark3.Certificate',
      ]);
    })
    .then(function(model) {
      Request = model.toJSON();
      return personMatch(app, Request.Person);
    })
    .then(function(code) {
      PersonCodeU = code;
      return eduDocumentFind(app, PersonCodeU, Request.EduDocument);
    })
    .then(function(eduDocGuid) {
      Request.EduDocument = eduDocGuid;
      prepareRequest(Request);

      if(Request.Stream === 4) { // spec mag
        if(req.param('subrequest-type') == 1) {
          return createSpecialistRequest(app, Request, PersonCodeU);
        }
        else
        if(req.param('subrequest-type') == 2) {
          return createMagisterRequest(app, Request, PersonCodeU);
        }
        else {
          throw undefined;
        }
      }

      if(Request.Stream === 2) { // jun spec
        if(req.param('subrequest-type') == 3) {
          return createGradedRequest(app, Request, PersonCodeU, 13);
        }
        else
        if(req.param('subrequest-type') == 4) {
          return createGradedRequest(app, Request, PersonCodeU, 14);
        }
        else {
          throw undefined;
        }
      }

      if(Request.Stream === 1) {
        return createBachelorRequest(app, Request, PersonCodeU);
      }

      if(Request.Stream === 3) { // renewal, always throw
        throw undefined;
      }
    })
    .then(function() {
      res.json({ success: true });
    })
    .catch(function(err) {
      if(err) {
        console.error(err.stack || err);
      }

      res.status(500);
      res.end();
    });
};

function createSpecialistRequest(app, Request, PersonCodeU) {
  var rawCode = Request.Speciality.Code.split('.'),
      needle  = '7.' + rawCode[1];

  return createSpecMagRequest(app, Request, PersonCodeU, needle);
}
function createMagisterRequest(app, Request, PersonCodeU) {
  var rawCode = Request.Speciality.Code.split('.'),
      needle  = '8.' + rawCode[1];

  return createSpecMagRequest(app, Request, PersonCodeU, needle);
}
function createSpecMagRequest(app, Request, PersonCodeU, needle) {
  var result = Specialities().filter(function(item) {
    if(item.SpecSpecialityClasifierCode !== needle) {
      return false;
    }
    if(item.Id_PersonEducationForm !== Request.TeachingForm) {
      return false;
    }
    if(item.UniversityFacultetFullName instanceof Object) {
      return false;
    }

    return true;
  });

  if(result.length > 1) {
    throw new Error('Too much specialities found for "' + needle + '"');
  }
  if(result.length === 0) {
    return;
  }

  var params = createParams(Request, PersonCodeU,
                            result[0].UniversitySpecialitiesKode);

  return createGeneralRequest(app, Request, params);
}

function createGradedRequest(app, Request, PersonCodeU, qualification) {
  var needle = Request.Speciality.Code;
  var result = Specialities().filter(function(item) {
    return item.SpecClasifierCode      === needle
        && item.Id_Qualification       === qualification
        && item.Id_PersonEducationForm === Request.TeachingForm
        && !(item.UniversityFacultetFullName instanceof Object);
  });

  if(result.length > 1) {
    throw new Error('Too much specialities found for "' + needle + '"');
  }
  if(result.length === 0) {
    return;
  }

  var params = createParams(Request, PersonCodeU,
                            result[0].UniversitySpecialitiesKode);

  return createGeneralRequest(app, Request, params);
}

function createBachelorRequest(app, Request, PersonCodeU) {
  var needle = Request.Speciality.Code;
  var result = Specialities().filter(function(item) {
    return item.SpecClasifierCode      === needle
        && item.Id_Qualification       === 1
        && item.Id_PersonEducationForm === Request.TeachingForm
        && !(item.UniversityFacultetFullName instanceof Object);
  });

  if(result.length > 1) {
    throw new Error('Too much specialities found for "' + needle + '"');
  }
  if(result.length === 0) {
    return;
  }

  var params = createParams(Request, PersonCodeU,
                            result[0].UniversitySpecialitiesKode);

  return Promise.props({
      subject1: findCertificateSubject(app, Request.Person, PersonCodeU,
                                       Request.CertificateMark1),
      subject2: findCertificateSubject(app, Request.Person, PersonCodeU,
                                       Request.CertificateMark2),
      subject3: findCertificateSubject(app, Request.Person, PersonCodeU,
                                       Request.CertificateMark3)
    })
    .then(function(subjects) {
      params.Id_PersonDocumentSubject1 = subjects.subject1;
      params.Id_PersonDocumentSubject2 = subjects.subject2;
      params.Id_PersonDocumentSubject3 = subjects.subject3;

      return createGeneralRequest(app, Request, params);
    });
}

function prepareRequest(Request) {
  if(!Request.ExaminationCause) {
    Request.ExaminationCause = 0;
  }

  if(Request.Person.NeedHostel) {
    Request.Person.NeedHostel = 1;
  }
  else {
    Request.Person.NeedHostel = 0;
  }

  if(Request.Original) {
    Request.Original = 1;
  }
  else {
    Request.Original = 0;
  }

  if(Request.Courses) {
    Request.Courses = 1;
    Request.CoursesMark = Request.CoursesMark ? Request.CoursesMark : 0;
  }
  else {
    Request.Courses = 0;
    Request.CoursesMark = 0;
  }
}

function createGeneralRequest(app, Request, params) {  
  return app.get('edbo')
    .Person
    .PersonRequestCheckCanAdd({
      Id_PersonRequestSeasons:    params.Id_PersonRequestSeasons,
      PersonCodeU:                params.PersonCodeU,
      UniversitySpecialitiesKode: params.UniversitySpecialitiesKode,
      RequestFromEB:              params.RequestFromEB,
      Id_PersonEducationForm:     params.Id_PersonEducationForm,
      Id_PersonDocument:          params.Id_PersonDocument,
      AutoSelectZnoSubjects:      params.AutoSelectZnoSubjects,
      RequestEnteranceCodes:      params.RequestEnteranceCodes,
    })
    .then(function(result) {
      if(result === 0) {
        throw new Error('ЕДБО заборонило внесення заяви!');
      }

      return app.get('edbo').Person.PersonRequestAdd2(params);
    })
    .then(function(Id_PersonRequest) {
      return app.get('edbo')
        .Person
        .PersonRequestsStatusChange({
          Id_PersonRequest:           Id_PersonRequest,
          Id_PersonRequestStatusType: 4,
          Id_UniversityEntrantWave:   0,
          IsBudejt:                  -1,
          IsContract:                -1,
          Descryption: ''
        });
    });
}

function createParams(Request, PersonCodeU, UniversitySpecialitiesKode) {
  return {
    Id_PersonRequestSeasons:    Seasons(),
    PersonCodeU:                PersonCodeU,
    Id_PersonEducationForm:     Request.TeachingForm,
    CodeOfBusiness:             Request.Number + '',
    UniversitySpecialitiesKode: UniversitySpecialitiesKode,

    OriginalDocumentsAdd: Request.Original,
    Id_PersonDocument:    Request.EduDocument,

    Id_PersonEnteranceTypes:          Request.Type,
    Id_PersonRequestExaminationCause: Request.ExaminationCause,

    Id_UniversitySpecialitiesQuota1: 0,
    Id_UniversitySpecialitiesQuota2: 0,
    Id_UniversitySpecialitiesQuota3: 0,

    RequestFromEB: Request.EZ ? 1 : 0,
    IsBudget:      Request.Budget === true ? 1 : 0,
    IsContract:    1,
    IsNeedHostel:  Request.Person.NeedHostel === true ? 1 : 0,
    Id_LanguageEx: 0,

    Id_PersonDocumentSubject1: 0,
    Id_PersonDocumentSubject2: 0,
    Id_PersonDocumentSubject3: 0,

    Id_PersonCourse: 0,
    PersonRequestCourseBonus: Request.CoursesMark + '',

    Id_PersonOlympiadAward: 0,
    PersonRequestOlympiadAwardBonus: '0',

    AutoSelectZnoSubjects: 0,
    RequestEnteranceCodes: '',
    IsHigherEducation:    -1,
    SkipDocumentValue:     0,

    Id_PersonBenefit1: 0,
    Id_PersonBenefit2: 0,
    Id_PersonBenefit3: 0,

    IsForeignWay:   0,
    Id_ForeignType: 0,

    RequestPriority: Request.Priority ? Request.Priority : 0
  };
}

function findCertificateSubject(app, Person, PersonCodeU, CertificateMark) {
  if(!CertificateMark) {
    return 0;
  }

  return app.get('edbo')
    .Person
    .PersonDocumentsGet({
      PersonCodeU:           PersonCodeU,
      Id_PersonDocumentType: 4, // Certificate
      Id_PersonDocument:     0,
      IsEntrantDocument:    -1,
      UniversityKode:        UniversityKode()
    })
    .then(function(certificates) {
      certificates = certificates.dPersonDocuments;
      certificates = certificates.filter(function(item) {
        return item.DocumentNumbers == CertificateMark.Certificate.Number;
      });

      if(certificates.length !== 1) {
        throw new Error('Неможливо знайти сертифікат!');
      }

      return app.get('edbo')
        .Person
        .PersonDocumentsSubjectsGet({
          Id_PersonDocument:     certificates[0].Id_PersonDocument,
          Id_Person:             0,
          Id_PersonDocumentType: 0
        });
    })
    .then(function(marks) {
      marks = marks.dPersonDocumentsSubjects;
      marks = marks.filter(function(mark) {
        return mark.IdZnoSubject == CertificateMark.Subject;
      });

      if(marks.length !== 1) {
        throw new Error('Неможливо знайти оцінку сертифіката!');
      }

      return marks[0].Id_PersonDocumentSubject;
    });
}
