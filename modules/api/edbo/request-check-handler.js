'use strict';
var handler = require('./request-check');

module.exports = function checkPerson(app, req, res) {
  if(!req.param('id')) {
    res.status(500);
    res.end();
    return;
  }
  if(!app.get('edbo')) {
    res.status(500);
    res.end('EDBO connection is not established!');
    return;
  }

  handler(app, req.param('id'))
    .then(function(request) {
      res.json(request);

      if(request.diff && request.diff.length === 0) {
        return new (app.get('db').RequestModel)({ id: req.param('id') })
          .fetch()
          .then(function(data) {
            return data.save({ Synchronized: true });
          });
      }

      if(request.SubRequests.length > 0) {
        var sync = request.SubRequests.every(function(item) {
          return item && item.diff && item.diff.length === 0;
        });

        if(sync) {
          return new (app.get('db').RequestModel)({ id: req.param('id') })
            .fetch()
            .then(function(data) {
              return data.save({ Synchronized: true });
            });
        }
      }
    })
    .catch(function(err) {
      console.error(err.stack || err);

      res.status(500);
      res.end();
    });
};
