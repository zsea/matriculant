'use strict';
var Promise        = require('bluebird'),
    async          = require('async'),
    Seasons        = require('./cache/seasons'),
    UniversityKode = require('./cache/university-kode'),
    personFind     = require('./person-find'),
    requestFind    = require('./request-find'),
    savePerson     = require('../person/save-person.js'),
    saveRequest    = require('../request/save-request.js');

module.exports = function(app, req, res) {
  return Promise.resolve()
    .then(function() {
      return Seasons(app);
    })
    .then(function() {
      return UniversityKode(app);
    })
    .then(function() {
      return app.get('edbo')
        .Guides
        .UniversityFacultetsGetRequests2({
          Id_PersonRequestSeasons: Seasons(),
          UniversityFacultetKode:  '',
          UniversitySpecialitiesKode: '',
          PersonCodeU: '',

          Id_Qualification: 1,
          MinDate: '',
          Hundred: 0,

          Id_PersonRequestStatusType1: 1,
          Id_PersonRequestStatusType2: 0,
          Id_PersonRequestStatusType3: 0,

          Id_PersonEducationForm: 0,
          UniversityKode: UniversityKode(),

          Filters: ''
        });
    })
    .then(function(result) {
      result = result.dUniversityFacultetsRequests2;
      if(!result) {
        throw undefined;
      }

      result = result.map(function(item) {
        return {
          PersonCodeU:      item.PersonCodeU,
          Id_PersonRequest: item.Id_PersonRequest
        };
      });

      return result;
    })
    .then(function(requests) {
      if(requests.length === 0) {
        return false;
      }
      var item = requests[0];

      return personFind(app, item.PersonCodeU)
        .then(function(person) {
          return findLocalPerson(app, mapPerson(person));
        })
        .then(function(person) {
          return requestFind(app, person, item.Id_PersonRequest);
        })
        .then(function(request) {
          return saveRequest(app, request);
        })
        .then(function() {
          return app.get('edbo')
            .Person
            .PersonRequestsStatusChange({
              Id_PersonRequest:           item.Id_PersonRequest,
              Id_PersonRequestStatusType: 4,
              Id_UniversityEntrantWave:   0,
              IsBudejt:                  -1,
              IsContract:                -1,
              Descryption: ''
            });
        })
        .then(function(request) {
          return true;
        });
    })
    .then(function(exists) {
      if(exists) {
        return res.json({ success: true });
      }

      res.json(404, { notFound: true });
    })
    .catch(function(err) {
      if(err) {
        console.log(err.stack || err);
        res.json(500, err.stack || err);
        return;
      }

      res.json(404, { notFound: true });
    });
};

function mapPerson(Person) {
  Person.Document.id = {
    Series: Person.Document.Series,
    Number: Person.Document.Number
  };

  Person.EduDocuments.forEach(function(item) {
    item.id = {
      Series: item.Series,
      Number: item.Number
    };
  });

  return Person;
}

function findLocalPerson(app, Person) {
  return new(app.get('db').PersonModel)({
    Name: Person.Name,
    Surname: Person.Surname,
    Patronymic: Person.Patronymic,
    BirthDate: Person.BirthDate
  })
  .fetch()
  .then(function(person) {
    if(person) {
      return person.id;
    }

    return savePerson(app, Person);
  })
  .then(function(person) {
    return new(app.get('db').PersonModel)({ id: person })
      .fetch()
      .then(function(model) {
        return model.load([
          'Document',     'EduDocuments', 'Benefits',
          'Certificates', 'Certificates.marks'
        ]);
      })
      .then(function(result) {
        return result.toJSON();
      });
  });
}
