'use strict';
var Promise = require('bluebird'),
    utils   = require('./utils');

module.exports = function personMatch(app, person) {
  return personNaiveMatch(app, person)
    .then(function(persons) {
      if(persons.length !== 1) {
        return personNaiveMatch2(app, person);
      }
      else {
        throw persons[0];
      }
    })
    .then(function(persons) {
      if(persons.length !== 1) {
        return personFullMatch(app, person);
      }

      throw persons[0];
    })
    .catch(function(person) {
      return person;
    });
};

function personNaiveMatch2(app, person) {
  return app.get('edbo')
    .Person
    .PersonsFind2({
      FIOMask: person.Surname + ' ' + person.Name + ' ' + person.Patronymic,
      DocumentSeries: person.EduDocuments[0].Series || '',
      DocumentNumber: person.EduDocuments[0].Number || '',
      Ids_DocumentTypes: '',
      Hundred: 0,
      PersonCodeU: '',
      Filters: ''
    })
    .then(function(found) {
      if(!found.dPersonsFind2 || found.dPersonsFind2.length === 0) {
        return [];
      }

      return found
        .dPersonsFind2.filter(function(item) {
          return person.BirthDate.getDay() == item.Birthday.getDay()
              && person.BirthDate.getMonth() == item.Birthday.getMonth()
              && person.BirthDate.getFullYear() == item.Birthday.getFullYear();
        })
        .map(function(person) {
          return person.PersonCodeU;
        });
    });
}

function personNaiveMatch(app, person) {
  return app.get('edbo')
    .Person
    .PersonsFind2({
      FIOMask: person.Surname + ' ' + person.Name + ' ' + person.Patronymic,
      DocumentSeries: '',
      DocumentNumber: '',
      Ids_DocumentTypes: '',
      Hundred: 0,
      PersonCodeU: '',
      Filters: ''
    })
    .then(function(found) {
      if(!found.dPersonsFind2 || found.dPersonsFind2.length === 0) {
        return [];
      }

      return found.dPersonsFind2
        .filter(function(item) {
          return person.BirthDate.getDay() == item.Birthday.getDay()
              && person.BirthDate.getMonth() == item.Birthday.getMonth()
              && person.BirthDate.getFullYear() == item.Birthday.getFullYear();
        })
        .map(function(person) {
          return person.PersonCodeU;
        });
    });
}

function personFullMatch(app, person) {
  var criteria = createCriteria(app, person);

  return Promise
    .all(criteria)
    .then(function(criteria) {
      var result = [];
      criteria.forEach(function(item) {
        result = result.concat(item);
      });

      return result;
    })
    .then(function(codes) {
      var result = { };
      codes.forEach(function(code) {
        if(!result[code]) {
          result[code] = 1;
        }
        else {
          ++result[code];
        }
      });

      var successor;
      for(var code in result) {
        if(result[code] > 1 && successor) {
          throw new Error('more than one successor');
        }

        if(result[code] > 1) {
          successor = code;
        }
      }

      return successor;
    })
}

function createCriteria(app, person) {
  var criteria = [
    utils.findByName(app, person.Surname, person.Name, person.Patronymic)
  ];
  person.EduDocuments.forEach(function(document) {
    if(document.Series && document.Number) {
      criteria.push(utils.findByDocument(app, document));
    }
  });

  if(person.Document.Series && person.Document.Number) {
    criteria.push(utils.findByDocument(app, person.Document));
  }

  return criteria;
}
