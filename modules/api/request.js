'use strict';
var saveRequestHandler = require('./request/save-request-handler'),
    findAllRequests    = require('./request/find-all-requests'),
    findExactRequest   = require('./request/find-exact-request'),
    generateRequest    = require('./request/generate'),
    generateJournal    = require('./request/journal'),
    examination        = require('./request/examination'),
    setOriginal        = require('./request/set-original'),
    setStatus          = require('./request/set-status'),
    setSubStatus       = require('./request/set-substatus'),
    updateCourses      = require('./request/update-courses'),
    count              = require('./request/count'),
    setExamMark        = require('./request/set-exam-mark');

module.exports = function(app) {
  app
    .post('/api/request', saveRequestHandler.bind(null, app))

    .get('/api/request', findAllRequests.bind(null, app))
    .get('/api/request/generate', generateRequest.bind(null, app))
    .get('/api/request/journal', generateJournal.bind(null, app))
    .get('/api/request/examination', examination.bind(null, app))
    .get('/api/request/count', count.bind(null, app))
    .get('/api/request/:id', findExactRequest.bind(null, app))

    .put('/api/request/:id/set-original', setOriginal.bind(null, app))
    .put('/api/request/:id/set-status', setStatus.bind(null, app))
    .put('/api/request/:id/set-substatus', setSubStatus.bind(null, app))
    .put('/api/request/:id/update-courses', updateCourses.bind(null, app))
    .put('/api/request/:id/set-exam-mark', setExamMark.bind(null, app))

    .delete('/api/request/:id', function(req, res) {
      var id = req.param('id');

      new (app.get('db').RequestModel)({ id: id })
        .fetch()
        .then(function(model) {
          if(!model) {
            throw undefined;
          }

          return model.destroy();
        })
        .then(function() {
          res.json({ success: true });
        })
        .catch(function() {
          res.json(404, {});
        });
    });
};
