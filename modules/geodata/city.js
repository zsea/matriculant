module.exports = function(app, koatuu) {
  if(!/^\d{10}$/.test(koatuu)) {
    return '';
  }

  var levels = [
    parseInt(koatuu.substr(0, 2)),
    parseInt(koatuu.substr(2, 3)),
    parseInt(koatuu.substr(5, 3)),
    parseInt(koatuu.substr(8, 2))
  ];

  if(!levels[2] || levels[2].toString().charAt(0) === '3') {
    return new (app.get('db').Geodata.GeodataLevel2)()
      .query()
      .where('GeodataLevel2.level1',    '=', levels[0])
      .andWhere('GeodataLevel2.level2', '=', levels[1])
      .select([ 'GeodataLevel2.name as level2Name' ])
      .then(function(data) {
        if(data.length === 0) {
          return '';
        }

        return data[0].level2Name;
      });

    return;
  }

  return new (app.get('db').Geodata.GeodataLevel3)()
    .query()
    .where('GeodataLevel3.level1',    '=', levels[0])
    .andWhere('GeodataLevel3.level2', '=', levels[1])
    .andWhere('GeodataLevel3.level3', '=', levels[2])
    .select([ 'GeodataLevel3.name as level3Name' ])
    .then(function(data) {
      if(data.length === 0) {
        return '';
      }

      return data[0].level3Name;
    });
};
