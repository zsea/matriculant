var fs       = require('fs'),
    async    = require('async'),
    globalDB = undefined;

function getClass(code, level) {
  if(level === 1) {
    return parseInt(code.substr(0, 2));
  }
  else if(level === 2) {
    return parseInt(code.substr(2, 3));
  }
  else if(level === 3) {
    return parseInt(code.substr(5, 3));
  }
  else {
    return parseInt(code.substr(8, 2));
  }
}

function process(code, name, done) {
  var L1 = getClass(code, 1);
  var L2 = getClass(code, 2);
  var L3 = getClass(code, 3);
  var L4 = getClass(code, 4);

  if(L2 === 0 && L3 === 0 && L4 === 0) {
    processAsLevel1(L1, name, done);
  }
  else if(L3 === 0 && L4 === 0) {
    processAsLevel2(L1, L2, name, done);
  }
  else if(L4 === 0) {
    processAsLevel3(L1, L2, L3, name, done);
  }
  else
    processAsLevel4(L1, L2, L3, L4, name, done);
}

function processAsLevel1(L1, name, done) {
  if(name[0] == 'А')
    name = name.substr(0, name.indexOf('/'));
  else {
    var space = name.indexOf(' ');
    if(space != -1)
      name = name.substr(0, space) + ' ОБЛ.';
  }
  
  new globalDB.Geodata.GeodataLevel1()
    .model
    .forge({ level1: L1, name: name })
    .save()
    .then(function() {
      done();
    },
    function(e) {
      done(e);
    });
}
function processAsLevel2(L1, L2, name, done) {
  if(L2 % 100 == 0)
    return done();

  var leadDigit = parseInt(L2 / 100);
  if(leadDigit == 2)
    name = name.substr(0, name.indexOf('/'));
  else
  if(leadDigit == 3)
    name += ' РАЙОН';

  new globalDB.Geodata.GeodataLevel2()
    .model
    .forge({ level1: L1, level2: L2, name: name })
    .save()
    .then(function() {
      done();
    },
    function(e) {
      done(e);
    });
}
function processAsLevel3(L1, L2, L3, name, done) {
  if(L3 % 10 == 0)
    return done();

  var leadDigit = parseInt(L3 / 100);
  if(leadDigit == 3)
    name += ' РАЙОН';
  else
  if(leadDigit == 8 || leadDigit == 9)
    name = name.substr(0, name.indexOf('/')) + ' СІЛЬРАДА';

  new globalDB.Geodata.GeodataLevel3()
    .model
    .forge({ level1: L1, level2: L2, level3: L3, name: name })
    .save()
    .then(function() {
      done();
    },
    function(e) {
      done(e);
    });
}
function processAsLevel4(L1, L2, L3, L4, name, done) {
  new globalDB.Geodata.GeodataLevel4()
    .model
    .forge({ level1: L1, level2: L2, level3: L3, level4: L4, name: name })
    .save()
    .then(function() {
      done();
    },
    function(e) {
      done(e);
    });
}

module.exports = function(connection) {
  
  globalDB = require('../db.js')(connection);
  
  fs.readFile('modules/geodata/KOATUU.dat', { encoding: 'utf-8' }, function (err, data) {
    if (err) {
      throw err;
    }
    
    console.log('creating Geodata...');
    
    var lines = data.split('\n');
    async.eachLimit(lines, 10, function(line, done) {
      var chunks = /(\d+) (.+)/.exec(line);
      if(!chunks) {
        return done();
      }
      
      while(chunks[1].length < 10) {
        chunks[1] = '0' + chunks[1];
      }
      
      process(chunks[1], chunks[2], done);
    },
    function(err) {
      if(err) {
        throw err;
      }
      
      console.log('creating Geodata... done!');
    });
  });
  
}