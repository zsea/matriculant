'use strict';

var connectionInstance;

var createCollection = function(dbInstance, name, opts) {
  opts = opts || { };
  opts.tableName = name;

  dbInstance[name + 'Model'] = connectionInstance.Model.extend(opts);
  dbInstance[name] = connectionInstance.Collection.extend({
    model: dbInstance[name + 'Model']
  });
};

module.exports = function(connection) {
  connectionInstance = connection;

  var db = {
    _connection: connection,

    Geodata: { }
  };

  createCollection(db.Geodata, 'GeodataLevel1');
  createCollection(db.Geodata, 'GeodataLevel2');
  createCollection(db.Geodata, 'GeodataLevel3');
  createCollection(db.Geodata, 'GeodataLevel4');

  createCollection(db, 'User');

  createCollection(db, 'Sex');
  createCollection(db, 'StreetType');

  createCollection(db, 'Document');
  createCollection(db, 'DocumentType');
  createCollection(db, 'EduDocument');
  createCollection(db, 'EduDocumentType');
  createCollection(db, 'EduDocumentMarkType');
  createCollection(db, 'EduDocumentAward');

  createCollection(db, 'Benefit', {
    Type: function() {
      return this.belongsTo(db.BenefitTypeModel, 'Type');
    }
  });
  createCollection(db, 'BenefitType');
  createCollection(db, 'EduBenefitType');
  createCollection(db, 'BenefitStrength');

  createCollection(db, 'Certificate', {
    marks: function() {
      return this.hasMany(db.CertificateMark, 'Certificate');
    }
  });
  createCollection(db, 'CertificateMark', {
    Certificate: function() {
      return this.belongsTo(db.CertificateModel, 'Certificate');
    }
  });
  createCollection(db, 'CertificateSubject');

  createCollection(db, 'Person', {
    Document: function() {
      return this.hasOne(db.DocumentModel, 'Person');
    },

    EduDocuments: function() {
      return this.hasMany(db.EduDocument, 'Person');
    },
    Certificates: function() {
      return this.hasMany(db.Certificate, 'Person');
    },
    Benefits: function() {
      return this.hasMany(db.Benefit, 'Person');
    },
    Requests: function() {
      return this.hasMany(db.Request, 'Person');
    }
  });

  createCollection(db, 'Stream');
  createCollection(db, 'TeachingForm');
  createCollection(db, 'Priority');
  createCollection(db, 'ExaminationCause');
  createCollection(db, 'RequestType');
  createCollection(db, 'RequestStatus');
  createCollection(db, 'Faculty');
  createCollection(db, 'SpecialityCapacity');
  createCollection(db, 'Speciality', {
    Faculty: function() {
      return this.belongsTo(db.FacultyModel, 'Faculty');
    },
    Capacity: function() {
      return this.hasMany(db.SpecialityCapacity, 'Speciality');
    }
  });
  createCollection(db, 'SpecialitySubject', {
    Subject: function() {
      return this.belongsTo(db.CertificateSubjectModel, 'Subject');
    }
  });
  createCollection(db, 'ExaminationResult', {
    Request: function() {
      return this.belongsTo(db.RequestModel, 'Request');
    },
    Subject: function() {
      return this.belongsTo(db.CertificateSubjectModel, 'Subject');
    }
  });

  createCollection(db, 'SubRequestType');
  createCollection(db, 'SubRequest', {
    Type: function() {
      return this.belongsTo(db.SubRequestTypeModel, 'Parent');
    }
  });

  createCollection(db, 'Request', {
    Person: function() {
      return this.belongsTo(db.PersonModel, 'Person');
    },
    Exams: function() {
      return this.hasMany(db.ExaminationResult, 'Request');
    },
    Speciality: function() {
      return this.belongsTo(db.SpecialityModel, 'Speciality');
    },
    CertificateMark1: function() {
      return this.belongsTo(db.CertificateMarkModel, 'CertificateMark1');
    },
    CertificateMark2: function() {
      return this.belongsTo(db.CertificateMarkModel, 'CertificateMark2');
    },
    CertificateMark3: function() {
      return this.belongsTo(db.CertificateMarkModel, 'CertificateMark3');
    },
    EduDocument: function() {
      return this.belongsTo(db.EduDocumentModel, 'EduDocument');
    },
    SubRequests: function() {
      return this.hasMany(db.SubRequest, 'Parent');
    },
  });

  return db;

};
