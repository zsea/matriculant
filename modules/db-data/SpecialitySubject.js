'use strict';

var relations = [ ];

var ukrMathGeo = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 8 ]
};

var ukrMathPhys = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 5 ]
};

var ukrMathFor = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 12, 13, 14, 15, 16 ]
};

var ukrMathHis = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 2 ]
};

relations[3] = relations[7] = relations[13] = relations[18] = ukrMathGeo;

relations[8] = relations[6] = relations[10] = relations[2] = 
relations[9] = relations[5] = relations[4] = relations[11] =
relations[1] = relations[12] = ukrMathPhys;

relations[14] = ukrMathFor;

relations[15] = relations[17] = relations[16] = ukrMathHis;

/*relations[2] = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 5 ]
};
relations[4]  = relations[2];
relations[5]  = relations[2];
relations[6]  = relations[2];
relations[7]  = relations[2];
relations[11] = relations[2];

relations[13] = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 5, 8 ]
};

relations[3] = {
  primary:   [ 3, 8 ],
  secondary: [ 1 ],
  optional:  [ 7 ]
};

relations[1] = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 5, 12, 13, 14, 15, 16 ]
};
relations[8]  = relations[1];
relations[12] = relations[1];

relations[9] = {
  primary:   [ 3, 6 ],
  secondary: [ 1 ],
  optional:  [ 5 ]
};

relations[10] = {
  primary:   [ 3, 12, 13, 14, 15, 16 ],
  secondary: [ 1 ],
  optional:  [ 5 ]
};

relations[14] = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 2, 8 ]
};
relations[15] = relations[14];
relations[16] = relations[14];
relations[17] = relations[14];

relations[18] = {
  primary:   [ 3 ],
  secondary: [ 1 ],
  optional:  [ 8, 12, 13, 14, 15, 16 ]
};*/

function primaryMapper(speciality, item) {
  return {
    Speciality: speciality,
    Subject:    item,
    Primary:    true,
    Secondary:  false
  };
}
function secondaryMapper(speciality, item) {
  return {
    Speciality: speciality,
    Subject:    item,
    Primary:    false,
    Secondary:  true
  };
}
function optionalMapper(speciality, item) {
  return {
    Speciality: speciality,
    Subject:    item,
    Primary:    false,
    Secondary:  false
  };
}

var result = [];
for(var i = 1; i <= 18; ++i) { // LENGTH
  var primary   = relations[i].primary.map(primaryMapper.bind(null, i)),
      secondary = relations[i].secondary.map(secondaryMapper.bind(null, i)),
      optional  = relations[i].optional.map(optionalMapper.bind(null, i));

  result = result.concat(primary).concat(secondary).concat(optional);
}

module.exports = result;
