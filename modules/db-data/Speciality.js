module.exports = [
  {
    "id": 1,
    "Name": "Програмна інженерія",
    "Hash": "СП",
    "Code": "6.050103",
    "Parent": null,
    "Faculty": 1,
    "Stream": 1
  },
  {
    "id": 2,
    "Name": "Металургія",
    "Hash": "МЕТ",
    "Code": "6.050401",
    "Parent": null,
    "Faculty": 4,
    "Stream": 1
  },
  {
    "id": 3,
    "Name": "Екологія, Охорона навколишнього середовища та збалансоване природокористування",
    "Hash": "ОНС",
    "Code": "6.040106",
    "Parent": null,
    "Faculty": 4,
    "Stream": 1
  },
  {
    "id": 4,
    "Name": "Охорона праці",
    "Hash": "ОП",
    "Code": "6.170202",
    "Parent": null,
    "Faculty": 4,
    "Stream": 1
  },
  {
    "id": 5,
    "Name": "Машинобудування",
    "Hash": "МБ",
    "Code": "6.050503",
    "Parent": null,
    "Faculty": 4,
    "Stream": 1
  },
  {
    "id": 6,
    "Name": "Будівництво",
    "Hash": "БУД",
    "Code": "6.060101",
    "Parent": null,
    "Faculty": 5,
    "Stream": 1
  },
  {
    "id": 7,
    "Name": "Гідротехніка (водні ресурси)",
    "Hash": "ВР",
    "Code": "6.060103",
    "Parent": null,
    "Faculty": 5,
    "Stream": 1
  },
  {
    "id": 8,
    "Name": "Автоматизація та комп’ютерно-інтегровані технології",
    "Hash": "АТП",
    "Code": "6.050202",
    "Parent": null,
    "Faculty": 1,
    "Stream": 1
  },
  {
    "id": 9,
    "Name": "Мікро- та наноелектроніка",
    "Hash": "МН",
    "Code": "6.050801",
    "Parent": null,
    "Faculty": 1,
    "Stream": 1
  },
  {
    "id": 10,
    "Name": "Електронні пристої та системи",
    "Hash": "ЕС",
    "Code": "6.050802",
    "Parent": null,
    "Faculty": 1,
    "Stream": 1
  },
  {
    "id": 11,
    "Name": "Теплоенергетика",
    "Hash": "ТЕ",
    "Code": "6.050601",
    "Parent": null,
    "Faculty": 2,
    "Stream": 1
  },
  {
    "id": 12,
    "Name": "Електротехніка та електротехнології",
    "Hash": "ЕТ",
    "Code": "6.050701",
    "Parent": null,
    "Faculty": 2,
    "Stream": 1
  },
  {
    "id": 13,
    "Name": "Гідроенергетика",
    "Hash": "ГЕ",
    "Code": "6.050602",
    "Parent": null,
    "Faculty": 2,
    "Stream": 1
  },
  {
    "id": 14,
    "Name": "Економічна кібернетика",
    "Hash": "ЕК",
    "Code": "6.030502",
    "Parent": null,
    "Faculty": 3,
    "Stream": 1
  },
  {
    "id": 15,
    "Name": "Економіка підприємства",
    "Hash": "ЕП",
    "Code": "6.030504",
    "Parent": null,
    "Faculty": 3,
    "Stream": 1
  },
  {
    "id": 16,
    "Name": "Фінанси і кредит",
    "Hash": "Ф",
    "Code": "6.030508",
    "Parent": null,
    "Faculty": 3,
    "Stream": 1
  },
  {
    "id": 17,
    "Name": "Облік і аудит",
    "Hash": "ОА",
    "Code": "6.030509",
    "Parent": null,
    "Faculty": 3,
    "Stream": 1
  },
  {
    "id": 18,
    "Name": "Менеджмент",
    "Hash": "М",
    "Code": "6.030601",
    "Parent": null,
    "Faculty": 3,
    "Stream": 1
  },

  {
    "id": 19,
    "Name": "Програмне забезпечення систем",
    "Hash": "ПЗС",
    "Code": "7(8).05010301",
    "Parent": 1,
    "Faculty": 1,
    "Stream": 4
  },
  {
    "id": 20,
    "Name": "Металургія чорних металів",
    "Hash": "МЧМ",
    "Code": "7(8).05040101",
    "Parent": 2,
    "Faculty": 4,
    "Stream": 4
  },
  {
    "id": 21,
    "Name": "Металургія кольорових металів",
    "Hash": "МКМ",
    "Code": "7(8).05040102",
    "Parent": 2,
    "Faculty": 4,
    "Stream": 4
  },
  {
    "id": 22,
    "Name": "Обробка металів тиском",
    "Hash": "ОМТ",
    "Code": "7(8).05040104",
    "Parent": 2,
    "Faculty": 4,
    "Stream": 4
  },
  {
    "id": 23,
    "Name": "Прикладна екологія та збалансоване природокористування",
    "Hash": "ПЕЗП",
    "Code": "7.04010602",
    "Parent": 3,
    "Faculty": 4,
    "Stream": 4
  },
  {
    "id": 24,
    "Name": "Металургійне обладнання",
    "Hash": "МО",
    "Code": "7(8).05050311",
    "Parent": 5,
    "Faculty": 4,
    "Stream": 4
  },
  {
    "id": 25,
    "Name": "Промислове і цивільне будівництво",
    "Hash": "ПЦБ",
    "Code": "7(8).06010101",
    "Parent": 6,
    "Faculty": 5,
    "Stream": 4
  },
  {
    "id": 26,
    "Name": "Міське будівництво і господарство",
    "Hash": "МБГ",
    "Code": "7(8).06010103",
    "Parent": 6,
    "Faculty": 5,
    "Stream": 4
  },
  {
    "id": 27,
    "Name": "Водопостачання та водовідведення",
    "Hash": "ВВ",
    "Code": "7(8).06010108",
    "Parent": 6,
    "Faculty": 5,
    "Stream": 4
  },
  {
    "id": 28,
    "Name": "Автоматизоване управління технологічними процесами",
    "Hash": "АУТП",
    "Code": "7(8).05020201",
    "Parent": 8,
    "Faculty": 1,
    "Stream": 4
  },
  {
    "id": 29,
    "Name": "Фізична та біомедична електроніка",
    "Hash": "ФБМЕ",
    "Code": "7(8).05080102",
    "Parent": 9,
    "Faculty": 1,
    "Stream": 4
  },
  {
    "id": 30,
    "Name": "Електронні системи",
    "Hash": "ЕС",
    "Code": "7(8).05080202",
    "Parent": 10,
    "Faculty": 1,
    "Stream": 4
  },
  {
    "id": 31,
    "Name": "Теплоенергетика",
    "Hash": "ТЕ",
    "Code": "7(8).05060101",
    "Parent": 11,
    "Faculty": 2,
    "Stream": 4
  },
  {
    "id": 32,
    "Name": "Енергетичний менеджмент",
    "Hash": "ЕМ",
    "Code": "7(8).05060105",
    "Parent": 12,
    "Faculty": 2,
    "Stream": 4
  },
  {
    "id": 33,
    "Name": "Гідроенергетика",
    "Hash": "ГЕ",
    "Code": "7(8).05060201",
    "Parent": 13,
    "Faculty": 2,
    "Stream": 4
  },
  {
    "id": 34,
    "Name": "Економічна кібернетика",
    "Hash": "ЕК",
    "Code": "7(8).03050201",
    "Parent": 14,
    "Faculty": 3,
    "Stream": 4
  },
  {
    "id": 35,
    "Name": "Економіка підприємства",
    "Hash": "ЕП",
    "Code": "7(8).03050401",
    "Parent": 15,
    "Faculty": 3,
    "Stream": 4
  },
  {
    "id": 36,
    "Name": "Фінанси і кредит",
    "Hash": "Ф",
    "Code": "7(8).03050801",
    "Parent": 16,
    "Faculty": 3,
    "Stream": 4
  },
  {
    "id": 37,
    "Name": "Облік і аудит",
    "Hash": "ОА",
    "Code": "7(8).03050901",
    "Parent": 17,
    "Faculty": 3,
    "Stream": 4
  },
  {
    "id": 38,
    "Name": "Менеджмент",
    "Hash": "М",
    "Code": "7(8).03060101",
    "Parent": 18,
    "Faculty": 3,
    "Stream": 4
  },

  {
    "id": 39,
    "Name": "Програмна інженерія",
    "Hash": "СП",
    "Code": "6.050103",
    "Parent": null,
    "Faculty": 1,
    "Stream": 2
  },
  {
    "id": 40,
    "Name": "Металургія",
    "Hash": "МЕТ",
    "Code": "6.050401",
    "Parent": null,
    "Faculty": 4,
    "Stream": 2
  },
  {
    "id": 41,
    "Name": "Екологія, Охорона навколишнього середовища та збалансоване природокористування",
    "Hash": "ОНС",
    "Code": "6.040106",
    "Parent": null,
    "Faculty": 4,
    "Stream": 2
  },
  {
    "id": 42,
    "Name": "Охорона праці",
    "Hash": "ОП",
    "Code": "6.170202",
    "Parent": null,
    "Faculty": 4,
    "Stream": 2
  },
  {
    "id": 43,
    "Name": "Машинобудування",
    "Hash": "МБ",
    "Code": "6.050503",
    "Parent": null,
    "Faculty": 4,
    "Stream": 2
  },
  {
    "id": 44,
    "Name": "Будівництво",
    "Hash": "БУД",
    "Code": "6.060101",
    "Parent": null,
    "Faculty": 5,
    "Stream": 2
  },
  {
    "id": 45,
    "Name": "Гідротехніка (водні ресурси)",
    "Hash": "ВР",
    "Code": "6.060103",
    "Parent": null,
    "Faculty": 5,
    "Stream": 2
  },
  {
    "id": 46,
    "Name": "Автоматизація та комп’ютерно-інтегровані технології",
    "Hash": "АТП",
    "Code": "6.050202",
    "Parent": null,
    "Faculty": 1,
    "Stream": 2
  },
  {
    "id": 47,
    "Name": "Мікро- та наноелектроніка",
    "Hash": "МН",
    "Code": "6.050801",
    "Parent": null,
    "Faculty": 1,
    "Stream": 2
  },
  {
    "id": 48,
    "Name": "Електронні пристої та системи",
    "Hash": "ЕС",
    "Code": "6.050802",
    "Parent": null,
    "Faculty": 1,
    "Stream": 2
  },
  {
    "id": 49,
    "Name": "Теплоенергетика",
    "Hash": "ТЕ",
    "Code": "6.050601",
    "Parent": null,
    "Faculty": 2,
    "Stream": 2
  },
  {
    "id": 50,
    "Name": "Електротехніка та електротехнології",
    "Hash": "ЕТ",
    "Code": "6.050701",
    "Parent": null,
    "Faculty": 2,
    "Stream": 2
  },
  {
    "id": 51,
    "Name": "Гідроенергетика",
    "Hash": "ГЕ",
    "Code": "6.050602",
    "Parent": null,
    "Faculty": 2,
    "Stream": 2
  },
  {
    "id": 52,
    "Name": "Економічна кібернетика",
    "Hash": "ЕК",
    "Code": "6.030502",
    "Parent": null,
    "Faculty": 3,
    "Stream": 2
  },
  {
    "id": 53,
    "Name": "Економіка підприємства",
    "Hash": "ЕП",
    "Code": "6.030504",
    "Parent": null,
    "Faculty": 3,
    "Stream": 2
  },
  {
    "id": 54,
    "Name": "Фінанси і кредит",
    "Hash": "Ф",
    "Code": "6.030508",
    "Parent": null,
    "Faculty": 3,
    "Stream": 2
  },
  {
    "id": 55,
    "Name": "Облік і аудит",
    "Hash": "ОА",
    "Code": "6.030509",
    "Parent": null,
    "Faculty": 3,
    "Stream": 2
  },
  {
    "id": 56,
    "Name": "Менеджмент",
    "Hash": "М",
    "Code": "6.030601",
    "Parent": null,
    "Faculty": 3,
    "Stream": 2
  },
  {
    "id": 57,
    "Name": "Теплогазопостачання та вентиляція",
    "Hash": "ТГП",
    "Code": "7(8).06010107",
    "Parent": 6,
    "Faculty": 2,
    "Stream": 4
  },
  {
    "id": 58,
    "Name": "Управління фінансово-економічною безпекою",
    "Hash": "УФБ",
    "Code": "7(8).18010014",
    "Parent": null,
    "Faculty": 3,
    "Stream": 4
  },
  {
    "id": 59,
    "Name": "Оподаткування",
    "Hash": "ОПД",
    "Code": "7(8).03050803",
    "Parent": 16,
    "Faculty": 3,
    "Stream": 4
  },
  {
    "id": 60,
    "Name": "Мікроелектронні інформаційні системи",
    "Hash": "МІС",
    "Code": "7(8).05080103",
    "Parent": 9,
    "Faculty": 1,
    "Stream": 4
  },
  {
    "id": 61,
    "Name": "Управління проектами",
    "Hash": "УП",
    "Code": "7(8).18010013",
    "Parent": 18,
    "Faculty": 3,
    "Stream": 4
  }
];
