CREATE OR REPLACE FUNCTION CalculateRequestNumber(Speciality integer,
                                                  Stream integer,
                                                  TeachingForm integer)
RETURNS integer AS $Result$

declare
  Result integer;

BEGIN
  SELECT MAX("Number") into Result
    FROM "Request"
    WHERE "Speciality" = Speciality AND "Stream" = Stream AND "TeachingForm" = TeachingForm
    GROUP BY "Number"
    ORDER BY 1 DESC
    LIMIT 1;

  Result = COALESCE(Result, 0) + 1;

  RETURN Result;
END;

$Result$ LANGUAGE plpgsql;
