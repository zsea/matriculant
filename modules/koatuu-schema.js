module.exports = function(connection) {

  var schema = connection.knex.schema;
  
  schema.hasTable('GeodataLevel1').then(function(exist) {
    if(!exist) {
      schema
        // drop
        .dropTableIfExists('GeodataLevel1')
        .then(function() {
          return schema.dropTableIfExists('GeodataLevel2');
        })
        .then(function() {
          return schema.dropTableIfExists('GeodataLevel3');
        })
        .then(function() {
          return schema.dropTableIfExists('GeodataLevel4');
        })
      
        // create
        .then(function() {
          return schema.createTable('GeodataLevel1', function(table) {
            //schema definition
            
            table.increments('id');
            table.integer('level1');
            table.string('name');
          })
        })
        .then(function() {
          return schema.createTable('GeodataLevel2', function(table) {
            //schema definition

            table.increments('id');
            table.integer('level1');
            table.integer('level2');
            table.string('name');
          });
        })
        .then(function() {
          return schema.createTable('GeodataLevel3', function(table) {
            //schema definition
            
            table.increments('id');
            table.integer('level1');
            table.integer('level2');
            table.integer('level3');
            table.string('name');
          });
        })
        .then(function() {
          return schema.createTable('GeodataLevel4', function(table) {
            //schema definition
            
            table.increments('id');
            table.integer('level1');
            table.integer('level2');
            table.integer('level3');
            table.integer('level4');
            table.string('name');
          });
        })
        .then(function() {
          console.log('`geodata`-family tables is created.');
          require('./geodata/fill.js')(connection);
        });
    }
  });
  
}