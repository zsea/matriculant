'use strict';
var async = require('async'),
    path  = require('path'),
    fs    = require('fs');

function createSchema(name, definition, created, done) {
  if(!done) {
    done = created;
    created = undefined;
  }

  var schema = this;
  schema.hasTable(name).then(function(exist) {
    if(!exist) {
      schema
        .createTable(name, definition)
        .then(function() {
          console.log('Table "' + name + '" is created.');

          if(created) {
            created(done);
          }
          else {
            done();
          }
        });
    }
    else {
      done();
    }
  });
}

function readData(name) {
  return require(path.join(__dirname, 'db-data', name + '.js'));
}

module.exports = function(connection) {

  var schema = connection.knex.schema;

  var order = [
    createSchema.bind(schema, 'User', function(table) {
      table.increments('id').primary();
      table.string('Name');
      table.string('Login');
      table.string('Hash');
      table.integer('Level');
    },
    function(done) {
      connection.knex('User')
        .insert([
          { // "admin": "zsea-secret"
            Name: 'Admin',
            Login: 'admin',
            Hash: '57216641b10c16f375803edb46a4b66495bb89b62e6826c8fd01b5c432999026',
            Level: 0
          }
        ])
        .then(function() {
          done();
        });
    }),

    createSchema.bind(schema, 'Sex', function(table) {
        table.increments('id').primary();
        table.string('SexName');
        table.string('SexShort');
      },
      function(done) {
        connection.knex('Sex')
          .insert([
            { SexName: 'чоловіча', SexShort: 'чол.' },
            { SexName: 'жіноча', SexShort: 'жін.' },
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'StreetType', function(table) {
        table.increments('id').primary();
        table.string('Name');
        table.string('Short');
      },
      function(done) {
        connection.knex('StreetType')
          .insert([
            { id: 0,   Name: '',           Short: ''           },
            { id: 1,   Name: 'вулиця',     Short: 'вул.'       },
            { id: 2,   Name: 'бульвар',    Short: 'бул.'       },
            { id: 3,   Name: 'площа',      Short: 'пл.'        },
            { id: 4,   Name: 'провулок',   Short: 'пров.'      },
            { id: 5,   Name: 'проспект',   Short: 'просп.'     },
            { id: 6,   Name: 'квартал',    Short: 'квартал'    },
            { id: 7,   Name: 'майдан',     Short: 'майдан'     },
            { id: 8,   Name: 'мікрорайон', Short: 'мікрорайон' },
            { id: 9,   Name: 'тупик',      Short: 'туп.'       },
            { id: 10,  Name: 'дільниця',   Short: 'дільниця'   },
            { id: 11,  Name: 'проїзд',     Short: 'проїзд'     },
            { id: 12,  Name: 'узвіз',      Short: 'узвіз'      },
            { id: 13,  Name: 'шосе',       Short: 'шс.'        },
            { id: 100, Name: 'набережна',  Short: 'набережна'  }
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Person', function(table) {
        table.increments('id').primary();
        table.string('Name');
        table.string('Surname');
        table.string('Patronymic');
        table.date('BirthDate');
        table.string('BirthPlace'); // koatuu
        table.string('LivingPlace'); // koatuu
        table.string('LivingStreet');
        table.string('PhoneNumber');
        table.string('PostCode');
        table.boolean('NeedHostel');
        table.boolean('IsForeigner');
        table.boolean('Synchronized').defaultTo(false);
        table.integer('FilledBy')
             .references('id')
             .inTable('User')
             .onDelete('SET NULL');

        table.integer('StreetType')
             .references('id')
             .inTable('StreetType');

        table.integer('Sex')
             .references('id')
             .inTable('Sex');
        table.string('IPN');
      }),

    createSchema.bind(schema, 'BenefitStrength', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('BenefitStrength')
          .insert([
            { Name: 'Позаконкурсний вступ' },
            { Name: 'Першочерговий вступ'  },
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'BenefitType', function(table) {
        table.increments('id').primary();
        table.string('Name');
        table.string('Description');
        table.integer('Strength')
             .references('id')
             .inTable('BenefitStrength');
      },
      function(done) {
        connection.knex('BenefitType')
          .insert(readData('BenefitType'))
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Benefit', function(table) {
        table.increments('id').primary();
        table.integer('Person')
             .references('id')
             .inTable('Person')
             .onDelete('CASCADE');

        table.integer('Type')
             .references('id')
             .inTable('BenefitType');
      }),

    createSchema.bind(schema, 'DocumentType', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('DocumentType')
          .insert([
            { id: 1,  Name: 'Свідоцтво про народження' },
            { id: 3,  Name: 'Паспорт' },
            { id: 17, Name: 'Посвідка на постійне проживання' },
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Document', function(table) {
        table.increments('id').primary();
        table.integer('Person')
             .references('id')
             .inTable('Person')
             .onDelete('CASCADE');

        table.integer('Type')
             .references('id')
             .inTable('DocumentType');

        table.string('Series');
        table.string('Number');
        table.date('Date');
        table.string('GivenBy');
      }),

    createSchema.bind(schema, 'EduDocumentType', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('EduDocumentType')
          .insert([
            { id: 2,  Name: 'Атестат про повну загальну середню освіту' },
            { id: 9,  Name: 'Диплом кваліфікованого робітника' },
            { id: 10, Name: 'Диплом молодшого спеціаліста' },
            { id: 11, Name: 'Диплом бакалавра' },
            { id: 12, Name: 'Диплом спеціаліста' },
            { id: 13, Name: 'Диплом магістра' },
            { id: 14, Name: 'Академічна довідка' },
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'EduDocumentMarkType', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('EduDocumentMarkType')
          .insert([
            { id: 5,   Name: '5-ти бальна' },
            { id: 12,  Name: '12-ти бальна' },
            { id: 60, Name: '60 бальна' },
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'EduDocumentAward', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('EduDocumentAward')
          .insert([
            { id: 0,   Name: 'Немає'         },
            { id: 1,   Name: 'Золота медаль' },
            { id: 2,   Name: 'Срібна медаль' },
            { id: 3,   Name: 'З відзнакою'   },
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'EduDocument', function(table) {
        table.increments('id').primary();
        table.integer('Person')
             .references('id')
             .inTable('Person')
             .onDelete('CASCADE');

        table.integer('Type')
             .references('id')
             .inTable('EduDocumentType');

        table.string('Series');
        table.string('Number');
        table.date('Date');
        table.string('GivenBy');
        table.float('Mark');
        table.integer('MarkType')
             .references('id')
             .inTable('EduDocumentMarkType');

        table.integer('Award')
             .references('id')
             .inTable('EduDocumentAward');
      }),

    createSchema.bind(schema, 'CertificateSubject', function(table) {
        table.increments('id').primary();
        table.integer('unused');
        table.string('Name');
      },
      function(done) {
        connection.knex('CertificateSubject')
          .insert(readData('CertificateSubject'))
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Certificate', function(table) {
        table.increments('id').primary();
        table.integer('Person')
             .references('id')
             .inTable('Person')
             .onDelete('CASCADE');

        table.string('Number');
        table.integer('Year');
        table.string('Pin');
      }),

    createSchema.bind(schema, 'CertificateMark', function(table) {
        table.increments('id').primary();
        table.integer('Certificate')
             .references('id')
             .inTable('Certificate')
             .onDelete('CASCADE');

        table.integer('Subject')
             .references('id')
             .inTable('CertificateSubject');

        table.float('Mark');
      }),

    createSchema.bind(schema, 'TeachingForm', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('TeachingForm')
          .insert([
            { id: 1, Name: 'Денна'  },
            { id: 2, Name: 'Заочна' }
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'RequestType', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('RequestType')
          .insert([
            { id: 1, Name: 'За даними сертифікату'   },
            { id: 2, Name: 'За результатами іспитів' }
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'ExaminationCause', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('ExaminationCause')
          .insert(readData('ExaminationCause'))
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Stream', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('Stream')
          .insert([
            { id: 1, Name: 'Бакалавр'            },
            { id: 2, Name: 'Молодший спеціаліст' },
            { id: 3, Name: 'Поновлення'          },
            { id: 4, Name: 'Спеціаліст, магістр' }
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Faculty', function(table) {
        table.increments('id').primary();
        table.string('Name');
        table.string('ShortName');
      },
      function(done) {
        connection.knex('Faculty')
          .insert(readData('Faculty'))
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Speciality', function(table) {
        table.increments('id').primary();
        table.string('Name');
        table.string('Hash');
        table.string('Code');
        table.integer('Stream')
             .references('id')
             .inTable('Stream')
             .onDelete('RESTRICT');

        table.integer('Parent')
             .references('id')
             .inTable('Speciality')
             .onDelete('RESTRICT');

        table.integer('Faculty')
             .references('id')
             .inTable('Faculty')
             .onDelete('RESTRICT');
      },
      function(done) {
        connection.knex('Speciality')
          .insert(readData('Speciality'))
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'RequestStatus', function(table) {
        table.increments('id').primary();
        table.string('Name');
        table.boolean('Active');
      },
      function(done) {
        var fnPath = path.join(__dirname, 'db-data', 'CalculateRequestNumber.sql'),
            fn = fs.readFileSync(fnPath, { encoding: 'utf-8' });

        connection.knex('RequestStatus')
          .insert([
            { id: 1, Name: 'Нова зява' },
            { id: 2, Name: 'Відмова'   },
            { id: 3, Name: 'Скасовано' },
            { id: 4, Name: 'Допущено'  },
            { id: 5, Name: 'Рекомендовано' },
            { id: 6, Name: 'Відхилено' },
            { id: 7, Name: 'До наказу' },
            { id: 8, Name: 'Заява надійшла з сайту'},
            { id: 9, Name: 'Затримано' },
          ])
          .then(function() {
            return connection.knex.raw(fn);
          })
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Priority', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('Priority')
          .insert([
            { id: 1,  Name: '1' },
            { id: 2,  Name: '2' },
            { id: 3,  Name: '3' },
            { id: 4,  Name: '4' },
            { id: 5,  Name: '5' },
            { id: 6,  Name: '6' },
            { id: 7,  Name: '7' },
            { id: 8,  Name: '8' },
            { id: 9,  Name: '9' },
            { id: 10, Name: '10' },
            { id: 11, Name: '11' },
            { id: 12, Name: '12' },
            { id: 13, Name: '13' },
            { id: 14, Name: '14' },
            { id: 15, Name: '15' }
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'Request', function(table) {
        table.increments('id').primary();
        table.integer('Number');
        table.integer('Person')
             .references('id')
             .inTable('Person')
             .onDelete('CASCADE');

        table.integer('Type')
             .references('id')
             .inTable('RequestType')
             .onDelete('CASCADE');

        table.integer('Speciality')
             .references('id')
             .inTable('Speciality')
             .onDelete('CASCADE');

        table.integer('ExaminationCause')
             .references('id')
             .inTable('ExaminationCause')
             .onDelete('RESTRICT');

        table.integer('CertificateMark1')
             .references('id')
             .inTable('CertificateMark')
             .onDelete('RESTRICT');

        table.integer('CertificateMark2')
             .references('id')
             .inTable('CertificateMark')
             .onDelete('RESTRICT');

        table.integer('CertificateMark3')
             .references('id')
             .inTable('CertificateMark')
             .onDelete('RESTRICT');

        table.integer('EduDocument')
             .references('id')
             .inTable('EduDocument')
             .onDelete('RESTRICT');

        table.integer('Status')
             .references('id')
             .inTable('RequestStatus')
             .onDelete('CASCADE');

        table.integer('TeachingForm')
             .references('id')
             .inTable('TeachingForm')
             .onDelete('CASCADE');

        table.integer('Stream')
             .references('id')
             .inTable('Stream')
             .onDelete('RESTRICT');

        table.boolean('Budget');
        table.boolean('Original');
        table.boolean('Courses');
        table.float('CoursesMark');
        table.date('Date').defaultTo('now');
        table.boolean('EZ').defaultTo('false');

        table.boolean('Synchronized');
        table.integer('FilledBy')
             .references('id')
             .inTable('User')
             .onDelete('SET NULL');
        table.integer('Priority')
              .references('id')
             .inTable('Priority')
             .onDelete('RESTRICT');
      }),

    createSchema.bind(schema, 'SubRequestType', function(table) {
        table.increments('id').primary();
        table.string('Name');
      },
      function(done) {
        connection.knex('SubRequestType')
          .insert([
            { id: 1, Name: 'Спеціаліст' },
            { id: 2, Name: 'Магістр'    },
            { id: 3, Name: '2 курс'     },
            { id: 4, Name: '3 курс'     }
          ])
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'SubRequest', function(table) {
        table.increments('id').primary();
        table.integer('Parent')
             .references('id')
             .inTable('Request')
             .onDelete('CASCADE');

        table.integer('Status')
             .references('id')
             .inTable('RequestStatus')
             .onDelete('CASCADE');

        table.integer('Type')
             .references('id')
             .inTable('SubRequestType')
             .onDelete('RESTRICT');
      }),

    createSchema.bind(schema, 'SpecialityCapacity', function(table) {
        table.increments('id').primary();

        table.integer('Speciality')
             .references('id')
             .inTable('Speciality')
             .onDelete('RESTRICT');

        table.integer('TeachingForm')
             .references('id')
             .inTable('TeachingForm')
             .onDelete('RESTRICT');

        table.integer('SubRequestType')
             .references('id')
             .inTable('SubRequestType')
             .onDelete('RESTRICT');

        table.integer('BudgetCapacity');
      }),

    createSchema.bind(schema, 'SpecialitySubject', function(table) {
        table.integer('Speciality')
             .references('id')
             .inTable('Speciality')
             .onDelete('CASCADE');

        table.integer('Subject')
             .references('id')
             .inTable('CertificateSubject')
             .onDelete('CASCADE');

        table.boolean('Primary');
        table.boolean('Secondary');

        table.primary(['Speciality', 'Subject']);
      },
      function(done) {
        connection.knex('SpecialitySubject')
          .insert(readData('SpecialitySubject'))
          .then(function() {
            done();
          });
      }),

    createSchema.bind(schema, 'ExaminationResult', function(table) {
        table.increments('id').primary();
        table.integer('Request')
             .references('id')
             .inTable('Request')
             .onDelete('CASCADE');

        table.boolean('Primary');
        table.boolean('Foreign');
        table.integer('Subject')
             .references('id')
             .inTable('CertificateSubject')
             .onDelete('RESTRICT');
        table.float('Mark');
      })
  ];

  async.series(order);
};
