'use strict';
var nconf     = require('nconf'),
    Bookshelf = require('bookshelf'),
    config    = nconf.file({ file: __dirname + '/../config/config.json' });

var Connection = Bookshelf.initialize({
  client: 'pg',
  connection: {
    host     : config.get('database:host'),
    user     : config.get('database:user'),
    password : config.get('database:password'),
    database : config.get('database:name'),
    charset  : 'utf8'
  }
});

module.exports = Connection;
