'use strict';

delete process.env.http_proxy;
delete process.env.HTTP_PROXY;

var EDBO    = require('./modules/edbo.js'),
    Promise = require('bluebird'),
    async   = require('async'),
    PersonMatch = require('./modules/api/edbo/person-match');

var app = require('express')();

EDBO.then(function(session) {
  app.set('db', require('./modules/db.js')(require('./modules/connection.js')));
  app.set('edbo', session);

  function selector(params) {
    params.where({ Stream: 4, TeachingForm: 1 });
    params.orderBy('Speciality');
  }

  new (app.get('db').Request)().query(selector)
    .fetch()
    .then(function(model) {
      return model.load([ 'Speciality', 'Person' ]);
    })
    .then(function(model) {
      var requests = model.toJSON();

      async.eachSeries(requests, function(request, next) {
        findPersonCodeU(request.Person.id)
          .then(function(PersonCodeU) {
            return app.get('edbo').Person.PersonEducationsGet({
              PersonCodeU: PersonCodeU,
              Id_PersonEducation: 0,
              Id_PersonEducationType: 3,
              Filters: ''
            });
          })
          .then(function(educations) {
            educations = educations.dPersonEducations;
            if(!educations) {
              return {
                code: '.ERROR',
                name: 'ERROR'
              };
            }

            educations = educations.filter(function(education) {
              return education.QualificationName.indexOf('Бакалавр') !== -1
                && education.PersonEducationHistoryTypeName === 'Закінчення навчання';
            });

            var education = educations[0];

            if(!education) {
              return {
                code: '.ERROR',
                name: 'ERROR'
              };
            }

            return {
              code: education.SpecClasifierCode,
              name: education.SpecDirectionName
            };
          })
          .then(function(data) {
            process.stderr.write(Math.random().toString() + '\n'); // imitate progress :)

            console.log(
              request.Person.Surname,
              request.Person.Name + ';',
              data.name + ';', request.Speciality.Name + ';',
              getLastCode(data.code) + ';', getLastCode(request.Speciality.Code)
            );

            next();
          });
      }, function() {
        process.stderr.write('done!');
      })
    });
});

var cache = { };

function findPersonCodeU(personId) {
  if (cache[personId]) {
    return Promise.resolve(cache[personId]);
  }

  return new (app.get('db').PersonModel)({ id: personId })
    .fetch()
    .then(function(model) {
      return model.load([
        'Document',
        'EduDocuments',
        'Certificates', 'Certificates.marks'
      ]);
    })
    .then(function(model) {
      return PersonMatch(app, model.toJSON());
    })
    .then(function(PersonCodeU) {
      return cache[personId] = PersonCodeU;
    })
}

function getLastCode(code) {
  return code.substr(code.indexOf('.') + 1);
}
