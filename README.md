# Matriculant

## About
**Zaporizhia State Engineering Academy** entrance's tool for accounting enrollees.


## Using recipe
Install requirements:

1. ~~somehow~~ install **node.js** and **PostgreSQL**
2. `bower install`
3. `npm install`
4. `touch config/config.json`
5. ~~somehow~~ fill the config
6. `npm install -g grunt-cli`

And finally run: `grunt`

## Config
There is a typical **config/config.json**:
```json
{
  "database": {
    "host": "localhost",
    "name": "matriculant-current",

    "user":     "db_user",
    "password": "db_password",
  },

  "edbo": {
    "host": "fakehost.local",
    "port": 8080,

    "appkey":   "my_extremely_secret_key",
    "user":     "fake_user",
    "password": "fake_password"
  }
}
```
Sure, you can use real EDBO connection details and auth credentials.
