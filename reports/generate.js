'use strict';

var httpPort = require('../package.json').httpPort;

var wkhtmltopdf = require('wkhtmltopdf'),
    querystring = require('querystring'),
    path = require('path'),
    fs   = require('fs');

function trim(number) {
  return number < 10 ? '0' + number : number;
}
function getFileName(date, teachingForm, passed) {
  return ((teachingForm === 2) ? 'zao_' : '') + 
         ((passed === 1) ? 'passed_' : '')  + 
         [
           date.getFullYear(),
           trim(date.getMonth() + 1),
           trim(date.getDate())
         ].join('-');
}

var TeachingForm = 1,
    Passed       = 0;

process.argv.forEach(function(e) {
  if(e === 'zao')
    TeachingForm = 2;
  else if(e === 'passed')
    Passed = 1;
});

var url  = 'http://localhost:' + httpPort + '/report?',
    date = new Date(),
    destPath = path.resolve('files', getFileName(date, TeachingForm, Passed) + '.pdf');

url += querystring.stringify({ Date: date, TeachingForm: TeachingForm, Passed: Passed });

wkhtmltopdf(url, {
    pageSize:        'a4',
    orientation:     'Landscape',
    javascriptDelay: 200
  },
  function(code, signal) {
    if(code) {
      console.error(signal);
    }

    process.exit();
  }).pipe(fs.createWriteStream(destPath));
