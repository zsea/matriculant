angular.module('UA-UK', ['l10n', 'l10n-tools']).config(['l10nProvider', function(l10n){
  l10n.add('ua-uk', {
    SignIn: {
      title: 'Автентифікація :: ЗДІА',
      login: 'Логін',
      password: 'Пароль',
      sign_in: 'Увійти',
      error: 'Помилка!',
      error_input_password: 'Введіть пароль.',
      error_input_login: 'Введіть логін.'
    },
    Main: {
      title: 'Застосунок',
      title_additional: 'ЗДІА',
      menu_title_person: 'Абітурієнт',
      menu_person_add: 'Додавання особи',
      menu_person_search: 'Пошук особи',
      menu_title_request: 'Заява',
      menu_request_add: 'Додавання заяви',
      menu_request_search: 'Пошук заяви',
      menu_title_options: 'Опції',
      menu_options_administration: 'Адміністрування',
      menu_options_search_generate_reports: 'Генерація звітів',
      menu_options_search_logout: 'Вихід',
      header_zsea: 'ЗДІА',
      header_pk: 'Приймальна комісія'
    },
    AddPerson: {
      title: 'Додати особу',
      header: 'Додавання особи - крок ',
      next_page: 'Далі',
      prev_page: 'Повернутися назад',
      clear: 'Очистити',
      finish: 'Готово!',

      name: 'Ім’я',
      surname: 'Прізвище',
      patronymic: 'По-батькові',

      document_sn: 'Серія та номер документу',
      passport_given: 'Виданий',
      passport_given_department: 'Відділ',
      passport_given_country: 'Країна',
      passport_birthdate: 'Дата народження',
      passport_birthplace: 'Місце нарождення',
    },
    AddRequest: {
      title: 'Додати заяву'
    },
    Admin: {
      title: 'Адміністрування'
    },
    DeletePerson: {
      title: 'Видалення персони',
      confirm: 'Ви впевненні, що хочете видалити персону №%1?',
      cancel: 'Скасувати',
      ok: 'Так'
    },
    ViewPerson: {
      edit: 'Редагувати профайл',
      view: 'Переглянути профайл',
      pen_edit: 'Редагувати',
      passport_data: 'Паспортні дані',
      passport_sn: 'Серія та номер',
      passport_given: 'Виданий',
      passport_given_department: 'Відділ',
      passport_given_country: 'Країна',
      _in: 'в',
      passport_given_region: 'Область',
      passport_birthdate: 'Дата народження',
      _y: 'р.',
      passport_birthplace: 'Місце народження',
      contacts: 'Контактна інформація',
      contacts_phone: 'Телефон',
      contacts_email: 'Електронна пошта',
      contacts_location: 'Адреса',
      _home: 'буд.',
      _flat: 'кв.',
      exams: 'Екзамени',
      exam_date: 'Дата складання',
      exam_members: 'Члени комісії',
      certificates: 'Сертифікати ЗНО',
      attestation: 'Атестати',
      attestation_grade_type: 'Тип',
      attestation_subject_delete: 'Видалити',
      other_info: 'Інша інформація',
      i_code: 'Ідентифікаційний код',
      e_entry: 'Електронний вступ',
      save: 'Зберегти'
    },
    Exit: {
      title: 'Вихід'
    },
    GenerateReports: {
      title: 'Генерація звітів'
    },
    Instruction: {
      title: 'Інструкції'
    },
    SearchPerson: {
      title: 'Пошук осіб',
      titles_missing: 'Відсутній',
      titles_not_recognized: 'Не розпізнаний',
      titles_present: 'Присутній',
      search_id: 'ID',
      search_surname: 'Прізвище',
      search_name: 'Ім’я',
      search_patronymic: 'По батькові',
      search_date_added: 'Додано',
      search: 'Пошук',
      sort: 'Сортування',
      sort_by_id: 'ID',
      sort_by_surname: 'Прізвищем',
      sort_by_name: 'Ім’ям',
      sort_by_patronymic: 'По батькові',
      sort_by_date_added: 'Додано',
      sort_by: 'сортувати за',
      in_order: 'у порядку',
      in_order_asc: 'Зростання',
      in_order_desc: 'Спадання',
      view_info: 'Переглянути інформацію',
      remove: 'Видалити',
      e_entry: 'Електронний вступ',
      letter_e_entry: 'E',
      added: 'Додано',
      num_certificates: 'Кількість сертифікатів',
      num_documents: 'Кількість атестатів',
      letter_num_certificates: 'С',
      letter_num_documents: 'А',
      passport: 'паспорт',
      i_code: 'ідент. код',
      loading: 'Завантаження даних...',
      show_more: 'Показати ще'
    },
    SearchRequest: {
      title: 'Знайти заяву'
    },
    User: {
      title: 'Користувачі',
      newPassword: 'Новий пароль',
      edit: 'Редагувати',
      level: 'рівень доступу',
      newName: 'Ім’я',
      newLogin: 'Логін',
      newPassword: 'Пароль',
      newLevel: 'Рівень доступу',
      add: 'Додати'
    }
  });
}]);
