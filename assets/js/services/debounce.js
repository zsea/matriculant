'use strict';

angular
.module('ZSEA-PK-App.services')

.constant('pkDebounce', function debounce(callback, $scope, delta) {
  if($scope.debounceTimeout_ !== undefined) {
    clearTimeout($scope.debounceTimeout_);
    $scope.debounceTimeout_ = setTimeout(function() {
      callback();

      $scope.debounceTimeout_ = undefined;
      $scope.$digest();
    }, delta);

    return;
  }

  callback();
  $scope.debounceTimeout_ = true;
});
