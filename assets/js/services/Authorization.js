'use strict';

angular
.module('ZSEA-PK-App.services')

.factory('Authorization', function($http, $q) {
  var defer = $q.defer();

  var config = localStorage.getItem('ZSEA:Matriculant:Authorization');
  if(!config) {
    defer.reject({ logged: false });
    return defer.promise;
  }

  config = JSON.parse(config);
  $http.put('/api/authorization', config)
    .then(
      function(res) {
        defer.promise.Level = res.data.Level;
        defer.promise.Name  = res.data.Name;
        defer.promise.id    = res.data.id;

        defer.resolve(res);
      },
      function() {
        localStorage.setItem('ZSEA:Matriculant:Authorization', null);
        defer.reject();
      });

  return defer.promise;
});
