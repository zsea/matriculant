'use strict';

angular
.module('ZSEA-PK-App.services')

.factory('Person', function($resource) {
  return new $resource('/api/person/:id', { 'id': '@id' }, {
      quickFind: {
        method:  'POST',
        url:     '/api/person/quick-find',
        isArray: false
      },

      query: { method: 'GET',  isArray: true, cache: false },
      save:  { method: 'POST', isArray: false },

      setEduMark: {
        method:  'PUT',
        url:     '/api/person/:id/set-edu-mark',
        isArray: false,
        cache:   false
      }
    });
})

.factory('EdboPerson', function($resource) {
  return new $resource('/api/edbo/person/:id', { 'id': '@id' }, {
      query: { method: 'GET',  isArray: true, cache: false },

      fixEduDocument: {
        method:  'PUT',
        isArray: false,
        cache:   false,
        url:     '/api/edbo/person/:id/edu-document'
      },

      fixCertificate: {
        method:  'PUT',
        isArray: false,
        cache:   false,
        url:     '/api/edbo/person/:id/certificate'
      },

      changeEduDocumentMark: {
        method:  'PUT',
        isArray: false,
        cache:   false,
        url:     '/api/edbo/person/:id/edu-document-mark'
      },

      ignore: {
        method:  'DELETE',
        isArray: false,
        cache:   false,
        url:     '/api/edbo/person/:id'
      }
    });
})

.factory('EdboRequest', function($resource) {
  return new $resource('/api/edbo/request/:id', { 'id': '@id' }, {
      query: { method: 'GET',  isArray: true, cache: false },

      addExam: {
        method: 'POST',
        url: '/api/edbo/request/:id/exam',
        isArray: false,
        cache: false
      },

      editSubStatus: {
        method: 'PUT',
        url: '/api/edbo/request/:id/sub-status',
        isArray: false,
        cache: false
      },

      editOriginal: {
        method: 'PUT',
        url: '/api/edbo/request/:id/original',
        isArray: false,
        cache: false
      },

      ignore: {
        method:  'DELETE',
        isArray: false,
        cache:   false,
        url:     '/api/edbo/request/:id'
      }
    });
})

.factory('Request', function($resource) {
  return new $resource('/api/request/:id', { 'id': '@id' }, {
      query: { method: 'GET',  isArray: true, cache: false },
      save:  { method: 'POST', isArray: false },

      generate: {
        method:  'GET',
        url:     '/api/request/generate',
        isArray: true,
        cache:   false
      },

      journal: {
        method:  'GET',
        url:     '/api/request/journal',
        isArray: true,
        cache:   false
      },

      examinations: {
        method:  'GET',
        url:     '/api/request/examination',
        isArray: true,
        cache:   false
      },

      count: {
        method:  'GET',
        url:     '/api/request/count',
        cache:   false
      },

      setOriginal: {
        method:  'PUT',
        url:     '/api/request/:id/set-original',
        isArray: false,
        cache:   false
      },

      updateCourses: {
        method:  'PUT',
        url:     '/api/request/:id/update-courses',
        isArray: false,
        cache:   false
      },

      setStatus: {
        method:  'PUT',
        url:     '/api/request/:id/set-status',
        isArray: false,
        cache:   false
      },

      setSubStatus: {
        method:  'PUT',
        url:     '/api/request/:id/set-substatus',
        isArray: false,
        cache:   false
      },

      setExamMark: {
        method:  'PUT',
        url:     '/api/request/:id/set-exam-mark',
        isArray: false,
        cache:   false
      }
    });
});
