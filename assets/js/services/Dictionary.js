'use strict';

angular
.module('ZSEA-PK-App.services')
.service('Dictionary', function($resource) {
  var opts = {
    fetch: { method: 'GET', cache: true, isArray: true }
  };

  this.DocumentType =
      $resource('/api/dictionary/document-type', { }, opts).fetch();

  this.EduDocumentType =
      $resource('/api/dictionary/edu-document-type', { }, opts).fetch();

  this.EduDocumentMarkType =
      $resource('/api/dictionary/edu-document-mark-type', { }, opts).fetch();

  this.EduDocumentAward =
      $resource('/api/dictionary/edu-document-award', { }, opts).fetch();

  this.BenefitType =
      $resource('/api/dictionary/benefit-type', { }, opts).fetch();

  this.CertificateSubject =
      $resource('/api/dictionary/certificate-subject', { }, opts).fetch();

  this.Sex = $resource('/api/dictionary/sex', { }, opts).fetch();

  this.StreetType = $resource('/api/dictionary/street-type', { }, opts).fetch();

  this.TeachingForm =
      $resource('/api/dictionary/teaching-form', { }, opts).fetch();

  this.Priority =
      $resource('/api/dictionary/priority', { }, opts).fetch();

  this.Stream = $resource('/api/dictionary/stream', { }, opts).fetch();

  this.ExaminationCause =
      $resource('/api/dictionary/examination-cause', { }, opts).fetch();

  this.RequestType =
      $resource('/api/dictionary/request-type', { }, opts).fetch();

  this.RequestStatus =
      $resource('/api/dictionary/request-status', { }, opts).fetch();

  this.Speciality = $resource('/api/dictionary/speciality', { }, opts).fetch();

  this.RawSpecialitySubject =
      $resource('/api/dictionary/speciality-subject', { }, opts).fetch();

  this.SubRequestType =
      $resource('/api/dictionary/subrequest-type', { }, opts).fetch();

  this.SpecialitySubject = function(speciality) {
    return this.RawSpecialitySubject.filter(function(item) {
      return item.Speciality == speciality;
    });
  };

  this.getEduDocumentTypeName = function(id) {
    var result = '';

    this.EduDocumentType.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  };

  this.getMarkTypeName = function(id) {
    var result = '';

    this.EduDocumentMarkType.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  };

  this.getEduDocumentAwardName = function(id) {
    var result = '';

    this.EduDocumentAward.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  };

  this.getCertificateSubjectName = function(id) {
    var result = '';

    this.CertificateSubject.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  };

  this.getSexName = function(id) {
    var result = '';

    this.Sex.some(function(e) {
      if(e.id === +id) {
        result = e.SexName;
        return true;
      }
    });

    return result;
  };
  this.getSexShortName = function(id) {
    var result = '';

    this.Sex.some(function(e) {
      if(e.id === +id) {
        result = e.SexShort;
        return true;
      }
    });

    return result;
  };

  this.getStreetTypeShort = function(id) {
    var result = '';

    this.StreetType.some(function(e) {
      if(e.id === +id) {
        result = e.Short;
        return true;
      }
    });

    return result;
  };


  this.getDocumentTypeName = function(id) {
    var result = '';

    this.DocumentType.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  };

  this.getBenefitTypeName = function(id) {
    var result = '';

    this.BenefitType.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  };

  this.getStreamName = function(id) {
    var result = '';

    this.Stream.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  };

  this.checkSpeciality = function(speciality, stream) {
    if(!stream) {
      return;
    }

    if(stream == 3 && [1, 4].indexOf(speciality.Stream) !== -1) {
      return true;
    }

    if(stream == speciality.Stream) {
      return true;
    }
  };

  this.checkRequestType = function(type, stream) {
    if(!stream) {
      return;
    }

    if(stream == 1) {
      return true;
    }

    if(type.id == 2) {
      return true;
    }
  };

  this.streamShort = function(speciality) {
    if(speciality.Stream === 1) {
      return 'аб.';
    }

    if(speciality.Stream === 2) {
      return 'мол.спец.';
    }

    if(speciality.Stream === 4) {
      return 'м.с.';
    }
  };

  this.getSpeciality = function(id) {
    var result = { };

    this.Speciality.some(function(e) {
      if(e.id === +id) {
        result = e;
        return true;
      }
    });

    return result;
  };

  this.getTeachingFormName = function(id) {
    var result = { };

    this.TeachingForm.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  };

  this.getSpecialityHash = function(id) {
    var result = { };

    this.Speciality.some(function(e) {
      if(e.id === +id) {
        result = e.Hash;
        return true;
      }
    });

    return result;
  }

  this.getSpecialityName = function(id) {
    var result = { };

    this.Speciality.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  }

  this.getRequestStatusName = function(id) {
    var result = { };

    this.RequestStatus.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  }

  this.getSubRequestTypeName = function(id) {
    var result = { };

    this.SubRequestType.some(function(e) {
      if(e.id === +id) {
        result = e.Name;
        return true;
      }
    });

    return result;
  }

  this.getExamTypeName = function(exam) {
    if(exam.Primary) {
      return 'Фаховий іспит';
    }
    if(exam.Foreign) {
      return 'Іноземна мова';
    }

    return this.getCertificateSubjectName(exam.Subject);
  }
});
