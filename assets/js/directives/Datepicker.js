'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkDatepicker', function() {
  return {
    restrict: 'A',
    require: '?ngModel',

    link: function(scope, element, attrs, ngModel) {
      element.datepicker({
        format: 'dd.mm.yyyy',
        startView: attrs.mode || 'day', //decade, year, day
        startDate: '01.01.1950',
        endDate: '31.12.' + (new Date()).getFullYear(),
        language: 'ua',
        keyboardNavigation: true,
        autoclose: true,
        forceParse: false,
        todayBtn: 'linked'
      });

      ngModel.$parsers.unshift(function() {
        var result = $.fn.datepicker.DPGlobal.parseDate(ngModel.$viewValue, 'dd.mm.yyyy', 'ua');
        var test = $.fn.datepicker.DPGlobal.formatDate(result, 'dd.mm.yyyy', 'ua');

        if(result && ngModel.$viewValue == test) {
          ngModel.$setValidity('date', true);
          return result;
        }

        ngModel.$setValidity('date', false);
      });

      ngModel.$formatters.unshift(function() {
        if(!ngModel.$modelValue)
          return '';

        var date = new Date(ngModel.$modelValue);
        var d = date.getDate() + '',
            m = date.getMonth() + 1 + '',
            y = date.getFullYear() + '';

        if(d.length == 1)
          d = '0' + d;
        if(m.length == 1)
          m = '0' + m;

        return d + '.' + m + '.' + y;
      });
    }
  };
});
