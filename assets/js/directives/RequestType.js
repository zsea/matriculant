'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkRequestType', function() {
  function validate($scope, ngModel, value) {
    if($scope.data.Stream != 3) {
      ngModel.$setValidity('required', value);
    }
    else {
      ngModel.$setValidity('required', true);
    }
  }

  return {
    restrict: 'A',
    require: '?ngModel',

    link: function($scope, element, attrs, ngModel) {
      $scope.$watch('data.Stream', function() {
        validate($scope, ngModel, ngModel.$modelValue);
      });

      ngModel.$parsers.push(function(value) {
        validate($scope, ngModel, value);
        return value;
      });

      ngModel.$formatters.unshift(function(value) {
        validate($scope, ngModel, value);
        return value;
      });
    }
  };
});
