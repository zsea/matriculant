'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkHideable', function() {
  return {
    restrict: 'A',

    link: function($scope, elem, attrs) {
      var header, content;

      header  = elem.find('header');
      content = elem.find('content');

      if(attrs.toggled === undefined) {
        content.addClass('collapsed');
      }

      header.on('click', function() {
        content.toggleClass('collapsed');
      });
    }
  };
});
