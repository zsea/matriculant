'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkFile', function() {
  return {
    restrict: 'A',
    scope: {
      'pkFile': '&'
    },

    link: function($scope, element){
      element.on('mousedown', function() {
        element.value = '';
      });

      element.on('change', function(e) {
        var reader = new FileReader();
        reader.onload = function(loadEvent) {
          $scope.$apply(function() {
            $scope.pkFile({ $data: loadEvent.target.result });
          })
        };

        reader.readAsText(e.target.files[0]);
      });
    }
  };
});
