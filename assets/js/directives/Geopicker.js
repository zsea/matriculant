'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkGeopickerWrapper', function() {
  return {
    restrict: 'C',
    templateUrl: '/view/directive/Geopicker.html'
  };
})

.directive('pkGeopicker', function($compile, $timeout, $http) {
  return {
    restrict: 'A',
    require: '^ngModel',
    priority: 1,
    scope: {},

    link: function($scope, element, attrs, ngModel) {
      var wrapper;
      var focusHandler = function() {
        wrapper = angular.element('<div class="pk-geopicker-wrapper"></div>');

        $compile(wrapper)($scope);
        $('body').append(wrapper);

        wrapper.css({
          'left': element.offset().left,
          'top': element.offset().top + element.outerHeight(),
          'width': element.outerWidth()
        });
      };
      var blurHandler = function() {
        setTimeout(function() {
          wrapper.remove();
        }, 200);
      };

      element.on('focus', focusHandler);
      element.on('blur',  blurHandler);
      element.attr('placeholder', 'назва населеного пункту');

      ngModel.$render = function(value) {
        if(!value) {
          element.val('');
          return;
        }

        $http({
          method: 'GET',
          url: '/api/geodata/convert',
          params: { koatuu: value },
          cache: true
        })
        .then(function(data) {
          element.val(data.data);
        });
      };

      ngModel.$parsers.unshift(function() {
        if(/^\d{10}$/.test(ngModel.$viewValue)) {
          ngModel.$render(ngModel.$viewValue);
          ngModel.$setValidity('geopicker', true);
          return ngModel.$viewValue;
        }

        ngModel.$setValidity('geopicker', false);

        if(!ngModel.$viewValue || ngModel.$viewValue.length < 4) {
          wrapper.removeClass('visible');
          return;
        }

        $scope.search();
      });
      ngModel.$formatters.push(function(value) {
        if(/^\d{10}$/.test(value)) {
          ngModel.$render(value);
          ngModel.$setValidity('geopicker', true);
          return value;
        }

        ngModel.$setValidity('geopicker', false);
      });

      $scope.search = function() {
        if($scope.findTimeout) {
          $timeout.cancel($scope.findTimeout);
        }

        $scope.findTimeout = $timeout(function() {
          $scope.findTimeout = undefined;

          $http({
            method: 'POST',
            url: '/api/geodata/search',
            data: { query: ngModel.$viewValue }
          })
          .then(function(data) {
            $scope.found = data.data;
            wrapper.addClass('visible');
          });
        }, 300);
      };

      $scope.select = function(item) {
        var l1 = item.level1 || '',
            l2 = item.level2 || '',
            l3 = item.level3 || '',
            l4 = item.level4 || '';
        l1 = l1 + '';
        l2 = l2 + '';
        l3 = l3 + '';
        l4 = l4 + '';

        while(l1.length < 2) {
          l1 = '0' + l1;
        }
        while(l2.length < 3) {
          l2 = '0' + l2;
        }
        while(l3.length < 3) {
          l3 = '0' + l3;
        }
        while(l4.length < 2) {
          l4 = '0' + l4;
        }

        $timeout(function() {
          ngModel.$setViewValue(l1 + l2 + l3 + l4);
        });
      };
    }
  };
});
