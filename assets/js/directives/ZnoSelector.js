'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkZnoSelector', function(Dictionary) {
  function mapperFn(item) {
    return item.Subject;
  }

  function updateSubjects($scope) {
    var subjects = Dictionary.SpecialitySubject(+$scope.info.Speciality);
    if(subjects.length === 0 || $scope.marks.length === 0) {
      $scope.primary = $scope.secondary = $scope.optional = [];
      return;
    }

    if($scope.primary.length !== 0) {
      return;
    }

    $scope.primary = subjects.filter(function(item) {
      return item.Primary === true;
    }).map(mapperFn);
    $scope.secondary = subjects.filter(function(item) {
      return item.Secondary === true;
    }).map(mapperFn);
    $scope.optional = subjects.filter(function(item) {
      return item.Primary === false && item.Secondary === false;
    }).map(mapperFn);
  }

  return {
    restrict: 'A',

    scope: {
      info:   '=',
      person: '='
    },

    templateUrl: '/view/directive/ZnoSelector.html',

    link: function($scope) {
      $scope.marks = [];

      $scope.$watch('info.Speciality', function() {
        $scope.primary = $scope.secondary = $scope.optional = [];
        updateSubjects($scope);
      });
      $scope.$watch('person.Certificates', function() {
        var certificates = $scope.person.Certificates || [];
        certificates = JSON.parse(JSON.stringify(certificates));

        certificates = certificates.filter(function(item) {
          return item.Year == (new Date()).getFullYear();
        });

        $scope.marks = [];
        certificates.forEach(function(item) {
          item.marks.forEach(function(mark) {
            mark.Certificate = item;
          });

          $scope.marks = $scope.marks.concat(item.marks);
          item.marks = undefined;
        });

        $scope.invalidateMarks();
      }, true);

      $scope.getMark = function(subject) {
        var marks = $scope.marks.filter(function(item) {
          return item.Subject == subject;
        });

        marks = marks.sort(function(lhs, rhs) {
          return rhs.Mark - lhs.Mark;
        });
        return marks[0];
      };

      $scope.invalidateMarks = function() {
        updateSubjects($scope);
      };

      $scope.getMarkText = function(mark) {
        if(!mark) {
          return '[предмет не знайдено]';
        }

        return '[' + mark.Mark +', сертифікат '+ mark.Certificate.Number + ']';
      };
    }
  };
});
