'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkEdboRequestItem', function(Dictionary, EdboRequest, $timeout) {
  return {
    restrict: 'A',
    scope:    true,

    templateUrl: '/view/directive/EdboRequestItem.html',

    link: function($scope) {
      $scope.dict = Dictionary;
      $scope.wave = 0;

      $scope.handled = $scope.item.key === 'Exams' ||
                       $scope.item.key === 'Status' ||
                       $scope.item.key === 'Original';

      $scope.add = function() {
        if($scope.item.key === 'Exams') {
          return $scope.addExam();
        }
      };

      $scope.edit = function() {
        if($scope.item.key === 'Status' && $scope.request.Stream == 4) {
          return $scope.editSubStatus();
        }

        if($scope.item.key === 'Original') {
          return $scope.editOriginal();
        }
      };

      $scope.addExam = function() {
        $scope.$emit('queue:inc');

        EdboRequest.addExam({
          id:         $scope.request.id,
          subrequest: $scope.subrequest ? $scope.subrequest.id : undefined
        }, $scope.item.current,
        function() {
          $scope.$emit('queue:dec');
          $scope.item.type = undefined;
        },
        function() {
          $scope.$emit('queue:dec');
          alert('Виникла помилка при додаванні екзамена! Дивіться лог серверу!');
        });
      };

      $scope.editSubStatus = function() {
        $scope.$emit('queue:inc');

        var data = {
          Status: $scope.item.current
        };

        EdboRequest.editSubStatus({
          id:         $scope.request.id,
          subrequest: $scope.subrequest.id
        }, data,
        function() {
          $scope.$emit('queue:dec');
          $scope.item.type = undefined;
        },
        function() {
          $scope.$emit('queue:dec');
          alert('Виникла помилка при редагуванні статусу! Дивіться лог серверу!');
        });
      };

      $scope.editOriginal = function() {
        $scope.$emit('queue:inc');

        var data = {
          Original: $scope.item.current
        };

        EdboRequest.editOriginal({
          id:         $scope.request.id,
          subrequest: $scope.subrequest ? $scope.subrequest.id : undefined
        }, data,
        function() {
          $scope.$emit('queue:dec');
          $scope.item.type = undefined;
        },
        function() {
          $scope.$emit('queue:dec');
          alert('Виникла помилка при редагуванні оригіналів! Дивіться лог серверу!');
        });
      };
    }
  };
});
