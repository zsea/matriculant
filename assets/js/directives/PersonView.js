'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkPersonView', function($http, $modal, $timeout,
                                    Dictionary, Person, Authorization) {
  return {
    restrict: 'E',
    scope: {
      person: '='
    },

    templateUrl: '/view/directive/PersonView.html',

    link: function($scope) {
      $scope.dictionary = Dictionary;
      $scope.auth = Authorization;

      var getBirthPlace = function() {
        $http({
          method: 'GET',
          url: '/api/geodata/convert',
          params: { koatuu: $scope.person.BirthPlace },
          cache: true
        })
        .then(function(data) {
          $timeout(function() {
            $scope.BirthPlace = data.data;
          });
        });
      };

      var getLivingPlace = function() {
        $http({
          method: 'GET',
          url: '/api/geodata/convert',
          params: { koatuu: $scope.person.LivingPlace },
          cache: true
        })
        .then(function(data) {
          $timeout(function() {
            $scope.LivingPlace = data.data;
          });
        });
      };

      getBirthPlace();
      getLivingPlace();

      $scope.certificateDelMark = function() {
        $http({
          method: 'DELETE',
          url:    [
            '/api/person/certificate/',
            $scope.removableCertificate.id,
            '/mark/',
            $scope.removableCertificate.marks[$scope.removableIdxMark].id
          ].join('')
        })
        .success(function(data) {
          delete $scope.removableCertificate.marks[$scope.removableIdxMark];
        })
        .error(function(err) {
          console.log(err.data);
        });
      };

      $scope.showRemoveCertMarkModal = function(certificate, idxMark) {
        $scope.removableCertificate = certificate;
        $scope.removableIdxMark     = idxMark;
        $modal({
          scope: $scope,
          template: '/view/modal/RemoveCertificateMarkModal.html',
          show: true
        });
      }

      $scope.certificateAddMark = function(certificate) {
        if(!certificate.newSubject) {
          return alert('Не вибрано предмет!');
        }
        if(!/[\d\.]+/.test(certificate.newMark)) {
          return alert('Невірний формат оцінки!');
        }

        $http({
          method: 'PUT',
          url: '/api/person/add-certificate-mark',
          params: { certificate: certificate.id },
          data: {
            Subject: +certificate.newSubject,
            Mark:    +certificate.newMark
          },
        })
        .success(function(data) {
          certificate.marks.push(data.Mark);
          certificate.newSubject = undefined;
          certificate.newMark    = undefined;
        })
        .error(function(err) {
          console.log(err.data);
        });
      };

      $scope.updateEduMark = function(eduDocument) {
        Person.setEduMark({
          id:          $scope.person.id,
          EduDocument: eduDocument.id_,
          Mark:        eduDocument.Mark
        },
        function() { },
        function(err) {
          alert('Невдалося встановити оцінку!');
        });
      };
    }
  };
});
