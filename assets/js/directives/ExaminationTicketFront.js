'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkExaminationTicketFront', function(Dictionary) {
  return {
    restrict: 'A',
    scope: {
      request: '=',
      exams:   '='
    },

    templateUrl: '/view/directive/ExaminationTicketFront.html',

    link: function($scope) {
      $scope.dict = Dictionary;
    }
  };
});
