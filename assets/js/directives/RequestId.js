'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkRequestId', function(Dictionary) {
  return {
    restrict: 'A',
    template: '{{ hash }}{{ appendix }} {{ number }}',

    scope: {
      pkRequestId: '='
    },

    link: function($scope) {
      if($scope.pkRequestId.Speciality.Hash) {
        $scope.hash = $scope.pkRequestId.Speciality.Hash;
      }
      else {
        $scope.hash = Dictionary.getSpecialityHash($scope.pkRequestId.Speciality);
      }

      $scope.number = $scope.pkRequestId.Number;

      if($scope.pkRequestId.TeachingForm === 2) {
        $scope.appendix = 'з';
      }
    }
  };
});
