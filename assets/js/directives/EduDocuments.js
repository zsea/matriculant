'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkEdudocuments', function(Dictionary) {
  return {
    restrict: 'E',
    scope: {
      documents: '='
    },

    templateUrl: '/view/directive/EduDocuments.html',
    replace: true,

    link: function($scope) {
      $scope.dictionary = Dictionary;
      $scope.data = { Award: 0 };

      $scope.addDocument = function() {
        if($scope.data.MarkType === '5' && $scope.data.Mark > 5) {
          return alert('Невірна оцінка!');
        }

        if($scope.data.MarkType === '12' && $scope.data.Mark > 12) {
          return alert('Невірна оцінка!');
        }

        if($scope.data.MarkType === '60') {
          if($scope.data.mark > 60) {
            return alert('Невірна оцінка!');
          }
        }

        $scope.documents.push($scope.data);
        $scope.data = { Award: 0 };
      };

      $scope.removeDocument = function(idx) {
        $scope.documents.splice(idx, 1);
      };
    }
  };
});
