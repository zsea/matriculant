'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkEdudocumentId', function() {
  return {
    restrict: 'A',
    require: '?ngModel',

    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.unshift(function() {
        // general:
        var value = /^(([а-яіїєґa-z]{2}) *(\d{8}))|(([а-яіїєґa-z]15) +(\d{6}))$/i.exec(ngModel.$viewValue);

        // renewal:
        // var value = /^(([а-яіїєґa-z\-]+) *(\d+))$/i.exec(ngModel.$viewValue);

        if(!value) {
          ngModel.$setValidity('edudocument', false);
          return undefined;
        }

        ngModel.$setValidity('edudocument', true);

        if(!value[5] && !value[6]) {
          return { Series: value[2].trim().toLocaleUpperCase(), Number: value[3] };
        }
        else {
          return { Series: value[5].trim().toLocaleUpperCase(), Number: value[6] };
        }
      });

      ngModel.$formatters.unshift(function() {
        if(!ngModel.$modelValue || !ngModel.$modelValue.Series ||
           !ngModel.$modelValue.Number)
          return '';

        return ngModel.$modelValue.Series.toLocaleUpperCase() + ' ' +
               ngModel.$modelValue.Number;
      });
    }
  };
});
