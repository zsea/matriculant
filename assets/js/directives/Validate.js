'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkValidate', function($compile) {
  var id = 0;

  return {
    restrict: 'A',
    priority: 100,

    compile: function() {
      return {
        pre: function($scope, rootForm, attrs) {
          attrs.name = '_auto_validated_form_' + id++;
          rootForm.attr('name', attrs.name);

          var submitButton = rootForm.find('input[ng-disabled]');
          if(submitButton.length === 0) {
            submitButton = rootForm.find('button[ng-disabled]');
          }

          if(submitButton.length === 0) {
            return;
          }

          if(submitButton.length > 0) {
            submitButton.attr('ng-disabled', attrs.name + '.$invalid');

            var newButton = $compile(submitButton[0].outerHTML)($scope),
                parent = submitButton.parent();

            submitButton.remove();
            parent.append(newButton);
          }
        },
        post: function($scope, rootForm) {
          var name = rootForm.attr('name'),
              groups = rootForm.find('.form-group');

          groups.each(function(i, group) {
            group = $(group);
            var input = $('input[name]', group).attr('name') || $('select[name]', group).attr('name');

            if(input === undefined) {
              return;
            }

            $scope.$watch(function() {
              if(!$scope[name]) {
                return [false, true];
              }

              return [
                $scope[name][input].$valid,
                $scope[name][input].$invalid
              ];
            }, function(value) {
              if(!value[0]) {
                group.removeClass('has-success');
              }
              else {
                group.addClass('has-success');
              }

              if(!value[1]) {
                group.removeClass('has-error');
              }
              else {
                group.addClass('has-error');
              }
            }, true);
          });
        }
      };
    }
  };
});
