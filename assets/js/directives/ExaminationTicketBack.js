'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkExaminationTicketBack', function(Dictionary) {
  return {
    restrict: 'A',
    scope: {
      request: '=',
      exams:   '='
    },

    templateUrl: '/view/directive/ExaminationTicketBack.html',

    link: function($scope) {
      $scope.dict = Dictionary;
    }
  };
});
