'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('uiFloatingMenu', function() {
  return{
    restrict: 'A',
    link: function(scope, element){
      $(window).scroll(function(){
        var top = $(document).scrollTop();
        if(top < 50)
          element.css({top: '60px', position: 'absolute'});
        else
          element.css({top: '10px', position: 'fixed'});
      });
    }

  };
});
