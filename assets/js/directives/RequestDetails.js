'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkRequestDetails', function($http, $timeout, $modal, $location,
                                        Dictionary, Request, Authorization) {
  return {
    restrict: 'E',
    scope: {
      request:  '=',
      detailed: '='
    },

    templateUrl: '/view/directive/RequestDetails.html',

    link: function($scope, element, attrs) {

      $scope.dict = Dictionary;
      $scope.auth = Authorization;
      if(!$scope.request.Exams && !attrs.lazy) {
        $scope.request.$get();
      }

      $scope.toggleOriginal = function() {
        Request.setOriginal({
          id:    $scope.request.id,
          value: !$scope.request.Original
        },
        function() {
          $scope.request.Original = !$scope.request.Original;
        });
      };

      $scope.updateStatus = function(id, status) {
        Request.setStatus({
          id:    id,
          value: status
        });
      };

      $scope.updateSubStatus = function(id, status) {
        Request.setSubStatus({
          id:    id,
          value: status
        });
      };

      $scope.updateMark = function(exam) {
        Request.setExamMark({
          id:   exam.Request,
          Exam: exam.id,
          Mark: exam.Mark
        },
        function() { },
        function(err) {
          alert('Невдалося встановити оцінку!');
        });
      };

      $scope.removeRequest = function() {
        $scope.removableRequest.$delete(function() {
          $location.url('/request');
        });
      };

      $scope.showRemoveRequestModal = function(request) {
        $scope.removableRequest = request;
        $modal({ scope: $scope, template: '/view/modal/RemoveRequestModal.html', show: true });
      }

    }
  };
});
