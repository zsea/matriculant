'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkExamSelector', function(Dictionary) {
  function mapperFn(item) {
    return item.Subject;
  }

  return {
    restrict: 'A',

    scope: {
      info: '='
    },

    templateUrl: '/view/directive/ExamSelector.html',

    link: function($scope) {
      $scope.$watch('info.Stream', function() {
        if(!$scope.info.Stream) {
          return;
        }

        if($scope.info.Stream == 1 && $scope.info.Exams.length !== 3) {
          $scope.info.Exams = [
            {
              Primary: false,
              Foreign: false
            },
            {
              Primary: false,
              Foreign: false
            },
            {
              Primary: false,
              Foreign: false
            }
          ];
        }
        if($scope.info.Stream == 2 && $scope.info.Exams.length !== 1) {
          $scope.info.Exams = [
            {
              Primary: true,
              Foreign: false
            }
          ];
        }
        else
        if($scope.info.Stream == 4 && $scope.info.Exams.length !== 2) {
          $scope.info.Exams = [
            {
              Primary: true,
              Foreign: false
            },
            {
              Primary: false,
              Foreign: true
            }
          ];
        }
        else if($scope.info.Stream == 3) {
          $scope.info.Exams = [];
        }
      });

      $scope.$watch('info.Speciality', function() {
        if($scope.info.Stream != 1) {
          return;
        }

        var subjects = Dictionary.SpecialitySubject(+$scope.info.Speciality);
        if(subjects.length === 0) {
          $scope.primary = $scope.secondary = $scope.optional = [];
          return;
        }

        $scope.primary = subjects
          .filter(function(item) {
            return item.Primary === true;
          })
          .map(mapperFn);

        $scope.secondary = subjects
          .filter(function(item) {
            return item.Secondary === true;
          })
          .map(mapperFn);

        $scope.optional = subjects
          .filter(function(item) {
            return item.Primary === false && item.Secondary === false;
          })
          .map(mapperFn);
      });
    }
  };
});
