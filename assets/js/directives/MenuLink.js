'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('menuLink', function($location) {
  function check(regex, element) {
    if(regex.test($location.path())) {
      element.addClass('active');
    }
    else {
      element.removeClass('active');
    }
  }
  return {
    restrict: 'A',

    link: function($scope, element, attrs) {
      var regex = new RegExp(attrs.menuLink, '');

      $scope.$on('$routeChangeSuccess', check.bind(null, regex, element));
      check(regex, element);
    }
  };
});
