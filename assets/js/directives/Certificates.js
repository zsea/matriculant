'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkCertificates', function(Dictionary) {
  return {
    restrict: 'E',
    scope: {
      certificates: '='
    },

    templateUrl: '/view/directive/Certificates.html',
    replace: true,

    link: function($scope) {
      $scope.dictionary = Dictionary;
      $scope.newSubject = { };
      $scope.newCertificate = { marks: [] };

      $scope.addCertificate = function() {
        $scope.certificates.push($scope.newCertificate);
        $scope.newCertificate = { marks: [] };
      };

      $scope.removeCertificate = function(idx) {
        $scope.certificates.splice(idx, 1);
      };

      $scope.addSubject = function(certificate) {
        certificate.marks.push($scope.newSubject);
        $scope.newSubject = { };
      };

      $scope.removeSubject = function(certificate, idx) {
        certificate.marks.splice(idx, 1);
      };
    }
  };
});
