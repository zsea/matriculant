'use strict';

angular
.module('ZSEA-PK-App.directives')
.directive('pkPassportData', function() {
  return {
    restrict: 'A',
    require: '?ngModel',

    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.unshift(function() {
        var value = /^([а-яіїєґa-z]{2}) *(\d{6,7})$/i.exec(ngModel.$viewValue);

        if(!value) {
          ngModel.$setValidity('passport', false);
          return undefined;
        }

        ngModel.$setValidity('passport', true);
        return { Series: value[1], Number: value[2] };
      });

      ngModel.$formatters.unshift(function() {
        if(!ngModel.$modelValue || !ngModel.$modelValue.Series ||
           !ngModel.$modelValue.Number)
          return '';

        return ngModel.$modelValue.Series.toLocaleUpperCase() + ' ' +
               ngModel.$modelValue.Number;
      });
    }
  };
});
