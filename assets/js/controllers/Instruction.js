'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('InstructionController', function($scope, l10n) {
  $scope.$parent.title = l10n.get('Instruction.title');
});
