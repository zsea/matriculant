'use strict';

angular
.module('RatesBuilder')
.controller('RatesStream2Controller',
    function($scope, Request, $http, $location, Dictionary) {
  $scope.dict   = Dictionary;
  $scope.params = {};
  $scope.params.Stream = 2;

  $scope.recommend = function(request) {
    Request.setStatus({
      id:    request.id,
      value: 5
    },
    function() {
      request.Status = 5;
    });
  };
  $scope.reject = function(request) {
    Request.setStatus({
      id:    request.id,
      value: 6
    },
    function() {
      request.Status = 6;
    });
  };
  $scope.accept = function(request) {
    Request.setStatus({
      id:    request.id,
      value: 4
    },
    function() {
      request.Status = 4;
    });
  };
  $scope.order = function(request) {
    Request.setStatus({
      id:    request.id,
      value: 7
    },
    function() {
      request.Status = 7;
    });
  };

  var regex = /[?&]([^=#]+)=([^&#]*)/g,
      url = window.location.href,
      match;

  while(match = regex.exec(url)) {
    $scope.params[match[1]] = +match[2];
  }

  Request.generate($scope.params, function(data) {
    data.forEach(function(request) {
      var primaryExam = request.Exams.filter(function(ex) {
        return ex.Primary === true;
      })[0];
      var foreignExam = request.Exams.filter(function(ex) {
        return ex.Foreign === true;
      })[0];

      request.EduDocument.Mark = +request.EduDocument.Mark.toFixed(2);

      request.PrimaryMark = primaryExam ? primaryExam.Mark : 0;
      request.TotalMark = request.PrimaryMark  + request.EduDocument.Mark;
    });

    $scope.requests = data;

    $http({
      method: 'GET',
      url: '/api/info/speciality',
      params: $scope.params
    })
    .then(function(res) {
      $scope.Speciality = res.data;
      $scope.Budget     = res.data.Capacity.general;

      $scope.rates = [];
      $scope.prepareRates();

      $scope.title = 'ready';
    });
  });

  $scope.getCurrentDate = function() {
    return new Date();
  };

  function isBudget(request) {
    return request.Budget && request.PrimaryMark &&
           request.Status !== 2 && request.Status !== 6;
  }
  function haveBenefit(request) {
    return request.Person.Benefits.some(function(benefit) {
      return benefit.Type.Strength === 1;
    });
  }
  function havePriorityBenefit(request) {
    if(request.EduDocument.Award !== 0) {
      return true;
    }

    return request.Person.Benefits.some(function(benefit) {
      return benefit.id !== 39 && benefit.Type.Strength === 2;
    });
  }

  function sorter(lhs, rhs) {
    if(rhs.TotalMark !== lhs.TotalMark) {
      return rhs.TotalMark - lhs.TotalMark;
    }

    var rhsPriority = havePriorityBenefit(rhs),
        lhsPriority = havePriorityBenefit(lhs);

    if(rhsPriority && !lhsPriority) {
      return 1;
    }
    if(lhsPriority && !rhsPriority) {
      return -1;
    }

    return rhs.PrimaryMark - lhs.PrimaryMark;
  }

  function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  $scope.prepareRates = function() {
    var benefitRequests = [], nonBenefitRequests = [], nonBudgetRequests = [],
        benefitLoosers  = [],
        benefitCount, budgetCount;

    budgetCount  = $scope.Budget || 0;
    benefitCount = Math.ceil(budgetCount / 4);

    var requests = deepClone($scope.requests);

    // some magic with 'Status' :)
    requests.forEach(function(request) {
      request.SubRequests = request.SubRequests.filter(function(item) {
        return item.Type === 2;
      });

      var subrequest = request.SubRequests[0];
      if(subrequest) {
        request.Status = subrequest.Status;
      }
    });

    nonBudgetRequests = requests.filter(function(request) {
      return !isBudget(request);
    });
    nonBenefitRequests = requests.filter(function(request) {
      return isBudget(request);
    });

    benefitRequests = nonBenefitRequests.filter(function(request) {
      return haveBenefit(request);
    });
    nonBenefitRequests = nonBenefitRequests.filter(function(request) {
      return !haveBenefit(request);
    });

    benefitRequests.sort(sorter);
    benefitLoosers = benefitRequests.splice(
      benefitCount, benefitRequests.length - benefitCount);

    nonBenefitRequests = nonBenefitRequests.concat(benefitLoosers);

    nonBenefitRequests.sort(sorter);
    nonBudgetRequests.sort(sorter);

    $scope.rates = benefitRequests.concat(nonBenefitRequests);
    if($scope.params.Detailed) {
      $scope.rates = $scope.rates.concat(nonBudgetRequests);
    }

    // highlight recomendations
    for(var i = 0; i < budgetCount; ++i) {
      $scope.rates[i].recommend = true;
    }
  };
});
