'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('DataOriginalsController', function($scope, l10n, Request, Dictionary) {
  $scope.$parent.title = 'Оригінали';
  $scope.dict = Dictionary;

  $scope.requests = Request.query({
    Originals: true,
    offset:    0,
    limit:     9999
  },
  function(requests) {
    for(var i = 0; i < requests.length - 1; ++i) {
      if(requests[i].Person.id === requests[i + 1].Person.id) {
        requests[i].error = true;
        requests[i + 1].error = true;
      }
    }
  });
});
