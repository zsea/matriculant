'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('EdboRequestController', function($scope, EdboRequest, Dictionary) {
  $scope.$parent.title = 'Заяви ЄДЕБО';
  $scope.queue = 0;
  $scope.dict  = Dictionary;

  $scope.update = function() {
    EdboRequest.query($scope.params, function(requests) {
      $scope.requests = requests;
    });
  };

  $scope.update();

  var checkHandler = function(request, next) {
    request.$get(function() {
      next();
      --$scope.queue;
    },
    function(err) {
      console.error(err.data);

      next(true);
      --$scope.queue;
    });
  };

  $scope.ignore = function(request) {
    EdboRequest.ignore({ id: request.id },
      function() {
        var idx = $scope.requests.indexOf(request);
        $scope.requests.splice(idx, 1);
      });
  };

  $scope.check = function(request) {
    ++$scope.queue;
    checkHandler(request, angular.noop);
  };
  $scope.checkAll = function() {
    $scope.queue += $scope.requests.length;
    async.eachSeries($scope.requests, checkHandler);
  };

  $scope.create = function(request) {
    ++$scope.queue;
    EdboRequest.save({ id: request.id },
      function() {
        checkHandler(request, angular.noop);
      },
      function(err) {
        --$scope.queue;
        alert('Виникла помилка при додаванні заяви! Дивіться лог серверу!');
      });
  };

  $scope.subcreate = function(request, subrequest) {
    ++$scope.queue;
    EdboRequest.save({ id: request.id, 'subrequest-type': subrequest.Type },
      function() {
        //checkHandler(request, angular.noop);
        --$scope.queue;
        subrequest.isMissing   = false;
        subrequest.isDifferent = false;
        subrequest.isUnknown   = true;
      },
      function(err) {
        --$scope.queue;
        alert('Виникла помилка при додаванні підзаяви! Дивіться лог серверу!');
      });
  };

  $scope.$on('queue:inc', function() {
    ++$scope.queue;
  });
  $scope.$on('queue:dec', function() {
    --$scope.queue;
  });
});
