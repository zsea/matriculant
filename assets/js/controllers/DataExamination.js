'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('DataExaminationController',
    function($scope, Dictionary, $http) {
  $scope.$parent.title = 'Екзаменація';
  $scope.dict = Dictionary;

  $scope.upload = function(text) {
    var obj;
    try {
      obj = JSON.parse(text);
    }
    catch(err) {
      alert('Невірний формат файлу! Очікується JSON файл!');
      return;
    }

    $http.post('/api/data/examination.json', obj,
      function(result) {
        console.log(result);
      },
      function(err) {
        console.log(err.data);
      });
  };
});
