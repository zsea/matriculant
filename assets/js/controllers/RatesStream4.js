'use strict';

angular
.module('RatesBuilder')
.controller('RatesStream4Controller',
    function($scope, Request, $http, $location, Dictionary) {
  $scope.dict   = Dictionary;
  $scope.params = {};
  $scope.params.Stream = 4;

  $scope.recommend = function(request) {
    Request.setSubStatus({
      id:    request.SubRequests[0].id,
      value: 5
    },
    function() {
      request.Status = 5;
    });
  };
  $scope.reject = function(request) {
    Request.setSubStatus({
      id:    request.SubRequests[0].id,
      value: 6
    },
    function() {
      request.Status = 6;
    });
  };
  $scope.dismiss = function(request) {
    Request.setSubStatus({
      id:    request.SubRequests[0].id,
      value: 2
    },
    function() {
      request.Status = 2;
    });
  };
  $scope.accept = function(request) {
    Request.setSubStatus({
      id:    request.SubRequests[0].id,
      value: 4
    },
    function() {
      request.Status = 4;
    });
  };
  $scope.order = function(request) {
    Request.setSubStatus({
      id:    request.SubRequests[0].id,
      value: 7
    },
    function() {
      request.Status = 7;
    });
  };

  var regex = /[?&]([^=#]+)=([^&#]*)/g,
      url = window.location.href,
      match;

  while(match = regex.exec(url)) {
    $scope.params[match[1]] = +match[2];
  }

  Request.generate($scope.params, function(data) {
    data.forEach(function(request) {
      var primaryExam = request.Exams.filter(function(ex) {
        return ex.Primary === true;
      })[0];
      var foreignExam = request.Exams.filter(function(ex) {
        return ex.Foreign === true;
      })[0];

      request.EduDocument.Mark = request.EduDocument.Mark ? +request.EduDocument.Mark.toFixed(2) : 0.00;

      request.PrimaryMark = primaryExam ? primaryExam.Mark : 0;
      request.ForeignMark = foreignExam ? foreignExam.Mark : 0;
      request.SpecialistMark = request.PrimaryMark  + request.EduDocument.Mark;
      request.MagisterMark   = request.SpecialistMark + request.ForeignMark;
    });

    $scope.requests = data;

    $http({
      method: 'GET',
      url: '/api/info/speciality',
      params: $scope.params
    })
    .then(function(res) {
      $scope.Speciality = res.data;
      $scope.MagisterBudget = res.data.Capacity.magister;
      $scope.SpecialistBudget = res.data.Capacity.specialist;

      $scope.rates = { };
      $scope.prepareMagisterRates();
      $scope.prepareSpecialistRates();

      $scope.title = 'ready';
    });
  });

  $scope.getCurrentDate = function() {
    return new Date();
  };

  function isSpecialistBudget(request) {
    return request.Budget && request.PrimaryMark &&
           request.Status !== 2 && request.Status !== 6;
  }
  function isMagisterBudget(request) {
    return isSpecialistBudget(request) && request.ForeignMark;
  }
  function haveBenefit(request) {
    return request.Person.Benefits.some(function(benefit) {
      return benefit.Type.Strength === 1;
    });
  }
  function havePriorityBenefit(request) {
    if(request.EduDocument.Award !== 0) {
      return true;
    }

    return request.Person.Benefits.some(function(benefit) {
      return benefit.id !== 39 && benefit.Type.Strength === 2;
    });
  }

  function magisterSorter(lhs, rhs) {
    if(rhs.MagisterMark !== lhs.MagisterMark) {
      return rhs.MagisterMark - lhs.MagisterMark;
    }

    var rhsPriority = havePriorityBenefit(rhs),
        lhsPriority = havePriorityBenefit(lhs);

    if(rhsPriority && !lhsPriority) {
      return 1;
    }
    if(lhsPriority && !rhsPriority) {
      return -1;
    }

    return rhs.PrimaryMark - lhs.PrimaryMark;
  }
  function specialistSort(lhs, rhs) {
    if(rhs.SpecialistMark !== lhs.SpecialistMark) {
      return rhs.SpecialistMark - lhs.SpecialistMark;
    }

    var rhsPriority = havePriorityBenefit(rhs),
        lhsPriority = havePriorityBenefit(lhs);

    if(rhsPriority && !lhsPriority) {
      return 1;
    }
    if(lhsPriority && !rhsPriority) {
      return -1;
    }

    return rhs.PrimaryMark - lhs.PrimaryMark;
  }

  function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  $scope.prepareMagisterRates = function() {
    var benefitRequests = [], nonBenefitRequests = [], nonBudgetRequests = [],
        benefitLoosers  = [],
        benefitCount, budgetCount;

    budgetCount  = $scope.MagisterBudget || 0;
    benefitCount = Math.ceil(budgetCount / 4);

    var requests = deepClone($scope.requests);

    // some magic with 'Status' :)
    requests.forEach(function(request) {
      request.SubRequests = request.SubRequests.filter(function(item) {
        return item.Type === 2;
      });

      var subrequest = request.SubRequests[0];
      if(subrequest) {
        request.Status = subrequest.Status;
      }
    });

    requests = requests.filter(function(request) {
      return request.Status;
    });

    /* nonBudgetRequests = requests.filter(function(request) {
      return !isMagisterBudget(request);
    });
    nonBenefitRequests = requests.filter(function(request) {
      return isMagisterBudget(request);
    });

    benefitRequests = nonBenefitRequests.filter(function(request) {
      return haveBenefit(request);
    });
    nonBenefitRequests = nonBenefitRequests.filter(function(request) {
      return !haveBenefit(request);
    });*/

    // benefitRequests.sort(magisterSorter);

    //benefitLoosers = benefitRequests.splice(
    //  benefitCount, benefitRequests.length - benefitCount);

    //nonBenefitRequests = nonBenefitRequests.concat(benefitLoosers);

    // nonBenefitRequests.sort(magisterSorter);
    // nonBudgetRequests.sort(magisterSorter);
    requests.sort(magisterSorter); //* update

    // $scope.rates.magister = benefitRequests.concat(nonBenefitRequests);
    $scope.rates.magister = requests; //* update
/*    if($scope.params.Detailed) {
      $scope.rates.magister = $scope.rates.magister.concat(nonBudgetRequests);
    }*/

    // highlight recomendations
    for(var i = 0; i < budgetCount; ++i) {
      if($scope.rates.magister[i]) {
        $scope.rates.magister[i].recommend = true;
      }
    }
  };

  $scope.prepareSpecialistRates = function() {
    var benefitRequests = [], nonBenefitRequests = [], nonBudgetRequests = [],
        benefitLoosers  = [],
        benefitCount, budgetCount;

    budgetCount  = $scope.SpecialistBudget || 0;
    benefitCount = Math.ceil(budgetCount / 4);

    var requests = deepClone($scope.requests);

    // some magic with 'Status' :)
    requests.forEach(function(request) {
      request.SubRequests = request.SubRequests.filter(function(item) {
        return item.Type === 1;
      });

      var subrequest = request.SubRequests[0];
      if(subrequest) {
        request.Status = subrequest.Status;
      }
    });

    requests = requests.filter(function(request) {
      return request.Status;
    });

    nonBudgetRequests = requests.filter(function(request) {
      return !isSpecialistBudget(request);
    });
    nonBenefitRequests = requests.filter(function(request) {
      return isSpecialistBudget(request);
    });

    benefitRequests = nonBenefitRequests.filter(function(request) {
      return haveBenefit(request);
    });
    nonBenefitRequests = nonBenefitRequests.filter(function(request) {
      return !haveBenefit(request);
    });

    benefitRequests.sort(specialistSort);


    //benefitLoosers = benefitRequests.splice(
    //  benefitCount, benefitRequests.length - benefitCount);

    // nonBenefitRequests = nonBenefitRequests.concat(benefitLoosers);

    nonBenefitRequests.sort(specialistSort);
    nonBudgetRequests.sort(specialistSort);

    $scope.rates.specialist = benefitRequests.concat(nonBenefitRequests);
    if($scope.params.Detailed) {
      $scope.rates.specialist = $scope.rates.specialist.concat(nonBudgetRequests);
    }

    // highlight recomendations
    for(var i = 0; i < $scope.rates.specialist.length && budgetCount > 0; ++i) {
      var spec = $scope.rates.specialist[i];

      var mag = $scope.rates.magister.filter(function(request) {
        return request.id === spec.id;
      })[0];

      if(!mag || !mag.recommend) {
        spec.recommend = true;
        --budgetCount;
      }
    }
  };
});
