'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('AddPersonController',
    function($scope, $timeout, l10n, Person, Dictionary, Authorization) {
  $scope.$parent.title = l10n.get('AddPerson.title');

  $scope.dictionary = Dictionary;

  $scope.stepNumber = 1;
  $scope.step = function(number) {
    return number == $scope.stepNumber;
  };
  $scope.prevStep = function() {
    --$scope.stepNumber;
  };
  $scope.nextStep = function() {
    ++$scope.stepNumber;
  };

  $scope.resetData = function() {
    $scope.stepNumber = 1;
    $scope.data = {
      Document: { },

      EduDocuments: [],
      Certificates: [],
      Benefits:     [],

      NeedHostel:  false,
      IsForeigner: false
    };
  };

  $scope.data = JSON.parse(sessionStorage['add-person-data'] || 'null');
  if(!$scope.data) {
    $scope.resetData();
  }

  $scope.$watch('data', function() {
    sessionStorage['add-person-data'] = JSON.stringify($scope.data);
  }, true);

  $scope.tryQuickFind = function() {
    if($scope.data.Document.GivenBy !== undefined) {
      return $scope.nextStep();
    }

    Person.quickFind($scope.data,
      function(data) {
        // TODO: check in local db

        if(data.Surname !== $scope.data.Surname) {
          return $scope.nextStep();
        }

        $scope.data = data;
        $scope.nextStep();
      },
      function() {
        $scope.nextStep();
      });
  };

  $scope.proposeSex = function() {
    if($scope.data.Sex !== undefined) {
      return;
    }

    if(/на$/.test($scope.data.Patronymic)) {
      $scope.data.Sex = 2;
    }
    else {
      $scope.data.Sex = 1;
    }
  };

  $scope.proposeDocumentType = function() {
    if($scope.data.Document.Type !== undefined) {
      return;
    }

    $scope.data.Document.Type = 3;
  };

  $scope.toggleBenefit = function(id) {
    var idx = $scope.data.Benefits.indexOf(id);
    if(idx !== -1) {
      $scope.data.Benefits.splice(idx, 1);
    }
    else {
      $scope.data.Benefits.push(id);
    }
  };
  $scope.hasBenefit = function(id) {
    var idx = $scope.data.Benefits.indexOf(id);
    return idx !== -1;
  };

  $scope.ticksLeft = 0;
  $scope.timer_ = undefined;
  $scope.initFinalTimer = function() {
    $scope.ticksLeft = (Authorization.Level === 0 ? 10 : 30);
    $timeout.cancel($scope.timer_);

    $scope.timer_ = $timeout(function tick() {
      if($scope.ticksLeft === 0) {
        return;
      }

      $scope.ticksLeft--;
      $scope.timer_ = $timeout(tick, 1000);
    }, 1000);
  };

  $scope.finalAccept = function() {
    Person.save($scope.data,
      function() {
        $scope.resetData();
      },
      function() {
        alert('Помилка! Зверніться до адміністратора!');
      });
  };
});
