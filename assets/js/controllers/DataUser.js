'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('DataUserController',
    function($scope, l10n, Dictionary, $http) {
  $scope.$parent.title = l10n.get('User.title');
  $scope.dict = Dictionary;
  
  $http.get('/api/authorization')
  .success(function(users) {
  	$scope.users = users;
  });

  $scope.editPassword = function(user) {
    var hash = CryptoJS.SHA3(user.NewPassword, { outputLength: 256 }) + '';
    $http.post('/api/authorization/' + user.id, { hash: hash })
    .success(function(users) {
      user.NewPassword = '';
    });
  };

  $scope.newUser = { Level: 5 };

  $scope.addUser = function() {
    var hash = CryptoJS.SHA3($scope.newUser.Password, { outputLength: 256 }) + '';
    $http.post('/api/authorization', {
      Name: $scope.newUser.Name,
      Login: $scope.newUser.Login,
      Hash: hash,
      Level: $scope.newUser.Level
    })
    .success(function(user) {
      $scope.newUser = { Level: 5 };
      $scope.users.push(user);
    });
  };

});
