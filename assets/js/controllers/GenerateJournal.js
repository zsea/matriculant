'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('GenerateJournalController',
    function($scope, l10n, Request, Dictionary) {
  $scope.l10n = l10n;
  $scope.$parent.title = 'Журнал';
  $scope.dict = Dictionary;

  $scope.query = {
    Stream: 4,
    TeachingForm: 1,
    Speciality: 19,
    Date: new Date(),
  };
  $scope.pages = [];

  $scope.update = function() {
    if(!$scope.query.Stream || !$scope.query.TeachingForm || !$scope.query.Speciality) {
      return;
    }

    Request.journal($scope.query, function(data) {
      $scope.pages = data;
    });
  };
  $scope.update();
});
