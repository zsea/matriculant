'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('FindRequestController',
    function($scope, $timeout, $location, $modal, l10n, Request, Dictionary, Authorization) {
  $scope.auth = Authorization;
  $scope.l10n = l10n;
  $scope.$parent.title = 'Пошук заяв';
  $scope.dict = Dictionary;

  $scope.pagination = {};
  $scope.paginationDone = false;
  $scope.requests = [];
  $scope.count = 0;

  $scope.update = function() {
    $scope.pagination.offset = 0;
    $scope.pagination.limit  = 30;

    Request.query($scope.pagination, function(data) {
      $scope.requests = data;
      $scope.paginationDone = false;
    });
    
    Request.count($scope.pagination, function(data) {
      $scope.count = data.count;
    });
  };
  $scope.update();

  $scope.loadMoreRequests = function() {
    $scope.pagination.offset += $scope.pagination.limit;
    $scope.pagination.limit = 15;

    Request.query($scope.pagination, function(data) {
      if(data.length !== $scope.pagination.limit) {
        $scope.paginationDone = true;
      }

      $scope.requests = $scope.requests.concat(data);
    });
  };

  $scope.loadDetails = function(request) {
    if(request.Exams) {
      return;
    }

    request.$get();
  }

  $scope.removeRequest = function() {
    $scope.removableRequest.$delete(function() {
      var idx = $scope.requests.indexOf($scope.removableRequest);
      if(idx !== -1) {
        $scope.requests.splice(idx, 1);
      }
    });
  };

  $scope.showRemoveRequestModal = function(request) {
    $scope.removableRequest = request;
    $modal({ scope: $scope, template: 'view/modal/RemoveRequestModal.html', show: true });
  }

});
