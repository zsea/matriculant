'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('MainController',
    function($scope, $location, $route, l10n, Authorization) {
  l10n.setLocale('ua-uk');

  $scope.title = l10n.get('Main.title');
  $scope.title_additional = l10n.get('Main.title_additional');

  $scope.$on('$routeChangeSuccess', function() {
    $scope.page = $location.path();
  });

  $scope.auth = Authorization;
  $scope.auth.then(
    function(auth) {
      $scope.authorized = true;
    },
    function() {
      window.location.href = '/sign-in';
    });

  $scope.logout = function() {
    localStorage.setItem('ZSEA:Matriculant:Authorization', null);
    window.location.href = '/sign-in';
  };
});
