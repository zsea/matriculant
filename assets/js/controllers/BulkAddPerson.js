'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('BulkAddPersonController', function($scope, $http) {
  $scope.$parent.title = 'Пакетне додавання';

  $scope.textData = '';
  $scope.persons = [];

  $scope.exchange = function() {
    $scope.textData = '';

    $scope.persons.forEach(function(person) {
      if(person.raw) {
        $scope.textData += person.raw + '\n';
        return;
      }

      if(person.status !== 'success') {
        var fio = [person.Surname, person.Name, person.Patronymic].join(' ');
        $scope.textData += fio + '\n';
      }
    });
  };

  $scope.parse = function() {
    var lines  = $scope.textData.split('\n'),
        regexp = /^(\S+)[ \t]+(\S+)[ \t]+(\S+)$/;

    $scope.persons = [];

    lines.forEach(function(item) {
      item = item.trim();
      if(item.length === 0) {
        return;
      }

      var chunks = regexp.exec(item);
      if(!chunks) {
        $scope.persons.push({
          raw: item,
          status: 'error'
        });
      }
      else {
        $scope.persons.push({
          Surname:    chunks[1],
          Name:       chunks[2],
          Patronymic: chunks[3]
        });
      }
    });
  };

  $scope.import = function() {
    async.eachSeries($scope.persons, function(item, next) {
      if(item.status === 'error') {
        return next();
      }

      $http
        .post('/api/person/bulk', item)
        .then(function(res) {
          next();

          if(res.data.success) {
            item.status = 'success';
            return;
          }
          if(res.data.found === false) {
            item.status = 'not-found';
            return;
          }
        },
        function(err) {
          console.error(err);
          next();
        });
    });
  };
});
