'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('AddRequestController',
    function($scope, l10n, Dictionary, Person, Request, $routeParams) {
  $scope.$parent.title = l10n.get('AddRequest.title');
  $scope.dictionary = Dictionary;

  $scope.person = Person.get({ id: +$routeParams.personId });

  $scope.resetData = function() {
    $scope.data = {
      Original: false,
      Courses:  false,
      Person:   +$routeParams.personId,

      MagisterDegree:   true,
      SpecialistDegree: true
    };
  };

  $scope.$watch('data.Stream', function() {
    $scope.data.Speciality = undefined;
    $scope.data.Type = undefined;
  });

  $scope.resetData();

  $scope.$watch('data.Type', function(current, last) {
    if(current != last) {
      $scope.data.CertificateMark1 = undefined;
      $scope.data.CertificateMark2 = undefined;
      $scope.data.CertificateMark3 = undefined;
      $scope.data.Exams = [];
    }
  });

  $scope.create = function() {
    var lastRequestId = Dictionary.getSpecialityHash($scope.data.Speciality);
    if($scope.data.TeachingForm == 2) {
      lastRequestId += 'з';
    }

    Request.save($scope.data,
      function(response) {
        $scope.lastRequestId = lastRequestId + ' ' + response.Number;
        $scope.resetData();
      },
      function() {
        alert('Помилка! Зверніться до адміністратора!');
      });
  };
});
