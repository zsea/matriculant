'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('FindPersonController',
    function($scope, $timeout, $location, l10n, Person, Dictionary, pkDebounce) {
  $scope.l10n = l10n;
  $scope.$parent.title = l10n.get('SearchPerson.title');
  $scope.dictionary = Dictionary;

  $scope.pagination = { filter: '' };
  $scope.paginationDone = false;
  $scope.persons = [];

  function updateHandler() {
    $scope.pagination.offset = 0;
    $scope.pagination.limit  = 30;

    Person.query($scope.pagination, function(data) {
      $scope.persons = data;
      $scope.paginationDone = false;
    });
  }
  $scope.update = function() {
    pkDebounce(updateHandler, $scope, 500);
  };
  updateHandler();

  $scope.loadMorePersons = function() {
    $scope.pagination.offset += $scope.pagination.limit;
    $scope.pagination.limit = 15;

    Person.query($scope.pagination, function(data) {
      if(data.length !== $scope.pagination.limit) {
        $scope.paginationDone = true;
      }

      $scope.persons = $scope.persons.concat(data);
    });
  };
});
