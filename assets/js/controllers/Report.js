'use strict';

angular
.module('RatesBuilder')
.controller('ReportController', function($scope, Dictionary, $http, $location) {
  $scope.teachingForm =  $location.search().TeachingForm;
  if(!$scope.teachingForm)
    $scope.teachingForm = 1;
  $scope.passed = $location.search().Passed;
  if($scope.passed != 0)
    $scope.passed = 1;
  $scope.specialities = Dictionary.Speciality.$promise.then(function() {
    $scope.specialities = Dictionary.Speciality
      .filter(function(spec) {
        return spec.Stream === 1;
      });

    $scope.specialities.push({ id: 0, Name: 'Всього' });

    async.eachSeries($scope.specialities, function(speciality, next) {
      $http.get('/api/report/' +
        speciality.id + '/' + $scope.teachingForm + '/' + $scope.passed)
        .then(function(res) {
          speciality.info = res.data;
          next();
        },
        function(err) {
          next(err);
        });
    });
  });
});
