'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('EdboElectronicRequestController', function($scope, $http) {
  $scope.$parent.title = 'Електронні заяви';

  $scope.exhaused = false;
  $scope.count    = 0;

  $scope.loadOneMoreRequest = function(next) {
    $http.get('/api/edbo/request/load')
      .then(function() {
        ++$scope.count;
        next();
      },
      function(err) {
        if(err.status === 404) {
          $scope.exhaused = true;
        }
        else {
          next(err.data);
        }
      })
  };

  $scope.exchange = function() {
    async.whilst(function() {
      return !$scope.exhaused;
    },
    $scope.loadOneMoreRequest,
    function(err) {
      if(err) {
        $scope.error = err;
      }

      $scope.done = true;
    });
  }
});
