'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('DataCoursesController', function($scope, l10n, Request) {
  $scope.$parent.title = 'Підготовчі курси';

  $scope.requests = Request.query({
    Courses: true,
    offset:  0,
    limit:   9999
  });

  $scope.save = function(request) {
    if(!/[\d\.]+/.test(request.CoursesMark)) {
      return alert('Невірний формат оцінки!');
    }

    request.CoursesMark = +request.CoursesMark;
    Request.updateCourses({
      id:    request.id,
      value: request.CoursesMark
    },
    function(result) {
      request.CoursesMark = result.CoursesMark;
    });
  };
});
