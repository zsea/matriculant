'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('PersonDetailsController',
    function($scope, $routeParams, l10n, Person) {
  $scope.l10n = l10n;
  $scope.$watch('person', function() {
    var value;
    if($scope.person) {
      value = $scope.person.Surname + ' ' +
              $scope.person.Name.substr(0, 1) + '.' +
              $scope.person.Patronymic.substr(0, 1) + '.';
    }
    else {
      value = 'Особа';
    }

    $scope.$parent.title = value;
  });

  Person.get({ id: +$routeParams.id }, function(data) {
    $scope.person = data;
  },
  function(err) {
    $scope.error = true;
    console.error(err);
  });
});
