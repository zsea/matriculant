'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('SignInController', function($scope, $http) {
  $scope.login = '';
  $scope.password = '';

  $scope.tryLogin = function() {
    var hash = CryptoJS.SHA3($scope.password, { outputLength: 256 }) + '';

    $http.put('/api/authorization', {
      login: $scope.login,
      hash:  hash
    })
    .then(
      function() {
        var config = JSON.stringify({
          login: $scope.login,
          hash:  hash
        });

        localStorage.setItem('ZSEA:Matriculant:Authorization', config);
        window.location.href = '/';
      },
      function() {
        alert('Помилка входу! Перевірте логін та пароль!');
      });
  };
});
