'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('ExitController', function($scope, l10n) {
  $scope.exit = function() {
    $scope.$parent.title = l10n.get('Exit.title');
    location.href = "/signin";
  };
});
