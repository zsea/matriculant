'use strict';

angular
.module('RatesBuilder')
.controller('RatesStream1Controller',
    function($scope, Request, $http, $location, Dictionary) {
  $scope.dict   = Dictionary;
  $scope.params = {};
  $scope.params.Stream = 1;
  $scope.origCounter = 0;
  $scope.origCounterInc = function() {
    return ++$scope.origCounter;
  };

  $scope.recommend = function(request) {
    Request.setStatus({
      id:    request.id,
      value: 5
    },
    function() {
      request.Status = 5;
    });
  };
  $scope.reject = function(request) {
    Request.setStatus({
      id:    request.id,
      value: 6
    },
    function() {
      request.Status = 6;
    });
  };
  $scope.accept = function(request) {
    Request.setStatus({
      id:    request.id,
      value: 4
    },
    function() {
      request.Status = 4;
    });
  };
  $scope.order = function(request) {
    Request.setStatus({
      id:    request.id,
      value: 7
    },
    function() {
      request.Status = 7;
    });
  };

  var regex = /[?&]([^=#]+)=([^&#]*)/g,
      url = window.location.href,
      match;

  while(match = regex.exec(url)) {
    $scope.params[match[1]] = +match[2];
  }

  Request.generate($scope.params, function(data) {
    data.forEach(function(request) {
      request.EduDocument.Mark = request.EduDocument.Mark * 5;

      if(request.CertificateMark3.Mark) {
        request.profSubj = +request.CertificateMark1.Mark;
      }
      else {
        request.profSubj = +request.Exams[0].Mark;
      }

      if(request.CertificateMark3.Mark) {
        request.secSubj = +request.CertificateMark2.Mark;
      }
      else {
        request.secSubj = +request.Exams[1].Mark;
      }

      if(request.CertificateMark3.Mark) {
        request.thiSubj = +request.CertificateMark3.Mark;
      }
      else {
        request.thiSubj = +request.Exams[2].Mark;
      }

      request.TotalMark = request.EduDocument.Mark;
      request.TotalMark += request.profSubj;
      request.TotalMark += request.secSubj;
      request.TotalMark += request.thiSubj;
      request.TotalMark += request.CoursesMark;

      request.RawPriority = request.Priority;
      request.Priority = havePriorityBenefit(request);
    });

    $scope.requests = data;

    $http({
      method: 'GET',
      url: '/api/info/speciality',
      params: $scope.params
    })
    .then(function(res) {
      $scope.Speciality = res.data;
      $scope.Budget     = res.data.Capacity.general;

      $scope.rates = [];
      $scope.prepareRates();

      $scope.title = 'ready';
    });
  });

  $scope.getCurrentDate = function() {
    return new Date();
  };

  function isBudget(request) {
    return request.Budget && request.TotalMark &&
           request.Status !== 2 && request.Status !== 6;
  }
  function haveBenefit(request) {
    return request.Person.Benefits.some(function(benefit) {
      return benefit.Type.Strength === 1;
    });
  }
  function havePriorityBenefit(request) {
    if(request.Courses || request.EduDocument.Award !== 0) {
      return true;
    }

    return request.Person.Benefits.some(function(benefit) {
      return benefit.id !== 39 && benefit.Type.Strength === 2;
    });
  }

  function sorter(lhs, rhs) {
    if(rhs.TotalMark !== lhs.TotalMark) {
      return rhs.TotalMark - lhs.TotalMark;
    }

    var rhsPriority = havePriorityBenefit(rhs),
        lhsPriority = havePriorityBenefit(lhs);

    if(rhsPriority && !lhsPriority) {
      return 1;
    }
    if(lhsPriority && !rhsPriority) {
      return -1;
    }

    if(lhs.profSubj !== rhs.profSubj) {
      return rhs.profSubj - lhs.profSubj;
    }

    return rhs.EduDocument.Mark - lhs.EduDocument.Mark;
  }

  function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  $scope.prepareRates = function() {
    var benefitRequests = [], nonBenefitRequests = [], nonBudgetRequests = [],
        benefitLoosers  = [],
        benefitCount, budgetCount;

    budgetCount  = $scope.Budget || 0;
    benefitCount = Math.ceil(budgetCount / 4);

    var requests = deepClone($scope.requests);

    nonBudgetRequests = requests.filter(function(request) {
      return !isBudget(request);
    });
    nonBenefitRequests = requests.filter(function(request) {
      return isBudget(request);
    });

    benefitRequests = nonBenefitRequests.filter(function(request) {
      return haveBenefit(request);
    });
    nonBenefitRequests = nonBenefitRequests.filter(function(request) {
      return !haveBenefit(request);
    });

    benefitRequests.sort(sorter);
    benefitLoosers = benefitRequests.splice(
      benefitCount, benefitRequests.length - benefitCount);

    nonBenefitRequests = nonBenefitRequests.concat(benefitLoosers);

    var localSorter = function(lhs, rhs) {
      if(lhs.Status === 7 && rhs.Status !== 7) {
        return -1;
      }
      else if(rhs.Status === 7 && lhs.Status !== 7) {
        return 1;
      }

      return sorter(lhs, rhs);
    };

    nonBenefitRequests.sort(localSorter);
    nonBudgetRequests.sort(localSorter);

    $scope.rates = benefitRequests.concat(nonBenefitRequests);
    if($scope.params.Control) {
      $scope.rates = $scope.rates.concat(nonBudgetRequests);
    }

    // highlight recomendations
    for(var i = 0; i < budgetCount; ++i) {
      $scope.rates[i].recommend = true;
    }
  };
});
