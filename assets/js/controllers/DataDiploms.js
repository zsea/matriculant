'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('DataDiplomsController', function($scope, $http) {
  $scope.$parent.title = 'Довнесення дипломів';

  $scope.textData = '';
  $scope.items = [];

  $scope.exchange = function() {
    $scope.textData = '';

    $scope.items.forEach(function(item) {
      if(item.raw) {
        $scope.textData += item.raw + '\n';
        return;
      }

      if(item.status !== 'success') {
        var data = [
          item.Surname,
          item.Name,
          item.Patronymic,
          item.Series,
          item.Number,
          item.Mark
        ].join(' ');
        $scope.textData += data + '\n';
      }
    });
  };

  $scope.parse = function() {
    var lines  = $scope.textData.replace(/,/g, '.').split('\n'),
        regexp = /^(\S+)[ \t]+(\S+)[ \t]+(\S+)[ \t]+(\S{2})[ \t]+(\d{8})[ \t]+([\d\.]+)$/;

    $scope.items = [];

    lines.forEach(function(item) {
      item = item.trim();
      if(item.length === 0) {
        return;
      }

      var chunks = regexp.exec(item);
      if(!chunks) {
        $scope.items.push({
          raw: item,
          status: 'error'
        });
      }
      else {
        $scope.items.push({
          Surname:    chunks[1],
          Name:       chunks[2],
          Patronymic: chunks[3],
          Series:     chunks[4],
          Number:     chunks[5],
          Mark:       parseFloat(chunks[6])
        });
      }
    });
  };

  $scope.import = function() {
    async.eachSeries($scope.items, function(item, next) {
      if(item.status === 'error') {
        return next();
      }

      $http
        .post('/api/person/diplom', item)
        .then(function(res) {
          next();

          if(res.data.success) {
            item.status = 'success';
            return;
          }
          if(res.data.found === false) {
            item.status = 'not-found';
            return;
          }
        },
        function(err) {
          console.error(err);
          next();
        });
    });
  };
});
