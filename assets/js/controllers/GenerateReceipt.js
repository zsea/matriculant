'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('GenerateReceiptController',
    function($scope, l10n, Request, Dictionary) {
  $scope.l10n = l10n;
  $scope.$parent.title = 'Генерація розписок';
  $scope.dict = Dictionary;

  $scope.query = {};
  $scope.requests = [];

  $scope.update = function() {
    if(!$scope.query.Stream || !$scope.query.TeachingForm || !$scope.query.Speciality) {
      return;
    }

    Request.generate($scope.query, function(data) {
      $scope.requests = data;
    });
  };
  $scope.update();
});
