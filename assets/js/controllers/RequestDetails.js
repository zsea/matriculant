'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('RequestDetailsController',
    function($scope, $routeParams, l10n, Request) {
  $scope.l10n = l10n;
  $scope.$watch('request', function() {
    var value;
    if($scope.request) {
      value = $scope.request.Speciality.Hash;
      if($scope.request.TeachingForm === 2) {
        value += 'з';
      }

      value += ' ' + $scope.request.Number;
    }
    else {
      value = 'Заява';
    }

    $scope.$parent.title = value;
  });

  Request.get({ id: +$routeParams.id }, function(data) {
    $scope.request = data;
  },
  function(err) {
    $scope.error = true;
    console.error(err);
  });
});
