'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('GenerateRatesController',
    function($scope, l10n, Request, Dictionary) {
  $scope.l10n = l10n;
  $scope.$parent.title = 'Рейтинг';
  $scope.dict = Dictionary;

  $scope.params = { };

  $scope.$watch('params', function() {
    if(!$scope.params.Stream ||
       !$scope.params.Speciality ||
       !$scope.params.TeachingForm)
    {
      return false;
    }

    $scope.url =  '/rates/' + $scope.params.Stream;
    $scope.url += '?Speciality=' + $scope.params.Speciality;
    $scope.url += '&TeachingForm=' + $scope.params.TeachingForm;
    $scope.url += '&Detailed=1';
    $scope.url += '&Control=1';
  }, true);
});
