'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('GenerateExaminationTicketsController',
    function($scope, Dictionary, Request) {
  $scope.$parent.title = 'Екзаменаційні відомості';
  $scope.dict = Dictionary;

  $scope.query = { Stream: 4, TeachingForm: 1, Speciality: 19 };
  $scope.pairs = [];
  $scope.exams = {
    date: { },
    time: { }
  };

  $scope.update = function() {
    if(!$scope.query.Stream || !$scope.query.TeachingForm || !$scope.query.Speciality) {
      return;
    }

    Request.examinations($scope.query, function(data) {
      $scope.pairs = [];
      for(var i = 0; i < data.length / 2; ++i) {
        $scope.pairs.push({
          lhs: data[i * 2],
          rhs: data[i * 2 + 1]
        });
      }
    });
  };
  $scope.update();
});
