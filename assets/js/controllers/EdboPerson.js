'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('EdboPersonController', function($scope, $http, EdboPerson) {
  $scope.$parent.title = 'Особи ЄДЕБО';

  $scope.persons = EdboPerson.query();
  $scope.queue = 0;

  var checkHandler = function(person, next) {
    person.$get(function() {
      if(person.diff && person.diff.length === 0) {
        var idx = $scope.persons.indexOf(person);
        $scope.persons.splice(idx, 1);
      }
      $scope.queue--;
      next();
    },
    function(err) {
      $scope.queue--;
      console.error(err.data);
      next(true);
    });
  };

  $scope.check = function(person) {
    $scope.queue++;
    checkHandler(person, angular.noop);
  };

  $scope.checkAll = function() {
    $scope.queue += $scope.persons.length;
    async.eachSeries($scope.persons, checkHandler);
  };

  $scope.create = function(person) {
    $scope.queue++;
    EdboPerson.save({ id: person.id },
      function() {
        $scope.queue--;
        checkHandler(person, angular.noop);
      },
      function(err) {
        $scope.queue--;
        console.error(err.data);
        alert('Виникла помилка при додаванні персони! Дивіться лог серверу!');
      });
  };

  $scope.ignore = function(person) {
    EdboPerson.ignore({ id: person.id },
      function() {
        var idx = $scope.persons.indexOf(person);
        $scope.persons.splice(idx, 1);
      });
  };

  $scope.fix = function(person, diff) {
    if(diff.key === 'EduDocuments') {
      addEduDocument(person, diff.type, diff);
      return;
    }

    if(diff.key === 'Certificates') {
      addCertificate(person, diff.type, diff);
      return;
    }

    alert('Для даного випадку можливість виправлення не реалізована!');
  };

  $scope.changeMark = function(person, diff) {
    EdboPerson.changeEduDocumentMark({ id: person.id }, diff.current,
      function() {
        checkHandler(person, angular.noop);
      },
      function(err) {
        alert('Помилка! Дивіться лог серверу!');
        console.log(err.data);
      })
  };

  function addEduDocument(person, action, diff) {
    var direction = action === 'add' ? 'to_edbo' : 'to_matriculant';

    if(direction === 'to_edbo') {
      $scope.queue++;
    }

    EdboPerson
      .fixEduDocument({ id: person.id, direction: direction }, diff.current,
        function() {

          if(direction === 'to_edbo') {
            $scope.queue--;
          }

          checkHandler(person, angular.noop);
        },
        function(err) {

          if(direction === 'to_edbo') {
            $scope.queue--;
          }

          alert('Помилка! Дивіться лог серверу!');
          console.log(err.data);
        });
  }

  function addCertificate(person, action, diff) {
    var direction = action === 'add' ? 'to_edbo' : 'to_matriculant';

    if(direction === 'to_edbo') {
      $scope.queue++;
    }

    EdboPerson
      .fixCertificate({ id: person.id, direction: direction }, diff.current,
        function() {

          if(direction === 'to_edbo') {
            $scope.queue--;
          }

          checkHandler(person, angular.noop);
        },
        function(err) {

          if(direction === 'to_edbo') {
            $scope.queue--;
          }

          alert('Помилка! Дивіться лог серверу!');
          console.log(err.data);
        });
  }
});
