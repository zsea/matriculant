'use strict';

angular
.module('ZSEA-PK-App.controllers')
.controller('GenerateJournalTitleController',
    function($scope, l10n, Request, Dictionary) {
  $scope.l10n = l10n;
  $scope.$parent.title = 'Титульні сторінки';
  $scope.dict = Dictionary;

  $scope.getYear = function() {
    return new Date().getFullYear();
  }
});
