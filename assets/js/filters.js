'use strict';

angular.module('ZSEA-PK-App.filters', [])
.filter('fio', function() {
  return function(input) {
    return input[0].toUpperCase() + input.substr(1).toLowerCase();
  };
});
