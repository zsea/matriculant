'use strict';

angular.module('ZSEA-PK-App.services', []);
angular.module('ZSEA-PK-App.directives', []);
angular.module('ZSEA-PK-App.controllers', []);

angular.module('ZSEA-PK-App', [
  'ZSEA-PK-App.services', 'ZSEA-PK-App.directives', 'ZSEA-PK-App.controllers',
  'ZSEA-PK-App.filters',

  'l10n', 'l10n-tools', 'UA-UK', 'ui.select2',
  'ngResource', 'ngRoute',
  'infinite-scroll', 'mgcrea.ngStrap'
])

.config(function($locationProvider) {
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
})

.config(function($modalProvider) {
  angular.extend($modalProvider.defaults, {
    animation: 'am-fade-and-scale',
    placement: 'center'
  });
})

.config(function($routeProvider) {
  var deps = {
    dictionary: 'Dictionary',
    auth:       'Authorization'
  };

  $routeProvider
    .when('/', {
      templateUrl: '/view/Instruction.html',
      controller:  'InstructionController',
      resolve:     deps
    })
    .when('/person/add', {
      templateUrl: '/view/AddPerson.html',
      controller:  'AddPersonController',
      resolve:     deps
    })
    .when('/person/bulk', {
      templateUrl: '/view/BulkAddPerson.html',
      controller:  'BulkAddPersonController',
      resolve:     deps
    })
    .when('/request/add/:personId', {
      templateUrl: '/view/AddRequest.html',
      controller:  'AddRequestController',
      resolve:     deps
    })
    .when('/person', {
      templateUrl: '/view/FindPerson.html',
      controller:  'FindPersonController',
      resolve:     deps
    })
    .when('/person/:id', {
      templateUrl: '/view/PersonDetails.html',
      controller:  'PersonDetailsController',
      resolve:     deps
    })
    .when('/request', {
      templateUrl: '/view/FindRequest.html',
      controller:  'FindRequestController',
      resolve:     deps
    })
    .when('/request/:id', {
      templateUrl: '/view/RequestDetails.html',
      controller:  'RequestDetailsController',
      resolve:     deps
    })
    .when('/data', {
      templateUrl: '/view/Data.html'
    })
    .when('/data/diploms', {
      templateUrl: '/view/DataDiploms.html',
      controller:  'DataDiplomsController',
      resolve:     deps
    })
    .when('/data/courses', {
      templateUrl: '/view/DataCourses.html',
      controller:  'DataCoursesController',
      resolve:     deps
    })
    .when('/data/originals', {
      templateUrl: '/view/DataOriginals.html',
      controller:  'DataOriginalsController',
      resolve:     deps
    })
    .when('/data/examination', {
      templateUrl: '/view/DataExamination.html',
      controller:  'DataExaminationController',
      resolve:     deps
    })
    .when('/generate', {
      templateUrl: '/view/Generate.html',
    })
    .when('/generate/receipt', {
      templateUrl: '/view/GenerateReceipt.html',
      controller:  'GenerateReceiptController',
      resolve:     deps
    })
    .when('/generate/journal/title', {
      templateUrl: '/view/GenerateJournalTitle.html',
      controller:  'GenerateJournalTitleController',
      resolve:     deps
    })
    .when('/generate/journal', {
      templateUrl: '/view/GenerateJournal.html',
      controller:  'GenerateJournalController',
      resolve:     deps
    })
    .when('/generate/examination/tickets', {
      templateUrl: '/view/GenerateExaminationTickets.html',
      controller:  'GenerateExaminationTicketsController',
      resolve:     deps
    })
    .when('/generate/rates', {
      templateUrl: '/view/GenerateRates.html',
      controller:  'GenerateRatesController',
      resolve:     deps
    })
    .when('/edbo/person', {
      templateUrl: '/view/EdboPerson.html',
      controller:  'EdboPersonController',
      resolve:     deps
    })
    .when('/edbo/request', {
      templateUrl: '/view/EdboRequest.html',
      controller:  'EdboRequestController',
      resolve:     deps
    })
    .when('/edbo/ez', {
      templateUrl: '/view/EdboElectronicRequest.html',
      controller:  'EdboElectronicRequestController',
      resolve:     deps
    })
    .when('/data/user', {
      templateUrl: '/view/DataUser.html',
      controller:  'DataUserController',
      resolve:     deps
    })
    .otherwise({
      templateUrl: '/view/Instruction.html',
      controller:  'InstructionController',
      resolve:     deps
    });
})

.run(function(uiSelect2Config) {
  var latinKeyboard    = "qwertyuiop[]asdfghjkl;'zxcvbnm,.",
      cyrillicKeyboard = "йцукенгшщзхїфівапролджєячсмитьбю",
      alter = {};

  for(var i = 0; i < latinKeyboard.length; ++i) {
    alter[latinKeyboard[i]] = cyrillicKeyboard[i];
  }

  uiSelect2Config.matcher = function(term, text) {
    if(text.toLowerCase().indexOf(term.toLowerCase()) >= 0) {
      return true;
    }

    var alteredTerm = '';
    for(var i = 0; i < term.length; ++i) {
      alteredTerm += alter[term[i]];
    }

    return text.toLowerCase().indexOf(alteredTerm.toLowerCase()) >= 0;
  };
});

angular.module('RatesBuilder', [ 'ngResource', 'ZSEA-PK-App.services' ])

.config(function($locationProvider) {
  $locationProvider.html5Mode(true);
});
