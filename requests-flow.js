var Connection = require('./modules/connection.js'),
    db         = require('./modules/db.js')(Connection),
    Promise    = require('bluebird');

function findSpecialities(Request, Subjects) {
  if(Subjects.length === 0) {
    return [];
  }

  var sql = [
    'SELECT DISTINCT parent."Speciality", ',
    '  (SELECT COUNT(*) FROM "Request" r',
    '    WHERE r."Person" = ' + Request.Person.id,
    '    AND "Speciality" = parent."Speciality"',
    '  ) AS "Own"',
    'FROM "SpecialitySubject" parent',
    'GROUP BY parent."Speciality"',
    'HAVING',
    '  EXISTS (',
    '    SELECT *',
    '    FROM "SpecialitySubject" child',
    '    WHERE child."Subject" IN (' + Subjects + ')',
    '      AND child."Speciality" = parent."Speciality"',
    '      AND child."Primary" = true AND child."Secondary" = false',
    '  )',
    '  AND EXISTS (',
    '    SELECT *',
    '    FROM "SpecialitySubject" child',
    '    WHERE child."Subject" IN (' + Subjects + ')',
    '      AND child."Speciality" = parent."Speciality"',
    '      AND child."Primary" = false AND child."Secondary" = false',
    '  )',
    'ORDER BY 1'
  ].join('\n');

  return db._connection.knex.raw(sql)
    .then(function(data) {
      data = data.rows.map(function(item) {
        return { id: item.Speciality, own: +item.Own > 0 };
      });

      return data;
    });
}

var specialities = { };
function specialityName(id) {
  return specialities[id];
}

new db.Speciality()
  .fetch()
  .then(function(data) {
    data.toJSON().forEach(function(item) {
      specialities[item.id] = item.Hash;
    });

    return new db.Request()
      .query(function(params) {
        params.where({ Original: true, Stream: 1, TeachingForm: 1 });
        params.whereNotIn('Status', [2, 6]);
      }).fetch();
  })
  .then(function(data) {
    return data.load('Person');
  })
  .then(function(data) {
    data = data.toJSON();

    var promises = data.map(function(request) {
      return new db.CertificateMark()
        .query(function(params) {
          params.join('Certificate', 'Certificate.Person', '=', request.Person.id);
          params.whereRaw('"Certificate" = "Certificate".id');
          params.distinct('Subject');
        })
        .fetch()
        .then(function(result) {
          return result.toJSON();
        });
    });

    return Promise.all(promises)
      .then(function(result) {
        data.forEach(function(item, idx) {
          item.Subjects = result[idx].map(function(mark) {
            return mark.Subject;
          });
        });

        return data;
      });
  })
  .then(function(requests) {
    var promises = requests.map(function(request) {
      return findSpecialities(request, request.Subjects);
    });

    return Promise.all(promises)
      .then(function(result) {
        requests.forEach(function(item, idx) {
          item.Specialities = result[idx];
        });

        return requests;
      });
  })
  .then(function(requests) {
    return requests.map(function(request) {
      return {
        Name: request.Person.Name,
        Surname: request.Person.Surname,
        Patronymic: request.Person.Patronymic,
        Original: request.Speciality,
        Subjects: request.Subjects,
        Specialities: request.Specialities
      };
    });
  })
  .then(function(requests) {
    requests.sort(function(lhs, rhs) {
      return lhs.Original - rhs.Original;
    });

    requests.forEach(function(request) {
      var specialities = request.Specialities.map(function(spec) {
        if(spec.own) {
          return '(' + specialityName(spec.id) + ')';
        }
        else {
          return specialityName(spec.id);
        }
      }).join(' ');

      console.log(request.Surname, request.Name, request.Patronymic,
        '[' + specialityName(request.Original) + ']:', specialities);
    });

    process.exit(0);
  });
