'use strict';

var async = require('async'),
    u     = require('./utils.js'),
    createRequest = require('./create_request');

var PERSONS_COUNT = 10000;

var createCertificates = function(isOriginal) {
  return [
    {
      'marks': [
        {
          'Subject': 1,
          'Mark': u.number(124, 200)
        },
        {
          'Subject': 3,
          'Mark': u.number(124, 200)
        },
        {
          'Subject': 5,
          'Mark': u.number(124, 200)
        }
      ],

      'Year':   2014,
      'Number': u.digits(8),
      'Pin':    u.digits(4),
      'IsOriginal': isOriginal
    },

    {
      'marks': [
        {
          'Subject': 1,
          'Mark': u.number(124, 200)
        },
        {
          'Subject': 6,
          'Mark': u.number(124, 200)
        },
        {
          'Subject': 5,
          'Mark': u.number(124, 200)
        }
      ],

      'Year':   2013,
      'Number': u.digits(8),
      'Pin':    u.digits(4),
      'IsOriginal': isOriginal
    }
  ];
};

var createDocument = function() {
  return {
    'GivenBy': u.locString(5) + ' РВ УМВС України',

    'id': {
      'Series': u.locString(2).toLocaleUpperCase(),
      'Number': u.digits(6)
    },

    'Type': 1,
    'Date': '2010-02-03T00:00:00.000Z'
  };
};


var createEduDocuments = function(isOriginal) {
  return [
    {
      'id': {
        'Series': u.locString(2).toLocaleUpperCase(),
        'Number': u.digits(6)
      },
      'Type':    2,
      'GivenBy': u.locString(10),
      'Date':    '2010-02-03T00:00:00.000Z',

      'Mark':     u.number(20, 60),
      'MarkType': 60,

      'Benefit':    Math.random() < 0.5 ? true : false,
      'IsOriginal': isOriginal
    }
  ];
};

var createBenefits = function() {
  var result = [];
  if(Math.random() > 0.5) {
    var count = u.number(0, 3);

    for(var i = 0; i < count; ++i) {
      result.push(u.number(1, 26));
    }
  }

  return result;
};

var createPerson = function() {
  var isOriginal = Math.random() < 0.5;

  return {
    'Document':     createDocument(),
    'EduDocuments': createEduDocuments(isOriginal),
    'Certificates': createCertificates(isOriginal),

    'Surname':    u.locString(u.number(6, 15)),
    'Name':       u.locString(u.number(6, 15)),
    'Patronymic': u.locString(u.number(6, 15)),

    'BirthDate': u.date(),
    'Sex':       u.number(1, 2),

    'BirthPlace':  u.digits(10),
    'LivingPlace': u.digits(10),

    'StreetType':   u.number(1, 3),
    'LivingStreet': u.locString(u.number(6, 15)),

    'PhoneNumber': u.digits(6),
    'PostCode':    u.digits(5),

    'Benefits':    createBenefits(),
    'IsForeigner': Math.random() > 0.9,
    'NeedHostel':  Math.random() > 0.5
  };
};

async.timesSeries(PERSONS_COUNT, function(i, next) {
  var person = createPerson();

  u
    .request('http://localhost:7070/api/person', 'POST', person)
    .then(function(res) {
      return createRequest(res.id);
    })
    .then(function() {
      next();
    })
    .catch(function(err) {
      next(err);
    });
},
function(err) {
  if(err) {
    console.error(err.stack);
  }
});
