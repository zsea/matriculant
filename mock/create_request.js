'use strict';
var u       = require('./utils'),
    Promise = require('bluebird');

var ExaminationCause = u
      .request('http://localhost:7070/api/dictionary/examination-cause')
      .then(function(data) {
        return JSON.parse(data);
      }),
    Speciality = u
      .request('http://localhost:7070/api/dictionary/speciality')
      .then(function(data) {
        return JSON.parse(data);
      }),
    CertificateSubject = u
      .request('http://localhost:7070/api/dictionary/certificate-subject')
      .then(function(data) {
        return JSON.parse(data);
      });

module.exports = function(person) {
  return getPersonInfo(person)
    .then(function(person) {
      var count = u.number(1, 4),
          queue = [];

      person = JSON.parse(person);

      for(var i = 0; i < count; ++i) {
        if(Math.random() < 0.5) {
          queue.push(createWithExams(person));
        }
        else {
          queue.push(createWithCertificate(person));
        }
      }

      return Promise.all(queue);
    });
};

function getPersonInfo(person) {
  return u.request('http://localhost:7070/api/person/' + person);
}

function createWithExams(person) {
  var request = {
    Person: person.id,
    Type: 2,
    TeachingForm: Math.random() < 0.5 ? 1 : 2,
    Stream: u.number(1, 4),

    EduDocument: person.EduDocuments[u.number(0, person.EduDocuments.length - 1)].id_,
    Original: Math.random() < 0.5,

    Courses: true,
    CourseMark: Math.random() < 0.3 ? u.number(1, 15) : null,

    Exams: createExams(person)
  };

  return ExaminationCause
    .then(function(cause) {
      request.ExaminationCause = cause[u.number(0, cause.length - 1)].id;
      return Speciality;
    })
    .then(function(spec) {
      request.Speciality = spec[u.number(0, spec.length - 1)].id;
      return createExams();
    })
    .then(function(exams) {
      request.Exams = exams;

      return u.request('http://localhost:7070/api/request', 'POST', request);
    });
}

function createExams() {
  if(Math.random() < 0.5) {
    return [
      {
        Primary: true,
        Foreign: false
      },
      {
        Primary: false,
        Foreign: true
      }
    ];
  }
  else {
    return CertificateSubject.then(function(subject) {
      return [
        {
          Subject: subject[u.number(0, subject.length - 1)].id
        },
        {
          Subject: subject[u.number(0, subject.length - 1)].id
        },
        {
          Subject: subject[u.number(0, subject.length - 1)].id
        }
      ];
    });
  }
}

function createWithCertificate(person) {
  var request = {
    Person: person.id,
    Type: 2,
    TeachingForm: Math.random() < 0.5 ? 1 : 2,
    Stream: u.number(1, 4),

    EduDocument: person.EduDocuments[u.number(0, person.EduDocuments.length - 1)].id_,
    Original: Math.random() < 0.5,

    CertificateMark1: person.Certificates[u.number(0, 1)].marks[0].id,
    CertificateMark2: person.Certificates[u.number(0, 1)].marks[1].id,
    CertificateMark3: person.Certificates[u.number(0, 1)].marks[2].id,

    Courses: true,
    CourseMark: Math.random() < 0.3 ? u.number(1, 15) : null,

    Exams: []
  };

  return Speciality
    .then(function(spec) {
      request.Speciality = spec[u.number(0, spec.length - 1)].id;
    })
    .then(function() {
      return u.request('http://localhost:7070/api/request', 'POST', request);
    });
}
