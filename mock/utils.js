'use strict';
var Promise = require('bluebird'),
    request = require('request');

var getNumber = function(min, max) {
  return min + Math.round(Math.random() * (max - min));
};

var getStringGeneral = function(alphabet, length) {
  var result = '';

  for(var i = 0; i < length; ++i) {
    result += alphabet[getNumber(0, alphabet.length - 1)];
  }

  return result;
};

module.exports = {

  string: function(length) {
    var alphabet = 'qwertyuiopsdfghjklzxcvbnm';
    return getStringGeneral(alphabet, length);
  },

  locString: function(length) {
    var alphabet = 'йцукенгшщзхъфывапролджэячсмитьбю';
    return getStringGeneral(alphabet, length);
  },

  digits: function(length) {
    var alphabet = '0123456789';
    return getStringGeneral(alphabet, length);
  },

  number: getNumber,

  date: function() {
    return new Date(getNumber(2000, 2014), getNumber(0, 11), getNumber(0, 28)).toISOString();
  }

};


module.exports.request = function(url, method, payload) {
  return new Promise(function(resolve, reject) {
    var opts = {
      url: url,
      method: method || 'GET'
    };

    if(payload) {
      opts.json = payload;
    }

    request(opts, function(error, res, body) {
      if(error) {
        return reject(error);
      }

      resolve(body);
    });
  });
};
