'use strict';

var httpPort = require('../package.json').httpPort;

var wkhtmltopdf = require('wkhtmltopdf'),
    Connection  = require('../modules/connection.js'),
    querystring = require('querystring'),
    translate   = require('./translate'),
    async = require('async'),
    db    = require('../modules/db.js')(Connection),
    path  = require('path'),
    fs    = require('fs');

new db.Speciality()
  .query(function() {
    this.where({ Stream: 4 });
  })
  .fetch()
  .then(function(models) {
    models = models.toJSON();

    async.eachSeries(models, function(speciality, next) {
      async.parallel([
        createRate.bind(null, {
          speciality: speciality,
          detailed:   0
        }),
        createRate.bind(null, {
          speciality: speciality,
          detailed:   1
        })
      ], next);
    },
    function(err) {
      if(err) {
        console.error(err);
      }

      process.exit();
    });
  });

function createRate(opts, next) {
  var destPath,
      url  = 'http://localhost:' + httpPort + '/rates/4?',
      name = translate(opts.speciality.Hash);

  if(opts.detailed) {
    name = 'detailed_' + name;
  }

  destPath = path.resolve('stream4', name + '.pdf'),

  url += querystring.stringify({
    Speciality: opts.speciality.id,
    Detailed:   opts.detailed,
    TeachingForm: 1
  });

  wkhtmltopdf(url, {
    pageSize:        'a4',
    javascriptDelay: 100,
    printMediaType:  true
  },
  function(code, signal) {
    if(code) {
      return next(signal);
    }

    next();
  }).pipe(fs.createWriteStream(destPath));
}
