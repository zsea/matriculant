'use strict';

var cyrLetters, latLetters, table = { };
cyrLetters = "йцукенгшщзхїфівапролджєячсмитьбюЙЦУКЕНГШЩЗХЇФІВАПРОЛДЖЄЯЧСМИТЬБЮ";
latLetters = "ycukengsszhyfivaproldzeycsmit_buYCUKENGSSZHYFIVAPROLDZEYCSMIT_BU";

for(var i = 0; i < cyrLetters.length; ++i) {
  table[cyrLetters.charAt(i)] = latLetters.charAt(i);
}

module.exports = function(text) {
  var result = '';
  for(var i = 0; i < text.length; ++i) {
    result += table[text.charAt(i)];
  }

  return result;
};
