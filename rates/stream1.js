'use strict';

var httpPort = require('../package.json').httpPort;

var wkhtmltopdf = require('wkhtmltopdf'),
    Connection  = require('../modules/connection.js'),
    querystring = require('querystring'),
    translate   = require('./translate'),
    async = require('async'),
    db    = require('../modules/db.js')(Connection),
    path  = require('path'),
    fs    = require('fs');

new db.Speciality()
  .query(function() {
    this.where({ Stream: 1 });
  })
  .fetch()
  .then(function(models) {
    models = models.toJSON();

    async.eachSeries(models, function(speciality, next) {
      async.parallel([
        createRate.bind(null, {
          speciality:  speciality,
          orientation: 'landscape',
          phones: 0
        }),
        createRate.bind(null, {
          speciality:  speciality,
          orientation: 'portrait',
          phones: 0
        }),
        createRate.bind(null, {
          speciality:  speciality,
          orientation: 'portrait',
          phones: 1
        })
      ], next);
    },
    function(err) {
      if(err) {
        console.error(err);
      }

      process.exit();
    });
  });

function createRate(opts, next) {
  var destPath,
      url  = 'http://localhost:' + httpPort + '/rates/1?',
      name = translate(opts.speciality.Hash);

  if(opts.phones) {
    name = path.join('phones', name + '.pdf');
  }
  else {
    name = path.join(opts.orientation, name + '.pdf')
  }

  destPath = path.resolve('stream1', name),

  url += querystring.stringify({
    TeachingForm: 1,
    Speciality: opts.speciality.id,
    Detailed:   1,
    Phones: opts.phones,
    Small:  opts.orientation == 'portrait' ? 1 : 0
  });

  wkhtmltopdf(url, {
    pageSize:        'a4',
    javascriptDelay: 100,
    orientation: opts.orientation,
    printMediaType:  true
  },
  function(code, signal) {
    if(code) {
      return next(signal);
    }

    next();
  }).pipe(fs.createWriteStream(destPath));
}
